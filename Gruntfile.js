var path = require('path');

module.exports = function(grunt) {

  require('load-grunt-tasks')(grunt);

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    bwr: grunt.file.readJSON('bower.json'),
    meta: {
      version: '<%= pkg.version %>',
      banner:
        '// LV Widgets \n' +
        '// ----------------------------------\n' +
        '// v<%= pkg.version %>\n' +
        '//\n' +
        '// Copyright (c)<%= grunt.template.today("yyyy") %> Longview INC.\n' +
        '// Distributed under MIT license\n' +
        '//\n' +
        '\n'
    },
    assets: {
      babysitter:   'bower_components/backbone.babysitter/lib/backbone.babysitter.js',
      paginator:    'bower_components/backbone.paginator/lib/backbone.paginator.js',
      underscore:   'bower_components/underscore/underscore.js',
      backbone:     'bower_components/backbone/backbone.js',
      jquery:       'bower_components/jquery/dist/jquery.js'
    },

    clean: {
      dist: 'dist'
    },

    bower: {
      install: {
        options: {
          copy: false
        }
      }
    },
    jst: {
      options: {
        prettify: false,
        processName: function(filename) {
          return filename.slice(filename.indexOf('html'), filename.length);
        }
      },
      compile: {
        files: {
          'tmp/templates.js': ['src/html/**/*.html']
        }
      }
    },

    preprocess: {
      dist: {
        src: 'src/build/lv-widgets.build.js',
        dest: 'tmp/lv-widgets.js'
      }
    },

    template: {
      options: {
        data: {
          version: '<%= pkg.version %>'
        }
      },
      dist: {
        src: '<%= preprocess.dist.dest %>',
        dest: '<%= preprocess.dist.dest %>'
      }
    },

    concat: {
      options: {
        banner: '<%= meta.banner %>'
      },
      dist: {
        src: '<%= preprocess.dist.dest %>',
        dest: 'dist/lv-widgets.js'
      }
    },

    uglify : {
      dist: {
        src : '<%= concat.dist.dest %>',
        dest : 'dist/lv-widgets.min.js',
        options : {
          banner: '<%= meta.banner %>'
        }
      }
    },

    exec: {
      unwrap: {
        command: './node_modules/.bin/unwrap ./bower_components/backbone.paginator/lib/backbone.paginator.js > ./tmp/backbone.paginator.bare.js'
      }
    },

    env: {
      coverage: {
        APP_DIR_FOR_CODE_COVERAGE: '../../../test/tmp/'
      }
    },

    instrument: {
      files: 'tmp/lv-widgets.js',
      options: {
        lazy: true,
        basePath: 'test'
      }
    },

    mochaTest: {
      tests: {
        options: {
          require: 'spec/javascripts/setup/node.js',
          reporter: grunt.option('mocha-reporter') || 'nyan',
          clearRequireCache: true,
          mocha: require('mocha')
        },
        src: [
          'spec/javascripts/setup/helpers.js',
          'spec/javascripts/*.spec.js'
        ]
      }
    },

    storeCoverage: {
      options: {
        dir: 'coverage'
      }
    },
    makeReport: {
      src: 'coverage/**/*.json',
      options: {
        type: 'lcov',
        dir: 'coverage',
        print: 'detail'
      }
    },

    coveralls: {
      options: {
        src: 'coverage/lcov.info',
        force: false
      },
      default: {
        src: 'coverage/lcov.info'
      }
    },

    plato: {
      lv : {
        src : 'src/*.js',
        dest : 'reports',
        options : {
          jshint : grunt.file.readJSON('.jshintrc')
        }
      }
    },

    jshint: {
      options: {
        jshintrc: '.jshintrc'
      },
      all: [
        'Gruntfile.js',
        'src/js/**/*.js'
      ]
    },

    watch: {
      lv : {
        options: {
          spawn: false
        },
        files : ['src/**/*.js', 'spec/**/*.js'],
        tasks : ['test']
      }
    },

    connect: {
      server: {
        options: {
          port: 8888
        }
      }
    },

    lintspaces: {
      all: {
        src: [
          'src/*.js',
          'docs/*.md'
        ],
        options: {
          editorconfig: '.editorconfig'
        }
      }
    },

    gitstash: {
      save: {
        options: {
          command: 'save'
        }
      },
      pop: {
        options: {
          command: 'pop'
        }
      }
    },

    gitpull: {
      task: {
        options: {
          remote: 'origin',
          branch: 'master'
        }
      }
    },

    gitcommit: {
      task: {
        options: {
          message: 'Updated to v<%= pkg.version %>'
        },
        files: {
          src: [
            'dist/**',
            'src/**',
            '.editorconfig',
            '.gitignore',
            'Gruntfile.js',
            'bower.json',
            'package.json',
            'readme.md'
          ]
        }
      }
    },

    gittag: {
      version: {
        options: {
          tag: 'v<%= pkg.version %>'
        }
      },
      edge: {
        options: {
          tag: 'edge'
        }
      },
      remove: {
        tag: 'edge',
        remove: true
      }
    },

    gitpush: {
      master: {
        options: {
          remote: 'origin',
          branch: 'master'
        }
      },
      tag: {
        options: {
          remote: 'origin',
          branch: 'v<%= pkg.version %>'
        }
      }
    }
  });

  var defaultTestsSrc = grunt.config('mochaTest.tests.src');
  var defaultJshintSrc = grunt.config('jshint.lv.src');
  var defaultJshintSpecSrc = grunt.config('jshint.specs.files.src');
  grunt.event.on('watch', function(action, filepath) {
    grunt.config('mochaTest.tests.src', defaultTestsSrc);
    grunt.config('jshint.lv.src', defaultJshintSrc);
    grunt.config('jshint.specs.files.src', defaultJshintSpecSrc);
    if (filepath.match('spec/javascripts/') && !filepath.match('setup') && !filepath.match('fixtures')) {
      grunt.config('mochaTest.tests.src', ['spec/javascripts/setup/helpers.js', filepath]);
      grunt.config('jshint.specs.files.src', filepath);
      grunt.config('jshint.lv.src', 'DO_NOT_RUN_ME');
    }
    if (filepath.match('src/')) {
      grunt.config('jshint.lv.src', filepath);
      grunt.config('jshint.specs.files.src', 'DO_NOT_RUN_ME');
    }
  });

  grunt.registerTask('verify-bower', function () {
    if (!grunt.file.isDir('./bower_components')) {
      grunt.fail.warn('Missing bower components. You should run `bower install` before.');
    }
  });

  grunt.registerTask('default', 'An alias task for running tests.', ['test']);

  grunt.registerTask('lint', 'Lints our sources', ['lintspaces', 'jshint']);

  grunt.registerTask('test', 'Run the unit tests.', ['verify-bower', 'lint', 'preprocess:bundle', 'template:bundle', 'mochaTest']);

  grunt.registerTask('coverage', ['exec:unwrap', 'preprocess:bundle', 'template:bundle', 'env:coverage', 'instrument', 'mochaTest', 'storeCoverage', 'makeReport', 'coveralls']);

  grunt.registerTask('dev', 'Auto-lints while writing code.', ['test', 'watch:lv']);

  grunt.registerTask('build', 'Build the distribution version of the library.', ['clean:dist', 'bower:install', 'lint', 'jst', 'exec:unwrap', 'preprocess', 'template', 'concat', 'uglify']);

  grunt.registerTask('publish', 'Tag and push the specified version to the remote origin server', function (versionType) {
    grunt.task.run(['gitstash:save', 'gitpull', 'gitstash:pop']);
    var semver = grunt.config.get('bwr').version.split('.');
    var publishVersion = function () {
      grunt.config.set('bwr.version', semver.join('.'));
      grunt.config.set('pkg.version', semver.join('.'));
      grunt.file.write('bower.json', JSON.stringify(grunt.config.get('bwr'), null, 2));
      grunt.file.write('package.json', JSON.stringify(grunt.config.get('pkg'), null, 2));
      grunt.task.run(['build', 'gitcommit', 'gittag:version', 'gitpush:master', 'gitpush:tag']);
    };
    switch (versionType) {
      case 'major':
        semver[0] = (parseInt(semver[0]) + 1);
        semver[1] = 0;
        semver[2] = 0;
        publishVersion();
        break;
      case 'minor':
        semver[1] = (parseInt(semver[1]) + 1);
        semver[2] = 0;
        publishVersion();
        break;
      case 'patch':
        semver[2] = (parseInt(semver[2]) + 1);
        publishVersion();
        break;
      default:
        grunt.fail.fatal('Publish requires a valid version number type to increment. Use publish:[version] (major, minor, patch).');
        break;
    }
  });
};
