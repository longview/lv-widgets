// LV Widgets 
// ----------------------------------
// v2.19.13
//
// Copyright (c)2016 Longview INC.
// Distributed under MIT license
//

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['jquery', 'underscore', 'backbone', 'marionette'], function ($, _, Backbone) {
      root = factory(root, $, _, Backbone);
    });
  } else {
    root = factory(root, root.jQuery, root._, root.Backbone);
  }

}(this, function (root, $, _, Backbone) {

  var previousLV = root.LV;

  this["JST"] = this["JST"] || {};
  
  this["JST"]["html/carousel/carousel-item.html"] = function(obj) {
  obj || (obj = {});
  var __t, __p = '', __e = _.escape;
  with (obj) {
  __p += '<div>Please specify an itemView for the carousel</div>';
  
  }
  return __p
  };
  
  this["JST"]["html/carousel/lv-carousel.html"] = function(obj) {
  obj || (obj = {});
  var __t, __p = '', __e = _.escape;
  with (obj) {
  __p += '<!-- Wrapper for slides -->\n<div class="carousel-section carousel-left-section"></div>\n<div class="carousel-section carousel-inner" role="listbox"></div>\n<div class="carousel-section carousel-right-section"></div>\n<!-- Controls -->\n<button class="left carousel-control" role="button" data-slide="prev">Previous</button>\n<button class="right carousel-control" role="button" data-slide="next">Next</button>';
  
  }
  return __p
  };
  
  this["JST"]["html/coversheet/allergiesInfoTab.html"] = function(obj) {
  obj || (obj = {});
  var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
  function print() { __p += __j.call(arguments, '') }
  with (obj) {
  
   if (headers) { ;
  __p += '\n<h4 class="coversheet-title">' +
  ((__t = ( obj.label )) == null ? '' : __t) +
  '</h4>\n';
   } ;
  __p += '\n<table class="table table-striped">\n  <thead>\n    <tr class="focusinto" tabindex="0">\n      <th>Allergen</th>\n      <th>Reaction</th>\n      <th>Facility</th>\n    </tr>\n  </thead>\n  <tbody class="allergyItems">\n  ';
   if (allergy.length !== 0) {;
  __p += '\n    ';
   for(var i =0; i < allergy.length; i++) { ;
  __p += '\n      <tr tabindex="0">\n        <td>\n          <span>' +
  ((__t = ( obj.allergy[i].substance )) == null ? '' : __t) +
  '</span>\n        </td>\n        <td>\n          <span>' +
  ((__t = ( obj.allergy[i].reaction )) == null ? '' : __t) +
  '</span>\n        </td>\n        <td>\n          <span>' +
  ((__t = ( obj.allergy[i].sourceSystem )) == null ? '' : __t) +
  '</span>\n        </td>\n      </tr>\n    ';
   } ;
  __p += '\n  ';
   } else { ;
  __p += '\n    <tr tabindex="0">\n      <td><span>No Data Found</span></td>\n      <td></td>\n      <td></td>\n    </tr>\n  ';
   } ;
  __p += '\n  </tbody>\n</table>\n';
  
  }
  return __p
  };
  
  this["JST"]["html/coversheet/appointmentInfoTab.html"] = function(obj) {
  obj || (obj = {});
  var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
  function print() { __p += __j.call(arguments, '') }
  with (obj) {
  
   if (headers) { ;
  __p += '\n<h4 class="coversheet-title">' +
  ((__t = ( obj.label )) == null ? '' : __t) +
  '</h4>\n';
   } ;
  __p += '\n<table class="table table-striped">\n  <thead>\n    <tr class="focusinto" tabindex="0">\n      <th>Facility / Clinic</th>\n      <th>Provider</th>\n      <th>Date / Time</th>\n    </tr>\n  </thead>\n  <tbody class="appointmentItems">\n  ';
   if (appointment.facilityName !== "No Data Found" && appointment.clinicName !== "No Data Found") {;
  __p += '\n    ';
   for(i = 0; i < appointment.length; i++) { ;
  __p += '\n      <tr tabindex="0">\n        <td>\n          <span>' +
  ((__t = ( obj.appointment[i].facilityName )) == null ? '' : __t) +
  ' / ' +
  ((__t = ( obj.appointment[i].clinicName )) == null ? '' : __t) +
  '</span>\n        </td>\n        <td>\n          <span>' +
  ((__t = ( obj.appointment[i].provider )) == null ? '' : __t) +
  '</span>\n        </td>\n        <td>\n          <span>' +
  ((__t = ( obj.appointment[i].appointmentStartDate )) == null ? '' : __t) +
  '</span>\n        </td>\n      </tr>\n    ';
   } ;
  __p += '\n  ';
   } else {;
  __p += '\n    <tr tabindex="0">\n      <td>\n        <span>No Data Found</span>\n      </td>\n      <td></td>\n      <td></td>\n    </tr>\n  ';
   } ;
  __p += '\n  </tbody>\n</table>\n';
  
  }
  return __p
  };
  
  this["JST"]["html/coversheet/contactInfoTab.html"] = function(obj) {
  obj || (obj = {});
  var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
  function print() { __p += __j.call(arguments, '') }
  with (obj) {
  
   if (headers) { ;
  __p += '\n<h4 class="coversheet-title">' +
  ((__t = ( obj.label )) == null ? '' : __t) +
  '</h4>\n';
   } ;
  __p += '\n<h4 class="focusinto" tabindex="0">' +
  __e( obj.displayName) +
  '</h4>\n<br />\n<div tabindex="0" class="row" id="DOB">\n  <div class="col-sm-3 col-xs-4">\n    Date of Birth\n  </div>\n  <div class="col-sm-9 col-xs-8">\n    ' +
  __e(obj.dateOfBirth) +
  '\n  </div>\n</div>\n<div tabindex="0" class="row" id="Gender">\n  <div class="col-sm-3 col-xs-4">\n    Gender\n  </div>\n  <div class="col-sm-9 col-xs-8">\n    ' +
  __e(obj.gender) +
  '\n  </div>\n</div>\n<div tabindex="0" class="row" id="Work">\n  <div class="col-sm-3 col-xs-4">\n    Work\n  </div>\n  <div class="col-sm-9 col-xs-8">\n    ';
   if(phoneNumberWork !== "()--") { ;
  __p += '\n    ' +
  __e(obj.phoneNumberWork) +
  '\n    ';
   } else { ;
  __p += '\n    No Data Found\n    ';
   } ;
  __p += '\n  </div>\n</div>\n<div tabindex="0" class="row" id="Home">\n  <div class="col-sm-3 col-xs-4">\n    Home\n  </div>\n  <div class="col-sm-9 col-xs-8">\n    ';
   if(phoneNumberHome !== "()--") { ;
  __p += '\n    ' +
  __e(obj.phoneNumberHome) +
  '\n    ';
   } else { ;
  __p += '\n    No Data Found\n    ';
   } ;
  __p += '\n  </div>\n</div>\n<div tabindex="0" class="row" id="Cell">\n  <div class="col-sm-3 col-xs-4">\n    Cell\n  </div>\n  <div class="col-sm-9 col-xs-8">\n    ';
   if(phoneNumberMobile !== "()--") { ;
  __p += '\n    ' +
  __e(obj.phoneNumberMobile) +
  '\n    ';
   } else { ;
  __p += '\n    No Data Found\n    ';
   } ;
  __p += '\n  </div>\n</div>\n<div tabindex="0" class="row" id="Pager">\n  <div class="col-sm-3 col-xs-4">\n    Pager\n  </div>\n  <div class="col-sm-9 col-xs-8">\n    ';
   if(phoneNumberPager !== "()--") { ;
  __p += '\n    ' +
  __e(obj.phoneNumberPager) +
  '\n    ';
   } else { ;
  __p += '\n    No Data Found\n    ';
   } ;
  __p += '\n  </div>\n</div>\n<div tabindex="0" class="row" id="Email">\n  <div class="col-sm-3 col-xs-4">\n    Email Address\n  </div>\n  <div class="col-sm-9 col-xs-8">\n    ';
   if(emailAddress !== "") { ;
  __p += '\n    ' +
  __e(obj.emailAddress) +
  '\n    ';
   } else { ;
  __p += '\n    No Data Found\n    ';
   } ;
  __p += '\n\n  </div>\n</div>\n<div tabindex="0" class="row" id="Location">\n  <div class="col-sm-3 col-xs-4">\n    Location\n  </div>\n  <div class="col-sm-9 col-xs-8">\n    ';
   if(wardLocation !== "") { ;
  __p += '\n    ' +
  __e(obj.wardLocation) +
  '\n    ';
   } else { ;
  __p += '\n    No Data Found\n    ';
   } ;
  __p += '\n  </div>\n</div>\n<div tabindex="0" class="row" id="Address">\n  <div class="col-sm-3 col-xs-4">\n    Address\n  </div>\n  <div class="col-sm-9 col-xs-8">\n    ';
   if(address.streetAddressLine1) { ;
  __p += '\n      ' +
  __e( obj.address.streetAddressLine1 ) +
  ' <br>\n    ';
   } else { ;
  __p += '\n    No Data Found <br>\n    ';
   } ;
  __p += '\n    ';
   if(address.streetAddressLine2) { ;
  __p += '\n    ' +
  __e( obj.address.streetAddressLine2 ) +
  ' <br>\n    ';
   } else { ;
  __p += '\n    No Data Found <br>\n    ';
   } ;
  __p += '\n    ';
   if(address.streetAddressLine3) { ;
  __p += '\n    ' +
  __e( obj.address.streetAddressLine3 ) +
  ' <br>\n    ';
   } else { ;
  __p += '\n    No Data Found <br>\n    ';
   } ;
  __p += '\n    ';
   if(typeof address.state !== "undefined") { ;
  __p += '\n    ' +
  __e( obj.address.city ) +
  ', ' +
  __e( obj.address.state ) +
  ', ' +
  __e( obj.address.zip ) +
  '\n    ';
   } else { ;
  __p += '\n    No Data Found\n    ';
   } ;
  __p += '\n  </div>\n</div>\n<h4 tabindex="0">Next of Kin</h4>\n<div tabindex="0" class="row" id="nok_label">\n  <div class="col-sm-3 col-xs-4">\n    Relationship\n  </div>\n  <div class="col-sm-9 col-xs-8">\n    ' +
  __e( obj.nextOfKin.relationship ) +
  '\n  </div>\n</div>\n<div tabindex="0" class="row" id="nok_Name">\n  <div class="col-sm-3 col-xs-4">\n    Name\n  </div>\n  <div class="col-sm-9 col-xs-8">\n    ' +
  __e( obj.nextOfKin.name ) +
  '\n  </div>\n</div>\n<div tabindex="0" class="row" id="nok_Phone">\n  <div class="col-sm-3 col-xs-4">\n    Phone\n  </div>\n  <div class="col-sm-9 col-xs-8">\n    ' +
  __e( obj.nextOfKin.phoneNumber ) +
  '\n  </div>\n</div>\n<br/>\n<div tabindex="0">To update demographic data, contact the Health Eligibility Center: 1-800-929-8387</div>\n';
  
  }
  return __p
  };
  
  this["JST"]["html/coversheet/hospitalizationsInfoTab.html"] = function(obj) {
  obj || (obj = {});
  var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
  function print() { __p += __j.call(arguments, '') }
  with (obj) {
  
   if (headers) { ;
  __p += '\n<h4 class="coversheet-title">' +
  ((__t = ( obj.label )) == null ? '' : __t) +
  '</h4>\n';
   } ;
  __p += '\n<table class="table table-striped">\n    <thead>\n    <tr class="focusinto" tabindex="0">\n        <th>Facility</th>\n        <th>Admission Date</th>\n        <th>Discharge Date</th>\n    </tr>\n    </thead>\n    <tbody class="appointmentItems">\n    ';
   if (admission.length) { ;
  __p += '\n    ';
   for(i = 0; i < admission.length; i++) { ;
  __p += '\n    <tr tabindex="0">\n        <td>\n            <span>' +
  ((__t = ( obj.admission[i].facilityName )) == null ? '' : __t) +
  '</span>\n        </td>\n        <td>\n            <span>' +
  ((__t = ( obj.admission[i].admissionDate )) == null ? '' : __t) +
  '</span>\n        </td>\n        <td>\n            <span>' +
  ((__t = ( obj.admission[i].dischargeDate )) == null ? '' : __t) +
  '</span>\n        </td>\n    </tr>\n    ';
   } ;
  __p += '\n    ';
   } else {;
  __p += '\n    <tr tabindex="0">\n        <td>\n            <span>No Data Found</span>\n        </td>\n        <td></td>\n        <td></td>\n    </tr>\n    ';
   } ;
  __p += '\n    </tbody>\n</table>\n';
  
  }
  return __p
  };
  
  this["JST"]["html/coversheet/lv-coversheet.html"] = function(obj) {
  obj || (obj = {});
  var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
  function print() { __p += __j.call(arguments, '') }
  with (obj) {
  __p += '<div class="centered-content content-padding row" id="cover-menu">\n  <div id="desktop-menu" class="' +
  __e( obj.desktopClass ) +
  '">\n    ';
   if (!customTabs) { ;
  __p += '\n    <ul class="nav nav-pills nav-stacked col-sm-3" role="tablist" id="tab-menu">\n      ';
   for (var i=0; i<tabs.length; i++) { ;
  __p += '\n        <li class="cover-tab-container ' +
  __e( obj.tabs[i].get('id') ) +
  ' ';
   if (i==0){ ;
  __p += 'active';
   };
  __p += '">\n          <a href="#' +
  __e( obj.tabs[i].get('id') ) +
  'Tab" role="tab" data-toggle="pill" class="cover-tab" aria-label="' +
  __e( obj.tabs[i].get('label') );
   if (i==0){ ;
  __p += ', Active';
   };
  __p += '">' +
  __e( obj.tabs[i].get('label') ) +
  '</a>\n        </li>\n      ';
   } ;
  __p += '\n    </ul>\n    ';
   } ;
  __p += '\n\n    <!-- Tab panes -->\n    <div class="tab-content ' +
  __e( obj.desktopTabClass ) +
  '" id="tab-info-container">\n      ';
   for (var i=0; i<tabs.length; i++) { ;
  __p += '\n        <div class="tab-pane ' +
  __e( obj.tabs[i].get('id') ) +
  'Tab ';
   if (i==0) { ;
  __p += 'active';
   } ;
  __p += '" id="' +
  __e( obj.tabs[i].get('id') ) +
  'Tab"></div>\n      ';
   } ;
  __p += '\n    </div>\n  </div>\n\n  ';
   if (!customTabs) { ;
  __p += '\n  <div class="panel-group visible-xs" id="mobile-menu" role="tablist" aria-multiselectable="false">\n    ';
   for (var i=0; i<tabs.length; i++) { ;
  __p += '\n      <div class="panel panel-default">\n        <a class="' +
  __e( obj.tabs[i].get('id') ) +
  ' ';
   if (i==0){ ;
  __p += 'active';
   } else { ;
  __p += 'collapsed';
   } ;
  __p += ' panel-heading" data-toggle="collapse" data-target="#' +
  __e( obj.tabs[i].get('id') ) +
  'Collapse" role="tab" id="' +
  __e( obj.tabs[i].get('id') ) +
  'Heading" aria-label="' +
  __e( obj.tabs[i].get('label') ) +
  ' Information';
   if (i==0){ ;
  __p += ', Open';
   };
  __p += '" tabindex="0">\n          <h4 class="panel-title">\n              ' +
  __e( obj.tabs[i].get('label') ) +
  '\n          </h4>\n        </a>\n        <div id="' +
  __e( obj.tabs[i].get('id') ) +
  'Collapse" class="panel-collapse collapse ';
   if (i==0){ ;
  __p += 'in';
   } ;
  __p += ' ' +
  __e( obj.tabs[i].get('id') ) +
  '" role="tabpanel" aria-labelledby="contactHeading">\n          <div class="panel-body ' +
  __e( obj.tabs[i].get('id') ) +
  'Tab">\n          </div>\n        </div>\n      </div>\n     ';
   } ;
  __p += '\n  </div>\n  ';
   } ;
  __p += '\n</div>\n';
  
  }
  return __p
  };
  
  this["JST"]["html/coversheet/medicalTab.html"] = function(obj) {
  obj || (obj = {});
  var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
  function print() { __p += __j.call(arguments, '') }
  with (obj) {
  
   if (headers) { ;
  __p += '\n<h4 class="coversheet-title">' +
  ((__t = ( obj.label )) == null ? '' : __t) +
  '</h4>\n';
   } ;
  __p += '\n<table class="table table-striped">\n  <thead>\n  <tr class="focusinto" tabindex="0">\n    <th>Active</th>\n    <th>Date</th>\n  </tr>\n  </thead>\n  <tbody>\n  ';
   if(isDiagnosis()) { ;
  __p += '\n    ';
   for(var i = 0; i < encounterDiagnosisICDCode.length; i++) { ;
  __p += '\n    <tr tabindex="0">\n      <td>\n        <span>' +
  ((__t = ( obj.encounterDiagnosisICDCode[i].description )) == null ? '' : __t) +
  '</span>\n      </td>\n      <td>\n        <span>' +
  ((__t = ( obj.encounterDiagnosisICDCode[i].dateOnset )) == null ? '' : __t) +
  '</span>\n      </td>\n    </tr>\n    ';
   } ;
  __p += '\n  ';
   } else { ;
  __p += '\n    <tr tabindex="0">\n      <td>No Data Found</td>\n      <td></td>\n    </tr>\n  ';
   } ;
  __p += '\n  </tbody>\n</table>\n';
  
  }
  return __p
  };
  
  this["JST"]["html/coversheet/medicationsItemview.html"] = function(obj) {
  obj || (obj = {});
  var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
  function print() { __p += __j.call(arguments, '') }
  with (obj) {
  
   if (headers) { ;
  __p += '\n<h4 class="coversheet-title">' +
  ((__t = ( obj.label )) == null ? '' : __t) +
  '</h4>\n';
   } ;
  __p += '\n<table class="table table-striped">\n  <thead>\n  <tr class="focusinto" tabindex="0">\n    ';
   for(var i = 0; i < 3; i++) { ;
  __p += '\n    <th scope="col">' +
  ((__t = ( obj.colHeaders()[i] )) == null ? '' : __t) +
  '</th>\n    ';
   } ;
  __p += '\n  </tr>\n  </thead>\n  <tbody>\n  ';
   if (dataRows().length !== 0) { ;
  __p += '\n    ';
   for(var i = 0; i < dataRows().length; i++) { ;
  __p += '\n    <tr tabindex="0">\n      ';
   for(var inner = 0; inner < dataRows()[i].length; inner++) { ;
  __p += '\n      <td>\n        <span>' +
  ((__t = ( obj.dataRows()[i][inner] )) == null ? '' : __t) +
  '</span>\n      </td>\n      ';
   } ;
  __p += '\n    </tr>\n    ';
   } ;
  __p += '\n  ';
   } else { ;
  __p += '\n    <tr tabindex="0">\n      <td><span>No Data Found</span></td>\n      <td></td>\n      <td></td>\n    </tr>\n  ';
   } ;
  __p += '\n  </tbody>\n</table>\n';
  
  }
  return __p
  };
  
  this["JST"]["html/coversheet/surgeriesInfoTab.html"] = function(obj) {
  obj || (obj = {});
  var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
  function print() { __p += __j.call(arguments, '') }
  with (obj) {
  
   if (headers) { ;
  __p += '\n<h4 class="coversheet-title">' +
  ((__t = ( obj.label )) == null ? '' : __t) +
  '</h4>\n';
   } ;
  __p += '\n<table class="table table-striped">\n    <thead>\n    <tr class="focusinto" tabindex="0">\n        <th>Description</th>\n        <th>Date</th>\n        <th>Facility</th>\n    </tr>\n    </thead>\n    <tbody class="appointmentItems">\n    ';
   if (surgery.length) { ;
  __p += '\n    ';
   for(i = 0; i < surgery.length; i++) { ;
  __p += '\n    <tr tabindex="0">\n        <td>\n            <span>' +
  ((__t = ( obj.surgery[i].procedure )) == null ? '' : __t) +
  '</span>\n        </td>\n        <td>\n            <span>' +
  ((__t = ( obj.surgery[i].procedureDate )) == null ? '' : __t) +
  '</span>\n        </td>\n        <td>\n            <span>' +
  ((__t = ( obj.surgery[i].facility )) == null ? '' : __t) +
  '</span>\n        </td>\n    </tr>\n    ';
   } ;
  __p += '\n    ';
   } else { ;
  __p += '\n    <tr tabindex="0">\n        <td>\n            <span>No Data Found</span>\n        </td>\n        <td></td>\n        <td></td>\n    </tr>\n    ';
   } ;
  __p += '\n    </tbody>\n</table>\n';
  
  }
  return __p
  };
  
  this["JST"]["html/lv-view-eula.html"] = function(obj) {
  obj || (obj = {});
  var __t, __p = '', __e = _.escape;
  with (obj) {
  __p += '<div class="container-full lv-eula-body">\n  <header class="header-fixed">\n    <div id="header">\n      <div class="layer title">\n        <img class="navbar-brand pull-right" src="assets/VAHealth-logo.png" alt="Veteran\'s Affairs Health Logo">\n      </div>\n    </div>\n  </header>\n  <div id="content" class="lv-eula-content">\n    <img src="assets/US-DeptOfVeteransAffairsLogo.png" class="lv-eula-logo" alt="Official seal of the United States Department of Veterans Affairs"> <br> <br>\n            <span><strong>End User License Agreement</strong><br>\n                  <strong>Effective 2/11/2014</strong><br><br>\n                By agreeing to install VA’s mobile application (“Application”) on to your device and by subsequent use of the Licensed Software, you agree to comply with the terms of this general End User License Agreement ("EULA") and Notice of Privacy Practices ("Notice"). If you do not agree to the terms of this EULA and Notice, do not install or use the Licensed Software but uninstall it from your device. This EULA and Notice applies to any upgrades and supplements to the original Licensed Software provided and is referred to on your opening screen.  A copy of the EULA is available from within the Licensed Software.  It is your responsibility to review any future changes to the EULA and to uninstall the software if you do not agree to the terms.<br> </br>\n              <ol><li>The Licensed Software is owned by VA. The Licensed Software is licensed, not sold, only on the terms of this EULA. Acceptance and use of the software indicates your acceptance of the terms and conditions of this EULA.</li>\n                <li>Upon accessing the Licensed Software, you will acquire the right to use the Licensed Software, directly from VA. You assume responsibility for the selection of the program to achieve your intended results, and for the access, use and results obtained from the Licensed Software.</li>\n                <li>VA and you acknowledge that this Agreement is concluded between VA and you only, and not with your hardware manufacturer, operating system vendor, or organization that provided the mechanism to download the software (i.e. Apple\'s App Store, VA Enterprise App Store, or other).  VA is solely responsible for the Licensed Software and its content.</li>\n                <li>In consideration of your acceptance of the terms and conditions contained in this EULA, VA grants you a non-exclusive license to use the Licensed Software and the associated documentation for your own needs on one device. You are not licensed to rent, lease, transfer, or distribute the Licensed Software.</li>\n                <li>Title to the Licensed Software, including media and documentation, remain with VA. You may not copy or reproduce, except as supported by the Licensed Software, in whole or in part, or as is necessary for back-up or archival purposes. You may not reverse engineer, translate, disassemble, decompile the software or create similar software in whole or in part.</li>\n                <li>The license is effective upon acceptance and access to the Licensed Software and shall continue with any subsequent use of the application. VA has the right to terminate this Agreement if you fail to comply with any term or condition of this EULA. Upon termination you shall stop all use of the Licensed Software.</li>\n                <li>Confidentiality of the Licensed Software will survive any termination of this EULA, to include the application, design, and functionality.</li>\n                <li>This Licensed Software is distributed AS IS, in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.</li>\n                <li>In no event will VA be liable for any damages, including for loss of data, loss of use, or indirect, special, incidental or consequential damages in any way related to or arising out of the use of the Licensed Software whether based upon warranty, contract, or otherwise, whether or not injury was sustained by persons or property or otherwise, and whether or not loss was sustained from, or arose out of use of Licensed Software.  You agree to waive any and all claims against the U.S. Government, VA, its employees, its contractors, their subcontractors, and shall indemnify and hold harmless the U.S. Government, its contractors, and their subcontractors for any damage that you may incur from your use of the Licensed Software.</li>\n                <li>VA shall be neither liable nor responsible for any maintenance or updating of the provided Licensed Software, nor for correction of any errors in the Licensed Software.  The VA may discontinue the Licensed Software, potentially, but not necessarily, replacing it with a subsequent product.  Compatibility of data between products is not guaranteed.</li>\n                <li>This Agreement shall be construed and enforced in accordance with federal law and each party agrees to be subject to those relevant laws for all purposes.</li>\n                <li>It is understood and acknowledged that VA has the absolute right to obtain injunctive relief to protect VA\'s proprietary rights.</li>\n                <li>By using the Licensed Software, you further agree that this is the complete and exclusive statement of the Agreement which supersedes any proposal or prior agreement, oral or written, and any other communications relating to the subject matter of this EULA.</li>\n                <li>If any provision of this Agreement is held to be invalid or unenforceable, the remaining provisions will not be affected.</li>\n                <li>By agreeing to use VA software that allows you to access your health data, you are further agreeing based upon individual end-user access permissions to allow the application to access and display your available electronic personal health information from VA systems after proper identification and authentication.</li>\n                <li>Some applications may provide access to other Web sites outside VA control and jurisdiction. When you link to these Web sites, your communications no longer are protected by our privacy policies. VA is not responsible for the privacy practices or the content of non-VA Web sites. We encourage you to review the privacy policy or terms and conditions of those sites to fully understand what information is collected and how it is used.</li>\n                -<li>You have a responsibility to keep your health information safe. While VA ensures privacy and security of your personal health information while the data is in VA systems, VA cannot ensure privacy once you remove a copy of your information from a VA system.  Once information is downloaded, saved, printed, emailed, faxed or shared by other method, its protection is up to you.  When using an Application that allows display of your health information, remember that people may see your personal information on the screen. Turn the screen away from their view. Do not walk away from the mobile device or computer with your information showing. Always remember to log off when you have finished. If you must write information down, keep them in a safe place. If you print copies of your personal health information, be careful not to leave it in any public places and store copies in a safe place, like a locked file cabinet. If you share your personal health information with others, VA has no authority to ensure these people protect your privacy. Be careful of who you give copies of your personal health information, whether the information was entered by you or is from a VA system. Be sure to destroy printed copies of your health information preferably with a shredder. You are encouraged to enhance the security of your information by taking actions on any mobile device or computer used to access your health information. <br>\n                  - Recommended actions include but are not limited to the following: <br>\n                  - Use a password to protect access to your device.  Ensure that your password is easy for you to remember but difficult for others to guess. <br>\n                  - Use the autolock feature and/or timeout feature on your device. <br>\n                  - Do not jailbreak your device or download apps from sources other than designated app stores as these actions may expose you to security risk.</li>\n                <li>PRIVACY ACT STATEMENT: Use of VA Licensed Software by you may involve the collection of individually identifiable data that you enter into the Application and data about your use of the Application.   As authorized by 38 U.S.C. Section 501, VA is asking you to provide information via this Application which may be included with other information VA uses to deliver health care to you.  VA may disclose the information that you entered into the Application as permitted by law. VA may make a "routine use" disclosure of the information as outlined in the Privacy Act systems of records notices and in accordance with the Veterans Health Administration (VHA) Notice of Privacy Practices. VHA will explain these routine uses and privacy practices upon further request. Providing the information is voluntary.  Failure to furnish your identifying information (username and login) when required by an Application will prevent you from being able to use the Licensed Software, but will not have any effect on any other benefits or care to which you may be entitled. VA may also use this information to identify users of the Licensed Software, and for other purposes authorized or required by law.</li>\n                <li>The Licensed Software transfer of individually identifiable data will use secure methods to transmit the data.  Data collected by the Licensed Software for patient care purposes will be securely transmitted into VA data systems to be stored as part of your health care records covered under a Privacy Act system of records.</li>\n                <li>DATA USE: Data resulting from the use of the Licensed Software will be made available to VA authorized persons in the conduct of their official business.  Data may be used for statistical and management purposes in assessing the benefit of this software.  Data provided for research purposes will be made anonymous so that it is not personally identifiable.</li>\n                <li>DISCLAIMER: The content of this Application is intended for use only as an informative tool by the user.  It is not, is not intended to be, and should not be used in any way as a substitute for professional medical advice or training.  The accuracy of the information provided is not guaranteed.  The user acknowledges in initiating this Application that the information is not meant to diagnose a health condition or disease and is not meant to develop a health treatment plan.  If you are in an emergency or life-threatening medical situation, seek medical assistance immediately.  Dial emergency number (911 in the USA) for emergency medical services.</li>\n              </ol>\n\n            </span>\n  </div>\n  <footer class="row footer-fixed lv-eula-footer">\n    <div class="col-xs-6 text-center">\n      <a id="decline-button" href="/launchpad" class="">Decline</a>\n    </div>\n    <div class="col-xs-6 text-center">\n      <a id="accept-button" href="#home" class="">Accept</a>\n    </div>\n  </footer>\n</div>\n';
  
  }
  return __p
  };
  
  this["JST"]["html/lv-view-popup-layout.html"] = function(obj) {
  obj || (obj = {});
  var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
  function print() { __p += __j.call(arguments, '') }
  with (obj) {
  __p += '<div class="popup-body ' +
  __e( obj.popupClass ) +
  '">\n    <div class="popup-header">\n        ';
   if (obj.title) { ;
  __p += '<h2>' +
  __e( obj.title ) +
  '</h2>';
   } ;
  __p += '\n        ';
   if (!obj.hideCloseBtn) { ;
  __p += '<button class="' +
  __e( obj.closeBtnClass ) +
  ' icon ' +
  __e( obj.closeBtnIcon ) +
  '" id="popup-close-btn" title="close">X</button>';
   } ;
  __p += '\n    </div>\n    <div class="popup-content ' +
  __e( obj.contentClass ) +
  '"></div>\n    <div class="popup-footer"></div>\n</div>';
  
  }
  return __p
  };
  
  this["JST"]["html/lv-view-popup-one-button.html"] = function(obj) {
  obj || (obj = {});
  var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
  function print() { __p += __j.call(arguments, '') }
  with (obj) {
  __p += '<button id="popup-button-l">\n    ';
   if (obj.btnIconL) { ;
  __p += '\n    <span class="icon ' +
  __e( obj.btnIconL ) +
  '"></span>\n    ';
   } ;
  __p += '\n    ' +
  __e( obj.btnTitleL ) +
  '\n</button>';
  
  }
  return __p
  };
  
  this["JST"]["html/lv-view-popup-two-button.html"] = function(obj) {
  obj || (obj = {});
  var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
  function print() { __p += __j.call(arguments, '') }
  with (obj) {
  __p += '<button id="popup-button-l" class="popup-two-button">\n    ';
   if (obj.btnIconL) { ;
  __p += '\n    <span class="icon ' +
  __e( obj.btnIconL ) +
  '"></span>\n    ';
   } ;
  __p += '\n    ' +
  __e( obj.btnTitleL ) +
  '\n</button>\n<button id="popup-button-r" class="popup-two-button">\n    ';
   if (obj.btnIconR) { ;
  __p += '\n    <span class="icon ' +
  __e( obj.btnIconR ) +
  '"></span>\n    ';
   } ;
  __p += '\n    ' +
  __e( obj.btnTitleR ) +
  '\n</button>';
  
  }
  return __p
  };
  
  this["JST"]["html/lv-view-refresh.html"] = function(obj) {
  obj || (obj = {});
  var __t, __p = '', __e = _.escape;
  with (obj) {
  __p += '<div>\n\t<button type=\'button\' class=\'btn\'>\n\t\t<span class=\'' +
  __e( obj.refreshIcon ) +
  '\'></span>\n        <span class=\'hide-text\'>Refresh</span>\n\t</button>\n\t<span>\n\t\t<span class=\'' +
  __e( obj.labelClass ) +
  '\'>' +
  __e( obj.label ) +
  '</span>\n\t\t' +
  __e( obj.time ) +
  '\n\t</span>\n</div>';
  
  }
  return __p
  };
  
  this["JST"]["html/lv-view-tab-layout.html"] = function(obj) {
  obj || (obj = {});
  var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
  function print() { __p += __j.call(arguments, '') }
  with (obj) {
  __p += '<ul class="lv-tabs nav nav-tabs nav-justified" role="tablist">\n    ';
   _.each(tabs, function (tab, tabname) { ;
  __p += '\n        ';
   if (tabname === defaultTab) { ;
  __p += '\n            <li class="active"><a class="lv-tab-' +
  ((__t = ( obj.tabname.toLowerCase() )) == null ? '' : __t) +
  '" href="#">' +
  ((__t = ( obj.tabname )) == null ? '' : __t) +
  '</a><div class="lv-tab-bottom"></div></li>\n        ';
   } else { ;
  __p += '\n            <li><a class="lv-tab-' +
  ((__t = ( obj.tabname.toLowerCase() )) == null ? '' : __t) +
  '" href="#">' +
  ((__t = ( obj.tabname )) == null ? '' : __t) +
  '</a><div class="lv-tab-bottom"></div></li>\n        ';
   } ;
  __p += '\n    ';
   }); ;
  __p += '\n</ul>\n<div class="lv-tab-content">\n\n</div>\n';
  
  }
  return __p
  };
  
  this["JST"]["html/mylist/filter-item-edit-view.html"] = function(obj) {
  obj || (obj = {});
  var __t, __p = '', __e = _.escape;
  with (obj) {
  __p += '<div>\n\t<div class="col-xs-1 no-padding">\n    \t<div class="filter-item">\n      \t\t<a id="option-' +
  __e( obj.index ) +
  '" tabindex="0" href="" role="checkbox" aria-live="polite" aria-label="Checkbox to see all patients on ' +
  __e( obj.name ) +
  ' list." aria-checked="' +
  __e( obj.isActive() ) +
  '" class="square-child ' +
  __e( obj.activeClass() ) +
  '">&nbsp;&#x2713;&nbsp;</a>\n    \t</div>\n\t</div>\n\t<div class="col-xs-10">\n\t\t<input type="text" value="' +
  __e( obj.name ) +
  '"></input>\n\t</div>\n</div>';
  
  }
  return __p
  };
  
  this["JST"]["html/mylist/filter-item-view.html"] = function(obj) {
  obj || (obj = {});
  var __t, __p = '', __e = _.escape;
  with (obj) {
  __p += '<div>\n\t<div class="col-xs-1">\n    \t<div aria-checked="' +
  __e( obj.isActive() ) +
  '" class="square-parent ' +
  __e( obj.activeClass() ) +
  '">\n      \t\t<button id="option-' +
  __e( obj.index ) +
  '" tabindex="0" role="checkbox" aria-live="polite" aria-label="Checkbox to see all patients on ' +
  __e( obj.name ) +
  ' list." aria-checked="' +
  __e( obj.isActive() ) +
  '" class="square-child">&nbsp;&#x2713;&nbsp;</button>\n    \t</div>\n\t</div>\n\t<div class="col-xs-10 mylist-name">\n\t\t' +
  __e( obj.name ) +
  '\n\t</div>\n</div>';
  
  }
  return __p
  };
  
  this["JST"]["html/mylist/filter-view.html"] = function(obj) {
  obj || (obj = {});
  var __t, __p = '', __e = _.escape;
  with (obj) {
  __p += '<div>\n  <div id="my-list-filters" class="clearfix"></div>\n  <button class="btn clearfix update-results" id="update-results">Update Results</button>\n</div>';
  
  }
  return __p
  };
  
  this["JST"]["html/mylist/layout.html"] = function(obj) {
  obj || (obj = {});
  var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
  function print() { __p += __j.call(arguments, '') }
  with (obj) {
  __p += '\n<div>\n  <div id="filter-region">\n    <div id="filter-item-view">\n      <div class="row">\n        <div class="col-xs-6 mylist-heading">\n          <span class="trueAria" id="titleShow">Show</span>\n          <span id="empty"></span>\n        </div>\n        <div class="col-xs-4 pull-right text-right mylist-icons">\n          <button id="edit" class="icon-finalicons-B-08 mylist-header-btn" aria-label="Click to Edit">\n              <span class="hide-text">Edit MyList Titles</span>\n          </button>\n          <button id="save" class="icon-finalicons-B-18 mylist-header-btn disabled" aria-label="Click to Save">\n              <span class="hide-text">Save MyList Titles</span>\n          </button>\n          ';
   if(closeBtn) { ;
  __p += '\n            <button id="close" class="icon-finalicons-A-31 mylist-header-btn" aria-label="Click to Close">\n                <span class="hide-text">Close MyList</span>\n            </button>\n          ';
   } ;
  __p += '\n        </div>\n      </div>\n      <div id="my-list-filter-region"></div>\n    </div>\n  </div>\n  <div id="my-list-region" class="my-list-region"></div>\n</div>';
  
  }
  return __p
  };
  
  this["JST"]["html/mylist/my-list-patient-select.html"] = function(obj) {
  obj || (obj = {});
  var __t, __p = '', __e = _.escape;
  with (obj) {
  __p += '<div class="modal modal-sm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >\n    <div class="modal-dialog popup-dialog patient-detail-modal">\n        <div class="modal-content">\n            <div id="modalBox">\n                <button tabindex="0" class="popup-close close" data-dismiss="modal">\n                    <span aria-hidden="true" class="close-icon icon-finalicons-A-31"></span>\n                    <span class="sr-only">Close</span>\n                </button>\n                <div id="patient-dialog-data" >\n                    <div class="row">\n                        <div class="col-xs-3">Last</div>\n                        <div class="col-xs-8 right-col"><b>' +
  __e( obj.lastName ) +
  '</b></div>\n                    </div>\n\n                    <div class="row">\n                        <div class="col-xs-3">First</div>\n                        <div class="col-xs-8 right-col"><b>' +
  __e( obj.firstName ) +
  '</b></div>\n                    </div>\n\n                    <div class="row">\n                        <div class="col-xs-3">DOB</div>\n                        <div class="col-xs-8 right-col">\n                            <b>' +
  __e( obj.getDate() ) +
  '</b>\n                            <span class="age-dialog">Age &nbsp;<b>' +
  __e( obj.getAge() ) +
  '</b></span>\n                        </div>\n                    </div>\n\n                    <div class="row">\n                        <div class="col-xs-3">Gender</div>\n                        <div class="col-xs-8 right-col"><b>' +
  __e( obj.gender ) +
  '</b></div>\n                    </div>\n\n                    <div class="row">\n                        <div class="col-xs-3">Location</div>\n                        <div class="col-xs-8 right-col"><b>' +
  __e( obj.wardLocation ) +
  '</b></div>\n                    </div>\n\n                    <div class="row">\n                        <div class="col-xs-3">SSN</div>\n                        <div class="col-xs-8 right-col"><b>' +
  __e( obj.ssn ) +
  '</b></div>\n                    </div>\n\n                    <div class="patient-lists"></div>\n\n                </div>\n\n                <button tabindex="0" id="select-patient" class="btn-link">Select Patient</button>\n\n                <button aria-label="Click to expand for my list options." tabindex="0" id="expand-mylist" class="btn-link collapsed expand-mylist" type="button" data-toggle="collapse" data-target="#mylist-options"><i class="binary-icon-mylist" aria-hidden="true"></i> <span id="mylist-btn-txt">' +
  __e( obj.myListBtnText ) +
  '</span></button>\n\n                <div id="mylist-options" class="collapse out">\n                    <div class="note-section"></div>\n\n                    <button tabindex="0" role="button" id="update-patient-mylist" class="btn-link update-patient-mylist">Update</button>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n';
  
  }
  return __p
  };
  
  this["JST"]["html/mylist/mylist-patient-item.html"] = function(obj) {
  obj || (obj = {});
  var __t, __p = '', __e = _.escape;
  with (obj) {
  __p += '<div>\n\t<div class="mylist-number-icon col-xs-1 binary-icon-' +
  __e( obj.myLists ) +
  '" aria-hidden="true">\n\t</div>\n\t<div class="col-xs-11 text-left" aria-label="' +
  __e( obj.displayName ) +
  ' ' +
  __e( obj.getMyListAriaLabel() ) +
  '">\n\t\t' +
  __e( obj.displayName ) +
  '\n\t</div>\n</div>';
  
  }
  return __p
  };
  
  this["JST"]["html/mylist/mylist-view.html"] = function(obj) {
  obj || (obj = {});
  var __t, __p = '', __e = _.escape;
  with (obj) {
  __p += '<div>\n  <div class="mylist-result-controls row">\n    <div class="col-xs-2 text-left">\n      <button role="button" id="previous-btn-ml" tabindex="0" class="icon-finalicons-B-05 btn backward-btn" aria-label="View previous page of results">\n          <span class="hide-text">Previous Page</span>\n      </button>\n    </div>\n    <div class="col-xs-8 text-center">\n      <h5>0 Results</h5>\n    </div>\n    <div class="col-xs-2 text-right">\n      <button role="button" id="next-btn-ml" class="icon-finalicons-B-05 forward-btn btn" tabindex="0" aria-label="View next page of results">\n          <span class="hide-text">Next Page</span>\n      </button>\n    </div>\n  </div>\n  <div id="patient-search-list">\n    <div id="mylist-results" class="mylist-results">\n    </div>\n  </div>\n</div>';
  
  }
  return __p
  };
  
  this["JST"]["html/mylist/patient-list-item.html"] = function(obj) {
  obj || (obj = {});
  var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
  function print() { __p += __j.call(arguments, '') }
  with (obj) {
  __p += '<div class="drop-down-result row patient-name" >\n  <a role="dialog" href="#" class="display-name" style="font-size: 20px;" aria-label="' +
  __e( obj.displayName );
   if (exists) {;
  __p += '. Exists in Database.';
  };
  __p += '">' +
  __e( obj.displayName ) +
  '\n    ';
   if (exists) { ;
  __p += '\n    <span class="icon icon-check h3 patient-exists" aria-hidden="true"></span>\n    ';
   } ;
  __p += '\n  </a>\n</div>';
  
  }
  return __p
  };
  
  this["JST"]["html/mylist/patient-mylist-item.html"] = function(obj) {
  obj || (obj = {});
  var __t, __p = '', __e = _.escape;
  with (obj) {
  __p += '<li>' +
  __e( obj.name ) +
  '</li>';
  
  }
  return __p
  };
  
  this["JST"]["html/mylist/patient-mylists.html"] = function(obj) {
  obj || (obj = {});
  var __t, __p = '', __e = _.escape;
  with (obj) {
  __p += '<div class="row my-list-row">\n    <div class="col-xs-3 binary-icon-' +
  __e( obj.myLists ) +
  '"></div>\n    <div class="col-xs-8 right-col">\n        <ul></ul>\n    </div>\n</div>';
  
  }
  return __p
  };
  
  this["JST"]["html/mylist/patient-select-note.html"] = function(obj) {
  obj || (obj = {});
  var __t, __p = '', __e = _.escape, __j = Array.prototype.join;
  function print() { __p += __j.call(arguments, '') }
  with (obj) {
  __p += '<div class="note">\n    <div class="row">\n        <div class="col-xs-1">\n            <div class="greater-checkbox">\n                <div aria-checked="' +
  ((__t = ( obj.selected )) == null ? '' : __t) +
  '" class="square-parent ';
   if (selected) { ;
  __p += ' checked ';
   } ;
  __p += '">\n                    <button aria-label="add patient to ' +
  __e( obj.name ) +
  '" id="option-' +
  __e( obj.id ) +
  '" tabindex="0" role="checkbox"\n                            aria-live="polite" class="square-child">&nbsp;&#x2713;&nbsp;</button>\n                </div>\n            </div>\n        </div>\n        <div class="col-xs-8 mylist-name"><span class="list-name">' +
  __e( obj.name ) +
  '</span></div>\n        <button tabindex="0" aria-label="Click to edit the Notes Section."\n                class="';
   if (!selected) { ;
  __p += ' hidden ';
   } ;
  __p += ' col-xs-1 btn edit-note patient-details-pencil-btn"\n                id="edit-note-' +
  __e( obj.id ) +
  '" title="Edit My List Note">\n            <span class="hide-text">Edit My List Note</span>\n            <i class="icon icon-finalicons-B-08" aria-hidden="true"></i>\n        </button>\n    </div>\n    <div id="toggle-' +
  __e( obj.id ) +
  '" class="note-area collapse ';
   if (selected) { ;
  __p += ' in ';
   } else { ;
  __p += ' out ';
   } ;
  __p += '">\n        <div class="row">\n            <div class="col-xs-1"></div>\n            <div class="col-xs-10">\n                <label class="note-label" for="note-' +
  __e( obj.id ) +
  '">' +
  __e( obj.getNoteLabel() ) +
  '</label>\n                <textarea tabindex="0" maxlength="' +
  __e( obj.charLimit ) +
  '"\n                          class="';
   if (selected) { ;
  __p += ' border-none ';
   } ;
  __p += ' note-box" name="note" id="note-' +
  __e( obj.id ) +
  '"\n                          rows="4" ';
   if (selected) { ;
  __p += ' disabled ';
   } ;
  __p += ' >' +
  __e( obj.note ) +
  '</textarea>\n            </div>\n        </div>\n    </div>\n</div>\n';
  
  }
  return __p
  };
  /*
   backbone.paginator 2.0.0
   http://github.com/backbone-paginator/backbone.paginator
  
   Copyright (c) 2013 Jimmy Yuen Ho Wong and contributors
   Licensed under the MIT @license.
   */ //Passing root with the factory
  (function(_, Backbone) {
    "use strict";
    var _extend = _.extend;
    var _omit = _.omit;
    var _clone = _.clone;
    var _each = _.each;
    var _pick = _.pick;
    var _contains = _.contains;
    var _isEmpty = _.isEmpty;
    var _pairs = _.pairs;
    var _invert = _.invert;
    var _isArray = _.isArray;
    var _isFunction = _.isFunction;
    var _isObject = _.isObject;
    var _keys = _.keys;
    var _isUndefined = _.isUndefined;
    var ceil = Math.ceil;
    var floor = Math.floor;
    var max = Math.max;
    var BBColProto = Backbone.Collection.prototype;
    function finiteInt(val, name) {
      if (!_.isNumber(val) || _.isNaN(val) || !_.isFinite(val) || ~~val !== val) {
        throw new TypeError("`" + name + "` must be a finite integer");
      }
      return val;
    }
    function queryStringToParams(qs) {
      var kvp, k, v, ls, params = {}, decode = decodeURIComponent;
      var kvps = qs.split("&");
      for (var i = 0, l = kvps.length; i < l; i++) {
        var param = kvps[i];
        kvp = param.split("="), k = kvp[0], v = kvp[1] || true;
        k = decode(k), v = decode(v), ls = params[k];
        if (_isArray(ls)) ls.push(v); else if (ls) params[k] = [ ls, v ]; else params[k] = v;
      }
      return params;
    }
    // hack to make sure the whatever event handlers for this event is run
    // before func is, and the event handlers that func will trigger.
    function runOnceAtLastHandler(col, event, func) {
      var eventHandlers = col._events[event];
      if (eventHandlers && eventHandlers.length) {
        var lastHandler = eventHandlers[eventHandlers.length - 1];
        var oldCallback = lastHandler.callback;
        lastHandler.callback = function() {
          try {
            oldCallback.apply(this, arguments);
            func();
          } catch (e) {
            throw e;
          } finally {
            lastHandler.callback = oldCallback;
          }
        };
      } else func();
    }
    var PARAM_TRIM_RE = /[\s'"]/g;
    var URL_TRIM_RE = /[<>\s'"]/g;
    /**
     Drop-in replacement for Backbone.Collection. Supports server-side and
     client-side pagination and sorting. Client-side mode also support fully
     multi-directional synchronization of changes between pages.
  
     @class Backbone.PageableCollection
     @extends Backbone.Collection
     */
    var PageableCollection = Backbone.PageableCollection = Backbone.Collection.extend({
      /**
       The container object to store all pagination states.
  
       You can override the default state by extending this class or specifying
       them in an `options` hash to the constructor.
  
       @property {Object} state
  
       @property {0|1} [state.firstPage=1] The first page index. Set to 0 if
       your server API uses 0-based indices. You should only override this value
       during extension, initialization or reset by the server after
       fetching. This value should be read only at other times.
  
       @property {number} [state.lastPage=null] The last page index. This value
       is __read only__ and it's calculated based on whether `firstPage` is 0 or
       1, during bootstrapping, fetching and resetting. Please don't change this
       value under any circumstances.
  
       @property {number} [state.currentPage=null] The current page index. You
       should only override this value during extension, initialization or reset
       by the server after fetching. This value should be read only at other
       times. Can be a 0-based or 1-based index, depending on whether
       `firstPage` is 0 or 1. If left as default, it will be set to `firstPage`
       on initialization.
  
       @property {number} [state.pageSize=25] How many records to show per
       page. This value is __read only__ after initialization, if you want to
       change the page size after initialization, you must call #setPageSize.
  
       @property {number} [state.totalPages=null] How many pages there are. This
       value is __read only__ and it is calculated from `totalRecords`.
  
       @property {number} [state.totalRecords=null] How many records there
       are. This value is __required__ under server mode. This value is optional
       for client mode as the number will be the same as the number of models
       during bootstrapping and during fetching, either supplied by the server
       in the metadata, or calculated from the size of the response.
  
       @property {string} [state.sortKey=null] The model attribute to use for
       sorting.
  
       @property {-1|0|1} [state.order=-1] The order to use for sorting. Specify
       -1 for ascending order or 1 for descending order. If 0, no client side
       sorting will be done and the order query parameter will not be sent to
       the server during a fetch.
       */
      state: {
        firstPage: 1,
        lastPage: null,
        currentPage: null,
        pageSize: 25,
        totalPages: null,
        totalRecords: null,
        sortKey: null,
        order: -1
      },
      /**
       @property {"server"|"client"|"infinite"} [mode="server"] The mode of
       operations for this collection. `"server"` paginates on the server-side,
       `"client"` paginates on the client-side and `"infinite"` paginates on the
       server-side for APIs that do not support `totalRecords`.
       */
      mode: "server",
      /**
       A translation map to convert Backbone.PageableCollection state attributes
       to the query parameters accepted by your server API.
  
       You can override the default state by extending this class or specifying
       them in `options.queryParams` object hash to the constructor.
  
       @property {Object} queryParams
       @property {string} [queryParams.currentPage="page"]
       @property {string} [queryParams.pageSize="per_page"]
       @property {string} [queryParams.totalPages="total_pages"]
       @property {string} [queryParams.totalRecords="total_entries"]
       @property {string} [queryParams.sortKey="sort_by"]
       @property {string} [queryParams.order="order"]
       @property {string} [queryParams.directions={"-1": "asc", "1": "desc"}] A
       map for translating a Backbone.PageableCollection#state.order constant to
       the ones your server API accepts.
       */
      queryParams: {
        currentPage: "page",
        pageSize: "per_page",
        totalPages: "total_pages",
        totalRecords: "total_entries",
        sortKey: "sort_by",
        order: "order",
        directions: {
          "-1": "asc",
          "1": "desc"
        }
      },
      /**
       __CLIENT MODE ONLY__
  
       This collection is the internal storage for the bootstrapped or fetched
       models. You can use this if you want to operate on all the pages.
  
       @property {Backbone.Collection} fullCollection
       */
      /**
       Given a list of models or model attributues, bootstraps the full
       collection in client mode or infinite mode, or just the page you want in
       server mode.
  
       If you want to initialize a collection to a different state than the
       default, you can specify them in `options.state`. Any state parameters
       supplied will be merged with the default. If you want to change the
       default mapping from #state keys to your server API's query parameter
       names, you can specifiy an object hash in `option.queryParams`. Likewise,
       any mapping provided will be merged with the default. Lastly, all
       Backbone.Collection constructor options are also accepted.
  
       See:
  
       - Backbone.PageableCollection#state
       - Backbone.PageableCollection#queryParams
       - [Backbone.Collection#initialize](http://backbonejs.org/#Collection-constructor)
  
       @param {Array.<Object>} [models]
  
       @param {Object} [options]
  
       @param {function(*, *): number} [options.comparator] If specified, this
       comparator is set to the current page under server mode, or the #fullCollection
       otherwise.
  
       @param {boolean} [options.full] If `false` and either a
       `options.comparator` or `sortKey` is defined, the comparator is attached
       to the current page. Default is `true` under client or infinite mode and
       the comparator will be attached to the #fullCollection.
  
       @param {Object} [options.state] The state attributes overriding the defaults.
  
       @param {string} [options.state.sortKey] The model attribute to use for
       sorting. If specified instead of `options.comparator`, a comparator will
       be automatically created using this value, and optionally a sorting order
       specified in `options.state.order`. The comparator is then attached to
       the new collection instance.
  
       @param {-1|1} [options.state.order] The order to use for sorting. Specify
       -1 for ascending order and 1 for descending order.
  
       @param {Object} [options.queryParam]
       */
      constructor: function(models, options) {
        BBColProto.constructor.apply(this, arguments);
        options = options || {};
        var mode = this.mode = options.mode || this.mode || PageableProto.mode;
        var queryParams = _extend({}, PageableProto.queryParams, this.queryParams, options.queryParams || {});
        queryParams.directions = _extend({}, PageableProto.queryParams.directions, this.queryParams.directions, queryParams.directions || {});
        this.queryParams = queryParams;
        var state = this.state = _extend({}, PageableProto.state, this.state, options.state || {});
        state.currentPage = state.currentPage == null ? state.firstPage : state.currentPage;
        if (!_isArray(models)) models = models ? [ models ] : [];
        models = models.slice();
        if (mode != "server" && state.totalRecords == null && !_isEmpty(models)) {
          state.totalRecords = models.length;
        }
        this.switchMode(mode, _extend({
          fetch: false,
          resetState: false,
          models: models
        }, options));
        var comparator = options.comparator;
        if (state.sortKey && !comparator) {
          this.setSorting(state.sortKey, state.order, options);
        }
        if (mode != "server") {
          var fullCollection = this.fullCollection;
          if (comparator && options.full) {
            this.comparator = null;
            fullCollection.comparator = comparator;
          }
          if (options.full) fullCollection.sort();
          // make sure the models in the current page and full collection have the
          // same references
          if (models && !_isEmpty(models)) {
            this.reset(models, _extend({
              silent: true
            }, options));
            this.getPage(state.currentPage);
            models.splice.apply(models, [ 0, models.length ].concat(this.models));
          }
        }
        this._initState = _clone(this.state);
      },
      /**
       Makes a Backbone.Collection that contains all the pages.
  
       @private
       @param {Array.<Object|Backbone.Model>} models
       @param {Object} options Options for Backbone.Collection constructor.
       @return {Backbone.Collection}
       */
      _makeFullCollection: function(models, options) {
        var properties = [ "url", "model", "sync", "comparator" ];
        var thisProto = this.constructor.prototype;
        var i, length, prop;
        var proto = {};
        for (i = 0, length = properties.length; i < length; i++) {
          prop = properties[i];
          if (!_isUndefined(thisProto[prop])) {
            proto[prop] = thisProto[prop];
          }
        }
        var fullCollection = new (Backbone.Collection.extend(proto))(models, options);
        for (i = 0, length = properties.length; i < length; i++) {
          prop = properties[i];
          if (this[prop] !== thisProto[prop]) {
            fullCollection[prop] = this[prop];
          }
        }
        return fullCollection;
      },
      /**
       Factory method that returns a Backbone event handler that responses to
       the `add`, `remove`, `reset`, and the `sort` events. The returned event
       handler will synchronize the current page collection and the full
       collection's models.
  
       @private
  
       @param {Backbone.PageableCollection} pageCol
       @param {Backbone.Collection} fullCol
  
       @return {function(string, Backbone.Model, Backbone.Collection, Object)}
       Collection event handler
       */
      _makeCollectionEventHandler: function(pageCol, fullCol) {
        return function collectionEventHandler(event, model, collection, options) {
          var handlers = pageCol._handlers;
          _each(_keys(handlers), function(event) {
            var handler = handlers[event];
            pageCol.off(event, handler);
            fullCol.off(event, handler);
          });
          var state = _clone(pageCol.state);
          var firstPage = state.firstPage;
          var currentPage = firstPage === 0 ? state.currentPage : state.currentPage - 1;
          var pageSize = state.pageSize;
          var pageStart = currentPage * pageSize, pageEnd = pageStart + pageSize;
          if (event == "add") {
            var pageIndex, fullIndex, addAt, colToAdd, options = options || {};
            if (collection == fullCol) {
              fullIndex = fullCol.indexOf(model);
              if (fullIndex >= pageStart && fullIndex < pageEnd) {
                colToAdd = pageCol;
                pageIndex = addAt = fullIndex - pageStart;
              }
            } else {
              pageIndex = pageCol.indexOf(model);
              fullIndex = pageStart + pageIndex;
              colToAdd = fullCol;
              var addAt = !_isUndefined(options.at) ? options.at + pageStart : fullIndex;
            }
            if (!options.onRemove) {
              ++state.totalRecords;
              delete options.onRemove;
            }
            pageCol.state = pageCol._checkState(state);
            if (colToAdd) {
              colToAdd.add(model, _extend({}, options || {}, {
                at: addAt
              }));
              var modelToRemove = pageIndex >= pageSize ? model : !_isUndefined(options.at) && addAt < pageEnd && pageCol.length > pageSize ? pageCol.at(pageSize) : null;
              if (modelToRemove) {
                runOnceAtLastHandler(collection, event, function() {
                  pageCol.remove(modelToRemove, {
                    onAdd: true
                  });
                });
              }
            }
          }
          // remove the model from the other collection as well
          if (event == "remove") {
            if (!options.onAdd) {
              // decrement totalRecords and update totalPages and lastPage
              if (!--state.totalRecords) {
                state.totalRecords = null;
                state.totalPages = null;
              } else {
                var totalPages = state.totalPages = ceil(state.totalRecords / pageSize);
                state.lastPage = firstPage === 0 ? totalPages - 1 : totalPages || firstPage;
                if (state.currentPage > totalPages) state.currentPage = state.lastPage;
              }
              pageCol.state = pageCol._checkState(state);
              var nextModel, removedIndex = options.index;
              if (collection == pageCol) {
                if (nextModel = fullCol.at(pageEnd)) {
                  runOnceAtLastHandler(pageCol, event, function() {
                    pageCol.push(nextModel, {
                      onRemove: true
                    });
                  });
                } else if (!pageCol.length && state.totalRecords) {
                  pageCol.reset(fullCol.models.slice(pageStart - pageSize, pageEnd - pageSize), _extend({}, options, {
                    parse: false
                  }));
                }
                fullCol.remove(model);
              } else if (removedIndex >= pageStart && removedIndex < pageEnd) {
                if (nextModel = fullCol.at(pageEnd - 1)) {
                  runOnceAtLastHandler(pageCol, event, function() {
                    pageCol.push(nextModel, {
                      onRemove: true
                    });
                  });
                }
                pageCol.remove(model);
                if (!pageCol.length && state.totalRecords) {
                  pageCol.reset(fullCol.models.slice(pageStart - pageSize, pageEnd - pageSize), _extend({}, options, {
                    parse: false
                  }));
                }
              }
            } else delete options.onAdd;
          }
          if (event == "reset") {
            options = collection;
            collection = model;
            // Reset that's not a result of getPage
            if (collection == pageCol && options.from == null && options.to == null) {
              var head = fullCol.models.slice(0, pageStart);
              var tail = fullCol.models.slice(pageStart + pageCol.models.length);
              fullCol.reset(head.concat(pageCol.models).concat(tail), options);
            } else if (collection == fullCol) {
              if (!(state.totalRecords = fullCol.models.length)) {
                state.totalRecords = null;
                state.totalPages = null;
              }
              if (pageCol.mode == "client") {
                state.lastPage = state.currentPage = state.firstPage;
              }
              pageCol.state = pageCol._checkState(state);
              pageCol.reset(fullCol.models.slice(pageStart, pageEnd), _extend({}, options, {
                parse: false
              }));
            }
          }
          if (event == "sort") {
            options = collection;
            collection = model;
            if (collection === fullCol) {
              pageCol.reset(fullCol.models.slice(pageStart, pageEnd), _extend({}, options, {
                parse: false
              }));
            }
          }
          _each(_keys(handlers), function(event) {
            var handler = handlers[event];
            _each([ pageCol, fullCol ], function(col) {
              col.on(event, handler);
              var callbacks = col._events[event] || [];
              callbacks.unshift(callbacks.pop());
            });
          });
        };
      },
      /**
       Sanity check this collection's pagination states. Only perform checks
       when all the required pagination state values are defined and not null.
       If `totalPages` is undefined or null, it is set to `totalRecords` /
       `pageSize`. `lastPage` is set according to whether `firstPage` is 0 or 1
       when no error occurs.
  
       @private
  
       @throws {TypeError} If `totalRecords`, `pageSize`, `currentPage` or
       `firstPage` is not a finite integer.
  
       @throws {RangeError} If `pageSize`, `currentPage` or `firstPage` is out
       of bounds.
  
       @return {Object} Returns the `state` object if no error was found.
       */
      _checkState: function(state) {
        var mode = this.mode;
        var links = this.links;
        var totalRecords = state.totalRecords;
        var pageSize = state.pageSize;
        var currentPage = state.currentPage;
        var firstPage = state.firstPage;
        var totalPages = state.totalPages;
        if (totalRecords != null && pageSize != null && currentPage != null && firstPage != null && (mode == "infinite" ? links : true)) {
          totalRecords = finiteInt(totalRecords, "totalRecords");
          pageSize = finiteInt(pageSize, "pageSize");
          currentPage = finiteInt(currentPage, "currentPage");
          firstPage = finiteInt(firstPage, "firstPage");
          if (pageSize < 1) {
            throw new RangeError("`pageSize` must be >= 1");
          }
          totalPages = state.totalPages = ceil(totalRecords / pageSize);
          if (firstPage < 0 || firstPage > 1) {
            throw new RangeError("`firstPage must be 0 or 1`");
          }
          state.lastPage = firstPage === 0 ? max(0, totalPages - 1) : totalPages || firstPage;
          if (mode == "infinite") {
            if (!links[currentPage + ""]) {
              throw new RangeError("No link found for page " + currentPage);
            }
          } else if (currentPage < firstPage || totalPages > 0 && (firstPage ? currentPage > totalPages : currentPage >= totalPages)) {
            throw new RangeError("`currentPage` must be firstPage <= currentPage " + (firstPage ? ">" : ">=") + " totalPages if " + firstPage + "-based. Got " + currentPage + ".");
          }
        }
        return state;
      },
      /**
       Change the page size of this collection.
  
       Under most if not all circumstances, you should call this method to
       change the page size of a pageable collection because it will keep the
       pagination state sane. By default, the method will recalculate the
       current page number to one that will retain the current page's models
       when increasing the page size. When decreasing the page size, this method
       will retain the last models to the current page that will fit into the
       smaller page size.
  
       If `options.first` is true, changing the page size will also reset the
       current page back to the first page instead of trying to be smart.
  
       For server mode operations, changing the page size will trigger a #fetch
       and subsequently a `reset` event.
  
       For client mode operations, changing the page size will `reset` the
       current page by recalculating the current page boundary on the client
       side.
  
       If `options.fetch` is true, a fetch can be forced if the collection is in
       client mode.
  
       @param {number} pageSize The new page size to set to #state.
       @param {Object} [options] {@link #fetch} options.
       @param {boolean} [options.first=false] Reset the current page number to
       the first page if `true`.
       @param {boolean} [options.fetch] If `true`, force a fetch in client mode.
  
       @throws {TypeError} If `pageSize` is not a finite integer.
       @throws {RangeError} If `pageSize` is less than 1.
  
       @chainable
       @return {XMLHttpRequest|Backbone.PageableCollection} The XMLHttpRequest
       from fetch or this.
       */
      setPageSize: function(pageSize, options) {
        pageSize = finiteInt(pageSize, "pageSize");
        options = options || {
          first: false
        };
        var state = this.state;
        var totalPages = ceil(state.totalRecords / pageSize);
        var currentPage = totalPages ? max(state.firstPage, floor(totalPages * state.currentPage / state.totalPages)) : state.firstPage;
        state = this.state = this._checkState(_extend({}, state, {
          pageSize: pageSize,
          currentPage: options.first ? state.firstPage : currentPage,
          totalPages: totalPages
        }));
        return this.getPage(state.currentPage, _omit(options, [ "first" ]));
      },
      /**
       Switching between client, server and infinite mode.
  
       If switching from client to server mode, the #fullCollection is emptied
       first and then deleted and a fetch is immediately issued for the current
       page from the server. Pass `false` to `options.fetch` to skip fetching.
  
       If switching to infinite mode, and if `options.models` is given for an
       array of models, #links will be populated with a URL per page, using the
       default URL for this collection.
  
       If switching from server to client mode, all of the pages are immediately
       refetched. If you have too many pages, you can pass `false` to
       `options.fetch` to skip fetching.
  
       If switching to any mode from infinite mode, the #links will be deleted.
  
       @param {"server"|"client"|"infinite"} [mode] The mode to switch to.
  
       @param {Object} [options]
  
       @param {boolean} [options.fetch=true] If `false`, no fetching is done.
  
       @param {boolean} [options.resetState=true] If 'false', the state is not
       reset, but checked for sanity instead.
  
       @chainable
       @return {XMLHttpRequest|Backbone.PageableCollection} The XMLHttpRequest
       from fetch or this if `options.fetch` is `false`.
       */
      switchMode: function(mode, options) {
        if (!_contains([ "server", "client", "infinite" ], mode)) {
          throw new TypeError('`mode` must be one of "server", "client" or "infinite"');
        }
        options = options || {
          fetch: true,
          resetState: true
        };
        var state = this.state = options.resetState ? _clone(this._initState) : this._checkState(_extend({}, this.state));
        this.mode = mode;
        var self = this;
        var fullCollection = this.fullCollection;
        var handlers = this._handlers = this._handlers || {}, handler;
        if (mode != "server" && !fullCollection) {
          fullCollection = this._makeFullCollection(options.models || [], options);
          fullCollection.pageableCollection = this;
          this.fullCollection = fullCollection;
          var allHandler = this._makeCollectionEventHandler(this, fullCollection);
          _each([ "add", "remove", "reset", "sort" ], function(event) {
            handlers[event] = handler = _.bind(allHandler, {}, event);
            self.on(event, handler);
            fullCollection.on(event, handler);
          });
          fullCollection.comparator = this._fullComparator;
        } else if (mode == "server" && fullCollection) {
          _each(_keys(handlers), function(event) {
            handler = handlers[event];
            self.off(event, handler);
            fullCollection.off(event, handler);
          });
          delete this._handlers;
          this._fullComparator = fullCollection.comparator;
          delete this.fullCollection;
        }
        if (mode == "infinite") {
          var links = this.links = {};
          var firstPage = state.firstPage;
          var totalPages = ceil(state.totalRecords / state.pageSize);
          var lastPage = firstPage === 0 ? max(0, totalPages - 1) : totalPages || firstPage;
          for (var i = state.firstPage; i <= lastPage; i++) {
            links[i] = this.url;
          }
        } else if (this.links) delete this.links;
        return options.fetch ? this.fetch(_omit(options, "fetch", "resetState")) : this;
      },
      /**
       @return {boolean} `true` if this collection can page backward, `false`
       otherwise.
       */
      hasPreviousPage: function() {
        var state = this.state;
        var currentPage = state.currentPage;
        if (this.mode != "infinite") return currentPage > state.firstPage;
        return !!this.links[currentPage - 1];
      },
      /**
       @return {boolean} `true` if this collection can page forward, `false`
       otherwise.
       */
      hasNextPage: function() {
        var state = this.state;
        var currentPage = this.state.currentPage;
        if (this.mode != "infinite") return currentPage < state.lastPage;
        return !!this.links[currentPage + 1];
      },
      /**
       Fetch the first page in server mode, or reset the current page of this
       collection to the first page in client or infinite mode.
  
       @param {Object} options {@link #getPage} options.
  
       @chainable
       @return {XMLHttpRequest|Backbone.PageableCollection} The XMLHttpRequest
       from fetch or this.
       */
      getFirstPage: function(options) {
        return this.getPage("first", options);
      },
      /**
       Fetch the previous page in server mode, or reset the current page of this
       collection to the previous page in client or infinite mode.
  
       @param {Object} options {@link #getPage} options.
  
       @chainable
       @return {XMLHttpRequest|Backbone.PageableCollection} The XMLHttpRequest
       from fetch or this.
       */
      getPreviousPage: function(options) {
        return this.getPage("prev", options);
      },
      /**
       Fetch the next page in server mode, or reset the current page of this
       collection to the next page in client mode.
  
       @param {Object} options {@link #getPage} options.
  
       @chainable
       @return {XMLHttpRequest|Backbone.PageableCollection} The XMLHttpRequest
       from fetch or this.
       */
      getNextPage: function(options) {
        return this.getPage("next", options);
      },
      /**
       Fetch the last page in server mode, or reset the current page of this
       collection to the last page in client mode.
  
       @param {Object} options {@link #getPage} options.
  
       @chainable
       @return {XMLHttpRequest|Backbone.PageableCollection} The XMLHttpRequest
       from fetch or this.
       */
      getLastPage: function(options) {
        return this.getPage("last", options);
      },
      /**
       Given a page index, set #state.currentPage to that index. If this
       collection is in server mode, fetch the page using the updated state,
       otherwise, reset the current page of this collection to the page
       specified by `index` in client mode. If `options.fetch` is true, a fetch
       can be forced in client mode before resetting the current page. Under
       infinite mode, if the index is less than the current page, a reset is
       done as in client mode. If the index is greater than the current page
       number, a fetch is made with the results **appended** to #fullCollection.
       The current page will then be reset after fetching.
  
       @param {number|string} index The page index to go to, or the page name to
       look up from #links in infinite mode.
       @param {Object} [options] {@link #fetch} options or
       [reset](http://backbonejs.org/#Collection-reset) options for client mode
       when `options.fetch` is `false`.
       @param {boolean} [options.fetch=false] If true, force a {@link #fetch} in
       client mode.
  
       @throws {TypeError} If `index` is not a finite integer under server or
       client mode, or does not yield a URL from #links under infinite mode.
  
       @throws {RangeError} If `index` is out of bounds.
  
       @chainable
       @return {XMLHttpRequest|Backbone.PageableCollection} The XMLHttpRequest
       from fetch or this.
       */
      getPage: function(index, options) {
        var mode = this.mode, fullCollection = this.fullCollection;
        options = options || {
          fetch: false
        };
        var state = this.state, firstPage = state.firstPage, currentPage = state.currentPage, lastPage = state.lastPage, pageSize = state.pageSize;
        var pageNum = index;
        switch (index) {
         case "first":
          pageNum = firstPage;
          break;
  
         case "prev":
          pageNum = currentPage - 1;
          break;
  
         case "next":
          pageNum = currentPage + 1;
          break;
  
         case "last":
          pageNum = lastPage;
          break;
  
         default:
          pageNum = finiteInt(index, "index");
        }
        this.state = this._checkState(_extend({}, state, {
          currentPage: pageNum
        }));
        options.from = currentPage, options.to = pageNum;
        var pageStart = (firstPage === 0 ? pageNum : pageNum - 1) * pageSize;
        var pageModels = fullCollection && fullCollection.length ? fullCollection.models.slice(pageStart, pageStart + pageSize) : [];
        if ((mode == "client" || mode == "infinite" && !_isEmpty(pageModels)) && !options.fetch) {
          this.reset(pageModels, _omit(options, "fetch"));
          return this;
        }
        if (mode == "infinite") options.url = this.links[pageNum];
        return this.fetch(_omit(options, "fetch"));
      },
      /**
       Fetch the page for the provided item offset in server mode, or reset the current page of this
       collection to the page for the provided item offset in client mode.
  
       @param {Object} options {@link #getPage} options.
  
       @chainable
       @return {XMLHttpRequest|Backbone.PageableCollection} The XMLHttpRequest
       from fetch or this.
       */
      getPageByOffset: function(offset, options) {
        if (offset < 0) {
          throw new RangeError("`offset must be > 0`");
        }
        offset = finiteInt(offset);
        var page = floor(offset / this.state.pageSize);
        if (this.state.firstPage !== 0) page++;
        if (page > this.state.lastPage) page = this.state.lastPage;
        return this.getPage(page, options);
      },
      /**
       Overidden to make `getPage` compatible with Zepto.
  
       @param {string} method
       @param {Backbone.Model|Backbone.Collection} model
       @param {Object} [options]
  
       @return {XMLHttpRequest}
       */
      sync: function(method, model, options) {
        var self = this;
        if (self.mode == "infinite") {
          var success = options.success;
          var currentPage = self.state.currentPage;
          options.success = function(resp, status, xhr) {
            var links = self.links;
            var newLinks = self.parseLinks(resp, _extend({
              xhr: xhr
            }, options));
            if (newLinks.first) links[self.state.firstPage] = newLinks.first;
            if (newLinks.prev) links[currentPage - 1] = newLinks.prev;
            if (newLinks.next) links[currentPage + 1] = newLinks.next;
            if (success) success(resp, status, xhr);
          };
        }
        return (BBColProto.sync || Backbone.sync).call(self, method, model, options);
      },
      /**
       Parse pagination links from the server response. Only valid under
       infinite mode.
  
       Given a response body and a XMLHttpRequest object, extract pagination
       links from them for infinite paging.
  
       This default implementation parses the RFC 5988 `Link` header and extract
       3 links from it - `first`, `prev`, `next`. Any subclasses overriding this
       method __must__ return an object hash having only the keys
       above. However, simply returning a `next` link or an empty hash if there
       are no more links should be enough for most implementations.
  
       @param {*} resp The deserialized response body.
       @param {Object} [options]
       @param {XMLHttpRequest} [options.xhr] The XMLHttpRequest object for this
       response.
       @return {Object}
       */
      parseLinks: function(resp, options) {
        var links = {};
        var linkHeader = options.xhr.getResponseHeader("Link");
        if (linkHeader) {
          var relations = [ "first", "prev", "next" ];
          _each(linkHeader.split(","), function(linkValue) {
            var linkParts = linkValue.split(";");
            var url = linkParts[0].replace(URL_TRIM_RE, "");
            var params = linkParts.slice(1);
            _each(params, function(param) {
              var paramParts = param.split("=");
              var key = paramParts[0].replace(PARAM_TRIM_RE, "");
              var value = paramParts[1].replace(PARAM_TRIM_RE, "");
              if (key == "rel" && _contains(relations, value)) links[value] = url;
            });
          });
        }
        return links;
      },
      /**
       Parse server response data.
  
       This default implementation assumes the response data is in one of two
       structures:
  
       [
       {}, // Your new pagination state
       [{}, ...] // An array of JSON objects
       ]
  
       Or,
  
       [{}] // An array of JSON objects
  
       The first structure is the preferred form because the pagination states
       may have been updated on the server side, sending them down again allows
       this collection to update its states. If the response has a pagination
       state object, it is checked for errors.
  
       The second structure is the
       [Backbone.Collection#parse](http://backbonejs.org/#Collection-parse)
       default.
  
       **Note:** this method has been further simplified since 1.1.7. While
       existing #parse implementations will continue to work, new code is
       encouraged to override #parseState and #parseRecords instead.
  
       @param {Object} resp The deserialized response data from the server.
       @param {Object} the options for the ajax request
  
       @return {Array.<Object>} An array of model objects
       */
      parse: function(resp, options) {
        var newState = this.parseState(resp, _clone(this.queryParams), _clone(this.state), options);
        if (newState) this.state = this._checkState(_extend({}, this.state, newState));
        return this.parseRecords(resp, options);
      },
      /**
       Parse server response for server pagination state updates. Not applicable
       under infinite mode.
  
       This default implementation first checks whether the response has any
       state object as documented in #parse. If it exists, a state object is
       returned by mapping the server state keys to this pageable collection
       instance's query parameter keys using `queryParams`.
  
       It is __NOT__ neccessary to return a full state object complete with all
       the mappings defined in #queryParams. Any state object resulted is merged
       with a copy of the current pageable collection state and checked for
       sanity before actually updating. Most of the time, simply providing a new
       `totalRecords` value is enough to trigger a full pagination state
       recalculation.
  
       parseState: function (resp, queryParams, state, options) {
               return {totalRecords: resp.total_entries};
             }
  
       If you want to use header fields use:
  
       parseState: function (resp, queryParams, state, options) {
                 return {totalRecords: options.xhr.getResponseHeader("X-total")};
             }
  
       This method __MUST__ return a new state object instead of directly
       modifying the #state object. The behavior of directly modifying #state is
       undefined.
  
       @param {Object} resp The deserialized response data from the server.
       @param {Object} queryParams A copy of #queryParams.
       @param {Object} state A copy of #state.
       @param {Object} [options] The options passed through from
       `parse`. (backbone >= 0.9.10 only)
  
       @return {Object} A new (partial) state object.
       */
      parseState: function(resp, queryParams, state, options) {
        if (resp && resp.length === 2 && _isObject(resp[0]) && _isArray(resp[1])) {
          var newState = _clone(state);
          var serverState = resp[0];
          _each(_pairs(_omit(queryParams, "directions")), function(kvp) {
            var k = kvp[0], v = kvp[1];
            var serverVal = serverState[v];
            if (!_isUndefined(serverVal) && !_.isNull(serverVal)) newState[k] = serverState[v];
          });
          if (serverState.order) {
            newState.order = _invert(queryParams.directions)[serverState.order] * 1;
          }
          return newState;
        }
      },
      /**
       Parse server response for an array of model objects.
  
       This default implementation first checks whether the response has any
       state object as documented in #parse. If it exists, the array of model
       objects is assumed to be the second element, otherwise the entire
       response is returned directly.
  
       @param {Object} resp The deserialized response data from the server.
       @param {Object} [options] The options passed through from the
       `parse`. (backbone >= 0.9.10 only)
  
       @return {Array.<Object>} An array of model objects
       */
      parseRecords: function(resp, options) {
        if (resp && resp.length === 2 && _isObject(resp[0]) && _isArray(resp[1])) {
          return resp[1];
        }
        return resp;
      },
      /**
       Fetch a page from the server in server mode, or all the pages in client
       mode. Under infinite mode, the current page is refetched by default and
       then reset.
  
       The query string is constructed by translating the current pagination
       state to your server API query parameter using #queryParams. The current
       page will reset after fetch.
  
       @param {Object} [options] Accepts all
       [Backbone.Collection#fetch](http://backbonejs.org/#Collection-fetch)
       options.
  
       @return {XMLHttpRequest}
       */
      fetch: function(options) {
        options = options || {};
        var state = this._checkState(this.state);
        var mode = this.mode;
        if (mode == "infinite" && !options.url) {
          options.url = this.links[state.currentPage];
        }
        var data = options.data || {};
        // dedup query params
        var url = options.url || this.url || "";
        if (_isFunction(url)) url = url.call(this);
        var qsi = url.indexOf("?");
        if (qsi != -1) {
          _extend(data, queryStringToParams(url.slice(qsi + 1)));
          url = url.slice(0, qsi);
        }
        options.url = url;
        options.data = data;
        // map params except directions
        var queryParams = this.mode == "client" ? _pick(this.queryParams, "sortKey", "order") : _omit(_pick(this.queryParams, _keys(PageableProto.queryParams)), "directions");
        var i, kvp, k, v, kvps = _pairs(queryParams), thisCopy = _clone(this);
        for (i = 0; i < kvps.length; i++) {
          kvp = kvps[i], k = kvp[0], v = kvp[1];
          v = _isFunction(v) ? v.call(thisCopy) : v;
          if (state[k] != null && v != null) {
            data[v] = state[k];
          }
        }
        // fix up sorting parameters
        if (state.sortKey && state.order) {
          var o = _isFunction(queryParams.order) ? queryParams.order.call(thisCopy) : queryParams.order;
          data[o] = this.queryParams.directions[state.order + ""];
        } else if (!state.sortKey) delete data[queryParams.order];
        // map extra query parameters
        var extraKvps = _pairs(_omit(this.queryParams, _keys(PageableProto.queryParams)));
        for (i = 0; i < extraKvps.length; i++) {
          kvp = extraKvps[i];
          v = kvp[1];
          v = _isFunction(v) ? v.call(thisCopy) : v;
          if (v != null) data[kvp[0]] = v;
        }
        if (mode != "server") {
          var self = this, fullCol = this.fullCollection;
          var success = options.success;
          options.success = function(col, resp, opts) {
            // make sure the caller's intent is obeyed
            opts = opts || {};
            if (_isUndefined(options.silent)) delete opts.silent; else opts.silent = options.silent;
            var models = col.models;
            if (mode == "client") fullCol.reset(models, opts); else {
              fullCol.add(models, _extend({
                at: fullCol.length
              }, _extend(opts, {
                parse: false
              })));
              self.trigger("reset", self, opts);
            }
            if (success) success(col, resp, opts);
          };
          // silent the first reset from backbone
          return BBColProto.fetch.call(this, _extend({}, options, {
            silent: true
          }));
        }
        return BBColProto.fetch.call(this, options);
      },
      /**
       Convenient method for making a `comparator` sorted by a model attribute
       identified by `sortKey` and ordered by `order`.
  
       Like a Backbone.Collection, a Backbone.PageableCollection will maintain
       the __current page__ in sorted order on the client side if a `comparator`
       is attached to it. If the collection is in client mode, you can attach a
       comparator to #fullCollection to have all the pages reflect the global
       sorting order by specifying an option `full` to `true`. You __must__ call
       `sort` manually or #fullCollection.sort after calling this method to
       force a resort.
  
       While you can use this method to sort the current page in server mode,
       the sorting order may not reflect the global sorting order due to the
       additions or removals of the records on the server since the last
       fetch. If you want the most updated page in a global sorting order, it is
       recommended that you set #state.sortKey and optionally #state.order, and
       then call #fetch.
  
       @protected
  
       @param {string} [sortKey=this.state.sortKey] See `state.sortKey`.
       @param {number} [order=this.state.order] See `state.order`.
       @param {(function(Backbone.Model, string): Object) | string} [sortValue] See #setSorting.
  
       See [Backbone.Collection.comparator](http://backbonejs.org/#Collection-comparator).
       */
      _makeComparator: function(sortKey, order, sortValue) {
        var state = this.state;
        sortKey = sortKey || state.sortKey;
        order = order || state.order;
        if (!sortKey || !order) return;
        if (!sortValue) sortValue = function(model, attr) {
          return model.get(attr);
        };
        return function(left, right) {
          var l = sortValue(left, sortKey), r = sortValue(right, sortKey), t;
          if (order === 1) t = l, l = r, r = t;
          if (l === r) return 0; else if (l < r) return -1;
          return 1;
        };
      },
      /**
       Adjusts the sorting for this pageable collection.
  
       Given a `sortKey` and an `order`, sets `state.sortKey` and
       `state.order`. A comparator can be applied on the client side to sort in
       the order defined if `options.side` is `"client"`. By default the
       comparator is applied to the #fullCollection. Set `options.full` to
       `false` to apply a comparator to the current page under any mode. Setting
       `sortKey` to `null` removes the comparator from both the current page and
       the full collection.
  
       If a `sortValue` function is given, it will be passed the `(model,
       sortKey)` arguments and is used to extract a value from the model during
       comparison sorts. If `sortValue` is not given, `model.get(sortKey)` is
       used for sorting.
  
       @chainable
  
       @param {string} sortKey See `state.sortKey`.
       @param {number} [order=this.state.order] See `state.order`.
       @param {Object} [options]
       @param {"server"|"client"} [options.side] By default, `"client"` if
       `mode` is `"client"`, `"server"` otherwise.
       @param {boolean} [options.full=true]
       @param {(function(Backbone.Model, string): Object) | string} [options.sortValue]
       */
      setSorting: function(sortKey, order, options) {
        var state = this.state;
        state.sortKey = sortKey;
        state.order = order = order || state.order;
        var fullCollection = this.fullCollection;
        var delComp = false, delFullComp = false;
        if (!sortKey) delComp = delFullComp = true;
        var mode = this.mode;
        options = _extend({
          side: mode == "client" ? mode : "server",
          full: true
        }, options);
        var comparator = this._makeComparator(sortKey, order, options.sortValue);
        var full = options.full, side = options.side;
        if (side == "client") {
          if (full) {
            if (fullCollection) fullCollection.comparator = comparator;
            delComp = true;
          } else {
            this.comparator = comparator;
            delFullComp = true;
          }
        } else if (side == "server" && !full) {
          this.comparator = comparator;
        }
        if (delComp) this.comparator = null;
        if (delFullComp && fullCollection) fullCollection.comparator = null;
        return this;
      }
    });
    var PageableProto = PageableCollection.prototype;
    return PageableCollection;
  })(_, Backbone);
  

  var JST = this['JST'];
  var LV = {};
  LV.VERSION = '2.19.13';
  LV.Views = {};
  LV.Models = {};
  LV.Collections = {};
  LV.noConflict = function() {
    root.LV = previousLV;
    return this;
  };

  LV.LoginUtils = function (options) {
    options = options || {};
  
    var appName = options.appName || '';
    var resources = options.resources || { get: function () { return null; } };
    var resourcesLink = options.resourcesLink || '';
    var authorizeUrl = resources.get('oauth-authorize') ? resources.get('oauth-authorize').get('href') : options.authorizeUrl || '';
    var lastAccessUrl = resources.get('last-accesstime') ? resources.get('last-accesstime').get('href') : options.lastAccessUrl || '';
    var noToken = typeof options.noToken === 'function' ? options.noToken : function () { };
    var autoRedirect = options.autoRedirect || false;
    var redirectUri = options.redirectUri || '';
    var onReceiveToken = typeof options.onReceiveToken === 'function' ? 
      options.onReceiveToken : 
      function (token) {
        $.ajaxPrefilter(function (options, originalOptions, jqXHR) {
          jqXHR.setRequestHeader('Authorization', 'Bearer ' + token);
        });
      };
    var that = this;
  
    var clientId;
    var generateState = function () {
      var state = appName + '-' + new Date().getTime();
  
      window.sessionStorage.setItem(appName + '-state', state);
  
      return state;
    };
  
    var getParams = function (url) {
      url = url || window.location.href;
  
      var params = {},
        queryString = url.split('?')[1] || '',
        queryParams = queryString.split('&') || [];
  
      queryParams.forEach(function(param) {
        param = param.split('=');
        
        params[decodeURIComponent(param[0])] = decodeURIComponent(param[1]);
      });
  
      return params;
    };
  
    var authorizeCordova = function (url) {
      var ref = window.open(url, '_blank', 'location=no,toolbar=no');
      ref.addEventListener('loadstart', function (e) {
        var params = getParams(e.url);
  
        var code = params.code;
        var clientState = params.state;
  
        if (code && clientState) {
          window.sessionStorage.setItem('code', code);
          window.sessionStorage.setItem('clientState', clientState);
          window.location.reload();
          ref.close();
        }
      });
    };
  
    $.ajax({
      url: resourcesLink + '/oauth/info',
      async: false,
      success: function (response) {
        clientId = response.clientId || '';
        redirectUri = redirectUri || response.redirectUri || '';
      }
    });
  
    this.authorize = function () {
      var authorizeParams = [
        'response_type=code',
        'state=' + generateState(),
        'client_id=' + clientId,
        'redirect_uri=' + redirectUri,
        'scope=read'
      ];
  
      var url = authorizeUrl + '?' + authorizeParams.join('&');
  
      if (window.cordova) {
        authorizeCordova(url);
      } else {
        window.location = url;
      }
    };
  
    this.checkForAuthCode = function () {
      var params = getParams();
  
      var code = window.sessionStorage.getItem('code') || params.code;
      var clientState = window.sessionStorage.getItem('clientState') || params.state;
      var sessionState = window.sessionStorage.getItem(appName + '-state');
      var token;
  
      window.sessionStorage.removeItem('code');
      window.sessionStorage.removeItem('clientState');
      window.sessionStorage.removeItem(appName + '-state');
      
      if (!window.cordova) {
        window.history.replaceState({}, '', '/' + appName + '/' + window.location.hash);
      }
  
      if (!code || !clientState) {
        try {
          token = JSON.parse(window.sessionStorage.getItem('token'));
        } catch (e) {
          token = window.sessionStorage.getItem('token');
        }
      } else if (code && clientState && clientState === sessionState) {
        resourcesLink += '/oauth/token?code=' + code + '&redirect_uri=' + redirectUri;
  
        $.ajax({
          url: resourcesLink,
          async: false,
          headers: {
            'accept': 'application/json'
          },
          success: function (response) {
            token = response.access_token;
  
            window.sessionStorage.setItem('token', JSON.stringify(token));
          }
        });
      }
  
      if (token) {
        onReceiveToken(token);
      } else {
        noToken();
        if (autoRedirect) {
          this.authorize();
        }
      }
    };
  };
  //Ajax Loader
  // ---------
  
  var appendHTML = function ($el) {
    $el.append('<div class="hide-me ajax-loading" aria-label="loading results"><span class="loader-icon loading"></span></div>');
  };
  
  LV.AjaxLoader = function (opts) {
    var options = opts || {};
    this.$el = options.el || $('body');
    appendHTML(this.$el);
    this.$loader = options.loader || $('.ajax-loading');
    this.initialize.apply(this, arguments);
  };
  
  _.extend(LV.AjaxLoader.prototype, Backbone.Events, {
    initialize: function () {
      var _self = this;
      $(document).ajaxStart(function () {
        _self.show();
      }).ajaxStop(function () {
        _self.hide();
      }).ajaxError(function (event, jqxhr, settings, thrownError) {
        if (thrownError === 'timeout') {
          _self.trigger('timeout');
        }
      });
    },
    show: function () {
      this.$loader.show();
    },
    hide: function () {
      this.$loader.hide();
    }
  });
  
  //Dateman
  // ---------
  
  var months = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'];
  var monthsShort = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
  var daysShort = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  var daysMin = ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'];
  var formatParts = /dd?|DD?|mm?|MM?|yy(?:yy)?/g;
  
  var _DEFAULT_DATE = new Date(-2209057200000);
  var _MS_PER_WEEK = 1000 * 60 * 60 * 24 * 7;
  var _GESTATION = 280;
  
  LV.Dateman = function (opts) {
    var options = opts || {};
    this.initialize.apply(this, arguments);
  };
  
  _.extend(LV.Dateman.prototype, {
    initialize: function () {},
    getGestation: function () { return _GESTATION; },
    getDefaultDate: function () { return _DEFAULT_DATE; },
    toUTC: function (date) {
      return Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(),
        date.getSeconds(), date.getMilliseconds());
    },
    getZeroHourDate: function (date) {
      if (!date) {
        date = new Date();
      }
      date.setHours(0, 0, 0, 0);
      return date;
    },
    isDefaultDate: function (date) {
      if (date === _DEFAULT_DATE.getTime() || date === _DEFAULT_DATE.toISOString()) {
        return true;
      }
      else if (date instanceof Date) {
        return (+date === +_DEFAULT_DATE);
      }
      else { return false; }
    },
    changeDate: function (e) {
      var calendar = this.convertDate(e.date);
      $(e.target).parent().children('.dateform').val(calendar).trigger('change');
    },
    convertDate: function (date, format) {
      if (!(date instanceof Date)) { return ''; }
      if (!format) { format = 'mm/dd/yyyy'; }
      if (typeof format === 'string') {
        format = this.parseFormat(format);
      }
      var val = {
        d: date.getUTCDate(),
        D: daysShort[date.getUTCDay()],
        DD: days[date.getUTCDay()],
        m: date.getUTCMonth() + 1,
        M: monthsShort[date.getUTCMonth()],
        MM: months[date.getUTCMonth()],
        yy: date.getUTCFullYear().toString().substring(2),
        yyyy: date.getUTCFullYear()
      };
      val.dd = (val.d < 10 ? '0' : '') + val.d;
      val.mm = (val.m < 10 ? '0' : '') + val.m;
      date = [];
      var seps = $.extend([], format.separators);
      for (var i = 0, cnt = format.parts.length; i <= cnt; i++) {
        if (seps.length) {
          date.push(seps.shift());
        }
        date.push(val[format.parts[i]]);
      }
      return date.join('');
    },
    parseFormat: function (format) {
      var separators = format.replace(formatParts, '\0').split('\0');
      var parts = format.match(formatParts);
      if (!separators || !separators.length || !parts || parts.length === 0) {
        throw new Error('Invalid date format.');
      }
      return {separators: separators, parts: parts};
    },
    validateDate: function (datestr) {
      if (datestr instanceof Date) {
        return datestr;
      }
      if ($.isNumeric(datestr)) {
        return new Date(datestr);
      }
      var day, datearr;
      datearr = datestr.split(/\W+/).map(function (itm) {
        if ($.isNumeric(itm)) {
          return parseInt(itm, 10);
        }
        else {
          for (var i = 0; i < months.length; i++) {
            if (itm.toLowerCase().indexOf(months[i]) > -1) {
              return i + 1;
            }
          }
          return NaN;
        }
      });
      try {
        day = new Date(datearr[2], datearr[0] - 1, datearr[1]);
        if (day.getUTCMonth() + 1 === datearr[0] && day.getUTCDate() === datearr[1]) {
          return day;
        }
        throw 'Bad Date Format';
      }
      catch (err) {
        return NaN;
      }
    },
    dateFieldIsValid: function (field) {
      var val = field.val();
      var yearCheck = val.match(/\/\d\d$/g);
      if (yearCheck !== null) {
        yearCheck = yearCheck[0].substring(1);
        val = val.replace(/\/\d\d$/g, '/20' + yearCheck);
        field.val(val);
      }
      var dat = this.validateDate(val);
      var $helpSpace = field.parent().siblings('.help-block').children('.date-validate').first();
      if (dat) {
        var helpText = this.convertDate(dat, 'D mm/dd/yyyy');
        $helpSpace.removeClass('text-danger').attr('aria-live', 'polite')
          .text(helpText);
        return true;
      } else if (field.val().length > 0) {
        $helpSpace.addClass('text-danger').attr('aria-live', 'assertive')
          .text('Not a valid date.');
        return false;
      } else {
        $helpSpace.removeClass('text-danger').attr('aria-live', 'polite')
          .html('&nbsp');
        return false;
      }
    },
    addDays: function (date, numdays) {
      if (date) {
        return date.addDays(numdays);
      }
    },
    dateDiffInWeeks: function (a, b) {
      var utc1 = Date.UTC(a.getUTCFullYear(), a.getUTCMonth(), a.getUTCDate());
      var utc2 = Date.UTC(b.getUTCFullYear(), b.getUTCMonth(), b.getUTCDate());
  
      return Math.floor((utc2 - utc1) / _MS_PER_WEEK);
    },
    calculateAge: function (date) {
      var today = new Date();
      var age = today.getUTCFullYear() - date.getUTCFullYear();
      var m = today.getUTCMonth() - date.getUTCMonth();
      if ((m < 0) || (m === 0 && today.getUTCDate() < date.getUTCDate())) {
        age--;
      }
      return age;
    }
  });
  
  Date.prototype.addDays = function (numdays) {
    var dat = new Date(this.valueOf());
    dat.setDate(dat.getUTCDate() + numdays);
    return dat;
  };
  
  
  //Resources
  // ---------
    
  var getJSON = function (path) {
    var response = $.ajax({ url: path, dataType: 'json', async: false });
    return JSON.parse(response.responseText);
  };
  
  var cleanUpSession = function () {
    window.sessionStorage.token = null;
    $.ajaxPrefilter(function (options, originalOptions, jqXHR) {
      jqXHR.setRequestHeader('Authorization', '');
    });
    document.cookie = encodeURIComponent('JSESSIONID') + '=deleted; expires=' + new Date(0).toUTCString();
  };
  
  LV.Resources = function (opts) {
    var options = opts || {};
    this.loginUtilsOpts = opts.loginUtilsOpts || {};
    this.path = options.path || 'MobileHealthPlatformWeb';
    this._protocol = options._protocol || '';
    this._host = options._host || '';
    this.user = {};
    this.directory = {};
    this.store = {
      pool: {},
      put: function (tag, objectFunc) {
        this.pool[tag] = objectFunc;
      },
      get: function (tag) {
        if (typeof this.pool[tag] === 'function') {
          this.pool[tag] = new this.pool[tag]();
        }
        return this.pool[tag];
      }
    };
    this.initialize.apply(this, arguments);
  };
  
  _.extend(LV.Resources.prototype, {
    initialize: function () {
      var path = this.getResourcePath();
      var protocol = this._protocol ? this._protocol + '//' : '';
      
      this.loginUtils = new LV.LoginUtils({
        appName: this.loginUtilsOpts.appName,
        resourcesLink: protocol + this._host + this.loginUtilsOpts.resourcesLink,
        redirectUri: this.loginUtilsOpts.redirectUri ? protocol + this._host + this.loginUtilsOpts.redirectUri : window.location.protocol + '//' + window.location.host + window.location.pathname,
        noToken: cleanUpSession,
        authorizeUrl: protocol + this._host + this.loginUtilsOpts.authorizeUrl,
        lastAccessUrl: protocol + this._host + this.loginUtilsOpts.lastAccessUrl,
        autoRedirect: this.loginUtilsOpts.autoRedirect,
        onReceiveToken: this.loginUtilsOpts.onReceiveToken
      });
      this.directory = getJSON(path).link;
      this.loginUtils.checkForAuthCode();
      this.fetch('public-user-session');
  
      this.user = (this.isLoggedIn()) ? this.store.get('public-user-session').mhpuser : {};
    },
    get: function (tag) {
      return _.first(this.directory.filter(function (obj) { return obj.title === tag; }));
    },
    getResourcePath: function () {
      return (window.cordova ? this._protocol : window.location.protocol) + '//' + (window.cordova ? this._host : window.location.host) + '/' + this.path + '/rest/public/resource-directory';
    },
    getRedirectURI: function () {
      return '?redirect_uri=' + window.location.protocol + '//' + window.location.host + window.location.pathname;
    },
    fetch: function (tag) {
      this.store.put(tag, getJSON(this.get(tag).href));
    },
    instance: function (link) {
      return {
        directory: link,
        get: this.get
      };
    },
    encrypt: function (name) {
      return name.replace(/[a-z0-9]/gi, function (a) { return a.charCodeAt(0) - 11 + '|'; }).slice(0, -1);
    },
    getEncryptedUserId: function () {
      return this.encrypt(this.user.id || '');
    },
    checkToken: function () {
      var storedToken = window.sessionStorage.token;
  
      if (typeof storedToken !== 'undefined' && storedToken !== 'undefined' && storedToken !== null && storedToken !== 'null') {
        return true;
      } else {
        return false;
      }
    },
    isLoggedIn: function () {
      return !!(this.store.get('public-user-session').mhpuser) && this.checkToken();
    },
    getUserName: function () {
      return this.user.displayName || '';
    },
    login: function () {
      this.loginUtils.authorize();
    },
    cleanSession: cleanUpSession,
    logoutAsProvider: function () {
      var tokenUrl = this.get('token').href;
      var logoutUrl = window.location.protocol + '//' + window.location.host + '/AuthorizationServices/logout'; //this.get('logout').href;
  
      $.ajax({
        url: tokenUrl,
        type: 'DELETE',
        success: function () {
          cleanUpSession();
          window.location = logoutUrl;
        },
        error: function () {
          console.log('failed to delete authorization token');
        }
      });
  
    },
    logout: function () {
      var tokenUrl = this.get('token').href,
        logoutUrl = this.get('logout').href,
        ref;
      if (window.cordova) { ref = window.open(logoutUrl, '_blank', 'location=no,toolbar=no'); }
      $.ajax({
        url: tokenUrl,
        type: 'DELETE',
        success: function () {
          if (window.cordova) {
            ref.close();
            window.location.reload();
          } else {
            window.location = logoutUrl;
          }
        },
        error: function () {
          console.in('failed to delete authorization token');
        }
      });
    },
    checkROA: function () {
      if (this.isLoggedIn()) {
        if (!this.user.rightOfAccessAccepted) {
          window.location = this.get('roa').href + this.getRedirectURI();
        }
      }
    }
  });
  //Session
  // ---------
  /*jshint -W117 */
  
  LV.Session = function (opts) {
    var options = opts || {};
    this.resources = options.resources || new LV.Resources();
    this.showWarningInterval = options.showWarningInterval || 180000;
    this.sleepCheckInterval = options.sleepCheckInterval || 20000;
    this.sleepCheckThreshold = options.sleepCheckThreshold || 60000;
    this.initialize.apply(this, arguments);
  };
  
  _.extend(LV.Session.prototype, Backbone.Events, {
    initialize: function () {
      if (this.resources.isLoggedIn()) {
        this.startSessionTimer();
        var that = this;
        $(document).on('ajaxSuccess', function () {
  	      var reset = _.bind(that.resetSessionTimer, that);
  	      reset(that.startSessionTimer());
        });
        this.startSessionSleepTimer();
      }
    },
    startSessionTimer: function() {
      var timeLeft = this.getTimeToExpireInMilliSeconds();
      if (this.warningTimer) {
        clearTimeout(this.warningTimer);
      }
      if (this.endedTimer) {
        clearTimeout(this.endedTimer);
      }
      var _self = this;
      if (timeLeft > 0) {
        this.warningTimer = window.setTimeout(function () {
  	      _self.trigger('session:warning', _self.showWarningInterval);
        }, timeLeft - _self.showWarningInterval);
        this.endedTimer = window.setTimeout(function () {
  	      _self.trigger('session:ended');
        }, timeLeft);
      } else {
          _self.trigger('session:ended'); //session has ended. let em know!
      }
    },
    startSessionSleepTimer: function () {
      this.lastActiveTime = new Date().getTime();
      this.sessionSleepThread();
    },
    sessionSleepThread: function () {
      var _self = this;
      window.setTimeout(function () {
        var currentTime = new Date().getTime();
        var diff = currentTime - _self.lastActiveTime - _self.sleepCheckInterval;
        if (diff > _self.sleepCheckThreshold) {
          _self.trigger('session:wokeUp');
        }
        _self.lastActiveTime = currentTime;
        window.setTimeout(_self.sessionSleepThread, _self.sleepCheckInterval);
      }, this.sleepCheckInterval);
    },
    resetSessionTimer: function(callback) {
      var cb = callback || function () {};
      var path = this.resources.get('user-session').href;
      var that = this;
      $.ajax({
        url: path,
        dataType: 'json',
        global: false,
        success: function () {
  		if (typeof cb === 'function') {
  		    var bound = _.bind(cb, that);
  		    bound();
  		}
        },
        error: function () {
  		cleanUpSession();
  		that.trigger('session:ended');
        }
      });
    },
    cleanUpSession: function () {
      if(this.warningTimer) {
        clearTimeout(this.warningTimer);
      }
      if (this.endedTimer) {
        clearTimeout(this.endedTimer);
      }
      if (this.sessionSleepThread) {
        clearTimeout(this.sessionSleepThread);
      }
      cleanUpSession();
    },
    getTimeToExpireInMilliSeconds: function () {
  	    var path = this.resources.get('last-accesstime').href;
  	    var that = this;
  	    var response = $.ajax({url: path, dataType: 'json', async: false, global: false, error: function () {
  			that.vent.trigger('session:ended');
  		    }});
  
  	    var result;
  	    if (response.status === 200) {
  		var json = JSON.parse(response.responseText);
  		result = json.timeToExpireInSeconds;
  	    } else {
  		result = '0';
  	    }
  
  	    return parseInt(result) * 1000;
    }
  });
  
  //Popup Region
  // ---------
  
  LV.PopupRegion = Backbone.Marionette.Region.extend({
    currentView: [],
    initialize: function (opts) {
      this.options = opts || {};
    },
    open: function (view) {
      if (typeof this.ensureEl === "function") {
        this.ensureEl();
      } else if (typeof this._ensureElement === "function") {
        this._ensureElement();
      }
      this.$el.append(view.el);
    },
    close: function (views) {
        if (typeof views === 'object') {
          views = [views];
        }
        else if (!views || !_.isArray(views)) {
          views = this.currentView;
        }
  
        _.each(views, this._closeView, this);
  
        this._removeViews(views);
        Backbone.Marionette.triggerMethod.call(this, 'close', views);
  
        return this;
      },
  
      show: function (views) {
        if (typeof views === 'object') {
          views = [views];
        }
        else if (!views || !_.isArray(views)) {
          this.renderAll();
          return this;
        }
  
        _.each(views, this._showView, this);
  
        this._addViews(views);
        Backbone.Marionette.triggerMethod.call(this, 'show', views);
  
        return this;
      },
  
      _closeView: function (view) {
        if (view.close) {
          view.close();
        }
        else {
          view.remove();
        }
  
        Backbone.Marionette.triggerMethod.call(this, 'close', view);
      },
  
      _showView: function (view) {
        view.render();
        this.open(view);
  
        Backbone.Marionette.triggerMethod.call(view, 'show');
        Backbone.Marionette.triggerMethod.call(this, 'show', view);
      },
  
      _removeViews: function (views) {
        this.currentView = _.difference(this.currentView, views);
      },
  
      _addViews: function (views) {
        _.union(this.currentView, views);
      },
  
      attachView: function (view) {
        this.open(view);
        this.currentView.push(view);
  
        return this;
      },
  
      renderAll: function () {
        _.each(this.currentView, function (view) {
          view.render();
        });
  
        return this;
      }
  });
  

  //Eula View
  // ---------
  
  LV.Views.EulaView = Backbone.Marionette.ItemView.extend({
    id: 'eula',
    tagName: 'div',
    template: function () {
      return JST['html/lv-view-eula.html']();
    },
    events: {
      'click #accept-button': 'accept',
      'click #decline-button': 'decline'
    },
    eulaDate: function () {
      return $(this.template()).find('#modified-date').text();
    },
    initialize: function (opts) {
      var options = opts || {};
      this.app = options.app || '';
      this.isUserSpecific = options.isUserSpecific || false;
  	this.resources = options.resources || new LV.Resources();
      this.value = {};
    },
    accept: function (e) {
  	var encryptedId = this.isUserSpecific ? '/' + this.resources.getEncryptedUserId() : '';
  
      e.preventDefault();
      this.value.DateAccepted = Date.now();
      localStorage.setItem(this.app + '/eula' + encryptedId, JSON.stringify(this.value));
      window.location.reload();
    },
    decline: function (e) {
      if (typeof device !== 'undefined' && 'Windows' || 'Android') {
        navigator.app.exitApp();
      }
      else {
        e.preventDefault();
      }
    }
  });
  
  //Tab Layout
  // ---------
  
  LV.Views.TabLayout = Backbone.Marionette.LayoutView.extend({
    template: function () {
      var data = this.serializeData();
      return JST['html/lv-view-tab-layout.html'](this.mixinTemplateHelpers(data));
    },
    tabs: {
    },
    defaultTab: '',
    mixinTemplateHelpers: function (target) {
      target = target || {};
      var templateHelpers = this.templateHelpers;
      if (_.isFunction(templateHelpers)) {
        templateHelpers = templateHelpers.call(this);
      }
      return _.extend(target, templateHelpers, { tabs: this.tabs, defaultTab: this.defaultTab });
    },
    _defaultEvents: {
      'click .lv-tabs a': 'handleTabClicked'
    },
    _defaultRegions: {
      tabContent: '.lv-tab-content'
    },
    _defaultUi: {
      tabs: '.lv-tabs'
    },
    constructor: function () {
      this.events = _.extend({}, this._defaultEvents, this.events);
      this.regions = _.extend({}, this._defaultRegions, this.regions);
      this.ui = _.extend({}, this._defaultUi, this.ui);
      Backbone.Marionette.Layout.prototype.constructor.apply(this, arguments);
    },
    handleTabClicked: function (e) {
      e.preventDefault();
      var tab = $(e.target).text();
  
      if (this.tabs[tab] !== undefined) {
        this.setActiveTab(tab);
      }
    },
    loadTab: function (Target) {
      var View, viewInstance;
      if (Target instanceof Backbone.Marionette.View) {
        viewInstance = Target;
        viewInstance.delegateEvents();
      } else {
        if (Target.prototype && _.isFunction(Target.prototype.constructor)) {
          View = Target;
        } else {
          View = Backbone.Marionette.ItemView.extend({template: _.template(Target)});
        }
        viewInstance = new View();
      }
      this.listenToOnce(this, 'show', this.onShowTab);
  
      this.tabContent.show(viewInstance);
    },
    onShowTab: function () {},
    resetActiveTab: function () {
      this.ui.tabs.find('li').removeClass('active');
    },
    setActiveTab: function (tab) {
      this.resetActiveTab();
      var tabNames = _.keys(this.tabs),
        idx = _.indexOf(tabNames, tab);
      var target = this.tabs[tab];
      this.currentTab = tab;
      this.loadTab(target);
      this.ui.tabs.find('li')[idx].className += ' active';
    },
    render: function () {
      Backbone.Marionette.Layout.prototype.render.apply(this, arguments);
      if (this.defaultTab && this.tabs[this.defaultTab] && !this.currentTab) {
        this.loadTab(this.tabs[this.defaultTab]);
      } else if (this.currentTab) {
        this.setActiveTab(this.currentTab);
      }
      return this;
    }
  });
  
  //Popup Layout
  // ---------
  
  var PopupModel = Backbone.Model.extend({
    initialize: function () {
      var that = this;
  
      this.on('change', function () {
        that.updateModel();
      });
    },
    updateModel: function () {
      var data = this.toJSON(),
        buttons = data.buttons || [],
        buttonL = buttons[0] || {},
        buttonR = buttons[1] || {};
  
      data.title = data.title || '';
      data.closeBtnClass = data.title ? 'title-close' : '';
      data.closeBtnIcon = data.closeBtnIcon || '';
      data.numButtons = buttons.length > 1 ? 2 : 1;
      data.btnTitleL = buttonL.title || 'OK';
      data.btnActionL = buttonL.action || function () {
      };
      data.btnIconL = buttonL.icon || '';
      data.btnTitleR = buttonR.title || '';
      data.btnActionR = buttonR.action || function () {
      };
      data.btnIconR = buttonR.icon || '';
      data.popupClass = data.popupClass || '';
      data.contentClass = data.contentClass || '';
      data.hideCloseBtn = data.hideCloseBtn || false;
  
      data.contentClass += data.title ? ' header-size' : '';
  
      data.scope = data.scope || {};
  
      this.set(data, {silent: true});
    }
  });
  
  LV.Views.PopupLayout = Backbone.Marionette.LayoutView.extend({
    className: 'popup',
    template: function (data) {
      return JST['html/lv-view-popup-layout.html'](data);
    },
    regions: {
      'content': '.popup-content'
    },
    ui: {
      body: '.popup-body',
      content: '.popup-content',
      footer: '.popup-footer'
    },
    events: {
      'click #popup-close-btn': 'closePopup',
      'click #popup-button-l': 'doLeftBtnAction',
      'click #popup-button-r': 'doRightBtnAction'
    },
    initialize: function (options) {
      options = options || {};
  
      this.model = new PopupModel();
  
      this.model.set(options).trigger('change');
    },
    onRender: function () {
      this.appendButtons();
  
      this.delegateEvents();
    },
    onShow: function () {
      this.stackButtons();
    },
    appendButtons: function () {
      var templateFileName = 'html/lv-view-popup-' + (this.model.get('numButtons') > 1 ? 'two' : 'one') + '-button.html';
      var buttonTemplate = JST[templateFileName](this.model.toJSON());
  
      this.ui.footer.empty().append(buttonTemplate);
    },
    stackButtons: function () {
      var width = this.ui.body.outerWidth(),
        numButtons = this.model.get('numButtons');
  
      if (width <= 400 && numButtons > 1) {
        this.ui.footer.find('button').removeClass('popup-two-button');
        this.ui.content.addClass('popup-two-button');
      }
    },
    doLeftBtnAction: function () {
      this.doButtonAction(this.model.get('btnActionL'));
    },
    doButtonAction: function (action) {
      if ($.isFunction(action)) {
        action(this.model.get('scope'));
      }
  
      this.closePopup();
    },
    doRightBtnAction: function () {
      this.doButtonAction(this.model.get('btnActionR'));
    },
    closePopup: function () {
      this.close();
    }
  });
    //Refresh View
    // ---------
    /*jshint -W117 */
  
  LV.Views.RefreshView = Backbone.Marionette.ItemView.extend({
  	getTemplate: function () {
  	    var data = this.serializeData();
  	  	return JST['html/lv-view-refresh.html'](data);
  	},
  	attributes: function(){
  		var attrs = {
  			class: 'refresh-widget' + (/^true$/i.test(this.options.invert) ? ' invert' : '')
  		};
    		return _.extend(attrs, this.options.widgetAttrs);
  	},
  	events: {
  		'click button': 'doRefresh'
  	},
  	ui: {
  		button: 'button'
  	},
  	initialize: function (options) {
  		var defaults = {
  			label: 'Last Refresh',
  			labelClass: 'bold',
  			format: 'MM/DD/YYYY HH:mm',
  			refreshIcon: 'glyphicon glyphicon-refresh',
  			boldLabel: true,
      disabled: false,
      fetchables: []
  		};
  		_.extend(this.options, defaults, options);
  		var fetchables = this.options.fetchables;
  		this.options.fetchables = _.isArray(fetchables) ? fetchables : [fetchables];
  	},
  	serializeData: function(){
  		return _.extend(
  					this.options, 
  					{time: this.formatTime()},
  					{labelClass: this.getLabelClass()}
  				);
  	},
  	formatTime: function () {
  		if(moment){
  			return moment().format(this.options.format);
  		}
  		function fix(val){
  			return val < 10 ? '0' + val : val;
  		}
  		console.warn('Moment Date Library not found, standard date format is shown for Refresh View.');
  		var d = new Date();
  		var month = fix(month = d.getMonth() + 1);
  		var day = fix(day = d.getDate());
  		var year = d.getFullYear();
  		var hour = fix(hour = d.getHours());
  		var minute = fix(minute = d.getMinutes());
  		return month + '/' + day + '/' + year + ' ' + hour + ':' + minute;
  	},
  	getLabelClass: function () {
  		if(!/^true$/i.test(this.options.boldLabel)){
  			this.options.labelClass.replace(/\bbold\b/gi, '');
  		}
  		return this.options.labelClass;
  	},
  	toggleRefreshButton: function () {
  	  this.ui.button[this.ui.button.is(':disabled') ? 'removeAttr' : 'attr']('disabled', true);
  	},
  	doRefresh: function () {
  		var that = this;
  		var fetchables = this.options.fetchables;
  		var deferreds = [];
  		
    		function finalize(){
    			var finalizeCallback = that.options.finalize;
    			if(_.isFunction(finalizeCallback)){
    				finalizeCallback.call(finalizeCallback);
    			}
    			that.toggleRefreshButton.call(that);
          	that.render();
    		}
  
  		this.toggleRefreshButton();
  		_.each(fetchables, function (fetchable) {
  			deferreds.push(fetchable.fetch());
  		});
  		$.when(deferreds).then(function(){
  			finalize.call(that);
  		});
  	},
  	onRender: function () {
  	  if(/^true$/i.test(this.options.disabled)){
  	    this.toggleRefreshButton();
  	  }
  	}
  });
  //Author: Almas Barzhaxynov
  /*
   * This is a jquery powered wrapper object.
   * Given aria tags to elements with ariaTrue or ariaFalse classes. It was created so that developers would need to spend
   * less time testing for 508 and continue development on other parts of the project
   *
   * Aria tags will be added when add:aria event is triggered in Show methods.
   */
  
  LV.Accessibility = function (opts) {
    var options = opts || {};
    $(this.selector).on('add:aria', this.addAria);
  };
  _.extend(LV.Accessibility.prototype, {
    addAria: function () {
      var $trueAria = $('.trueAria');
      var $falseAria = $('.falseAria');
  
      $trueAria.each(function(index) {
        $(this).attr('tabindex', 0);
        $(this).attr('aria-hidden', 'false');
        if(this.tagName === 'BUTTON') {
          $(this).attr('role', 'button');
        }
        if(this.tagName === 'A') {
          $(this).attr('role', 'link');
        }
      });
  
      $falseAria.each(function(index) {
        $(this).attr('tabindex', -1);
        $(this).attr('aria-hidden', true);
      });
    }
  });
  
  //Author: Almas Barzhaxynov
  //Editor: Will Preston
  /*
   * This is a bootstrap powered jquery wrapper object.
   * Given a valid selector and other parameters, will display a popover briefly once triggered.
   * It was painstakingly tested to be Section 508 accessible and compliant on iPad ios7
   *
   * USE: Instaniate the popover object with the require parameters.
   * @param: success        - if an error occured this should be set to false otherwise true
   * @param: content        - the description of the popover
   * @param: placement      - [ 'left', 'right', 'top', 'bottom']
   * @param: container      - valid jquery selector of html tag the popover should be associated with
   *
   * When the popover should appear, trigger an 'addPopover' event on the container element.
   */
  
  LV.Popover = function (opts) {
      if (typeof opts === 'undefined' || opts.length !== 6) {
  	console.log('Popover requires (success, content, placement, container, delayForReader, delayRemove) as parameters');
  	return;
      } else {
  	this.container      = opts.container;
  	this.success        = opts.success;
  	this.content        = opts.content;
  	this.placement      = opts.placement;
  	this.delayForReader = 1500; //ms
  	this.delayRemove    = 4000; //ms
      }
      $(this.selector).on('addPopover', this.show);
  };
  _.extend(LV.Popover.prototype, {
  	show: function () {
  	var $popoverContainer = $(this.container);
  	if (this.success) {
  	    $popoverContainer.popover({
  		    content: this.content + ' <i class="icon icon-check-1" aria-hidden="true"></i>',
  			trigger: 'manual',
  			placement: this.placement,
  			html: 'true'
  			});
  	} else {
  	    $popoverContainer.popover({
  		    content: this.content + ' <i class="icon icon-close-1" aria-hidden="true"></i>',
  			trigger: 'manual',
  			placement: this.placement,
  			html: 'true'
  			});
  	}
  	$popoverContainer.popover('show');
  	var that = this;
  	setTimeout(function () {
  		var $popover = $('.popover-content');
  		$popover.attr('tabindex', 0);
  		$popover.attr('aria-label', that.content);
  		$popover.focus();
  	    }, this.delayForReader);
  	setTimeout(function () {
  		$popoverContainer.popover('destroy');
  		$popoverContainer.focus();
  	    }, this.delayRemove);
  	}
  });
  

  //MyList Library
  // ---------
  /*
   * Below you will find a collection of methods useful for creating a front end application 
   * for the mylist back end.
   *
   */
  
  LV.MyListLib = {
    insertURLPath: function (base, key, val) {
      //find the path that matches the key and insert the val in the following path
      // erg /opo/###/ko/
      //      ^^^
      //      key
      // erg /opo/###/ko/
      //          ^^^
      //          val
      base = base.split('%23').join('');
      var findMe = '/' + key + '/';
      var splitAt = base.indexOf(findMe) + findMe.length;
      var A = base.substring(0, splitAt);
      var B = base.substring(splitAt);
      return A + val + B;
    },
    convertBooleanArrayToMyListsNumber: function (bArr) {
      /* given an array of true or false values representing a binary string, convert it into a base ten integer. */
      bArr.reverse();
      var sumOfLists = 0;
      var factor = 1;
      for (var i = 0; i < bArr.length; i++) {
        if (bArr[i]) {
          sumOfLists += factor;
        }
        factor *= 2;
      }
  
      return sumOfLists;
    },
    convertMyListsNumberToBooleanArray: function (bnum) {
      /* given an integer number, transform it into an array of booleans representing the bits of a binary number 0 or 1 (base two) */
  
      var boolBits = [];
      var result = 0;
      while (bnum > 0) {
        result = bnum % 2;
        boolBits.push(!!result);
        bnum = Math.floor(bnum / 2);
      }
  
      while (boolBits.length < 4) {
        boolBits.push(false);
      }
  
      return boolBits.reverse();
    },
    createAriaLabel: function (mylistNumber, listNames) {
      //given a mylists number and an array of listNames, create a sentence explaining which of the user defined lists belong to the given integer
  
      var label = '';
      var line1 = 'Patient belongs to the following list: ';
      var line2 = 'Patient belongs to the following lists: ';
      var lineZero = 'Patient belongs to zero lists.';
      var isOnLists = this.convertMyListsNumberToBooleanArray(mylistNumber).reverse();
  
      var count = 0;
      _.each(isOnLists, function (isOnThisList, index, lists) {
        if (!isOnThisList) {
          return;
        } else {
          count++;
        }
  
        if (count > 1) {
          label += ' and ' + listNames[index];
        } else {
          label += ' ' + listNames[index];
        }
  
      });
  
      if(!label){
        label = '';
      }
  
      if (count < 1) {
        label = lineZero;
      } else if (count > 1) {
        label = line2 + label;
      } else {
        label = line1 + label;
      }
      return label;
    },
    createMyListTitleModel: function (url) {	//one must build the full url to be passed in
      var Model = Backbone.Model.extend({
        idAttribute: 'id',
        defaults: {
          listOneName: 'My List One',
          listTwoName: 'My List Two',
          listThreeName: 'My List Three',
          listFourName: 'My List Four',
          staffId: '',
          facilityCode: ''
        },
        initialize: function (args) {
          this.url = args.url;
        },
        fetchTitles: function () {
          if (typeof this.url === 'undefined' || this.url === null) {
            console.log('please provide the list model with a correctly built restful url.');
            return;
          }
          this.fetch({
            dataType: 'json',
            reset: true,
            async: false
          }).fail(function (jqXHR) {
            if (jqXHR.status === 400) {
              this.trigger('my-list-titles:error', $.parseJSON(jqXHR.responseText).message);
            }
          });
        },
        getTitlesArr: function () {
          return [this.get('listOneName'), this.get('listTwoName'), this.get('listThreeName'), this.get('listFourName')];
        },
        createAriaLabel: function (mylistNumber, listNames) {
          //given a mylists number and an array of listNames, create a sentence explaining which of the user defined lists belong to the given integer
  
          var label = '';
          var line1 = 'Patient belongs to the following list: ';
          var line2 = 'Patient belongs to the following lists: ';
          var lineZero = 'Patient belongs to zero lists.';
          var isOnLists = this.convertMyListsNumberToBooleanArray(mylistNumber).reverse();
  
          var count = 0;
          _.each(isOnLists, function (isOnThisList, index, lists) {
            if (!isOnThisList) {
              return;
            } else {
              count++;
            }
  
            if (count > 1) {
              label += ' and ' + listNames[index];
            } else {
              label += ' ' + listNames[index];
            }
  
          });
  
          if (count < 1) {
            label = lineZero;
          } else if (count > 1) {
            label = line2 + label;
          } else {
            label = line1 + label;
          }
          return label;
        },
        parse: function (response) {
          if (typeof response.listOneName !== 'undefined') {
            response.listOneName = LV.MyListLib.unescape(response.listOneName);
            response.listTwoName = LV.MyListLib.unescape(response.listTwoName);
            response.listThreeName = LV.MyListLib.unescape(response.listThreeName);
            response.listFourName = LV.MyListLib.unescape(response.listFourName);
          }
  
          return response;
        }
      });
      return  new Model({url: url});
    },
    unescape: function (line) {
      line = line || '';
  
      var funnyStuff = [
        {
          change: '&quot;',
          to: '\"'
        },
        {
          change: '&lt;',
          to: '<'
        },
        {
          change: '&gt;',
          to: '>'
        },
        {
          change: '&amp;',
          to: '&'
        }
      ];
  
      _.each(funnyStuff, function (mapping, index, arr) {
        var pieces = line.split(mapping.change);
        line = pieces.join(mapping.to);
      });
      return line;
    },
    createMyListPatientModel: function (url) {
      var Model = Backbone.Model.extend({
        numMyLists: 4,
        defaults: {
          'id': null,
          'displayName': '',
          'firstName': '',
          'lastName': '',
          'middleName': '',
          'gender': '',
          'location': '',
          'ssn': '',
          'dateOfBirth': -2209057200000,
          'facilityCode': '',
          'staffId': '',
          'patientId': '',
          'notesListOne': '',
          'notesListTwo': '',
          'notesListThree': '',
          'notesListFour': '',
          'myLists': 0
  
        },
        initialize: function (args) {
          this.url = args.url;
        },
        patientFetch: function () {
          if (typeof this.url === 'undefined' || this.url === null) {
            console.log('please provide the mylist patient model with a correctly built restful url.');
            return;
          }
          this.fetch({
            dataType: 'json',
            reset: true,
            async: false
          }).fail(function (jqXHR) {
            if (jqXHR.status === 400) {
              this.trigger('patient-details:error', $.parseJSON(jqXHR.responseText).message);
            }
          });
        },
        parse: function (response) {
          var patient = this.toJSON();
          if (typeof response.id !== 'undefined' && response.id.length > 0) {
            patient = response;
            patient.notesListFour = LV.MyListLib.unescape(response.notesListFour);
            patient.notesListThree = LV.MyListLib.unescape(response.notesListThree);
            patient.notesListTwo = LV.MyListLib.unescape(response.notesListTwo);
            patient.notesListOne = LV.MyListLib.unescape(response.notesListOne);
          }
          return patient;
        },
        getNotesArr: function () {
          if (this.isExistingOnMongo()) {
            return [this.get('notesListOne'), this.get('notesListTwo'), this.get('notesListThree'), this.get('notesListFour')];
          } else {
            return ['', '', '', ''];
          }
        },
        isOnListsArr: function () {
          return LV.MyListLib.convertMyListsNumberToBooleanArray(this.get('myLists')).reverse();
        },
        isAlreadySavedToMongo: function () {
          return this.get('id') !== null;
        },
        isOnAnyMyLists: function () {
          return this.get('myLists') > 0;
        },
        assignPatientToMyLists: function (mylistArray) {
          if (mylistArray.length !== 4) {
            console.log('array provided to assignPatientToMyLists must be of length exactly 4');
            return false;
          }
          //change which lists the patient is on by providing an array of booleans represting which lists the patienet is being added to
          this.set('myLists', LV.MyListLib.convertBooleanArrayToMyListsNumber(mylistArray));
          return true;
        },
        getIconClasses: function () {
          //inject these class names into the class property of an <i> tag to show the icon
          return 'icon binary-icon-' + this.get('myLists');
        }
      });
      return new Model({url: url});
    }
  };
  
  var Resources,
    vent,
    basePath = 'PatientViewerServices/',
    myListBasePath = 'my-list-resource';
  
  function MY_LIST_LIMIT() {
    return 4;
  }
  
  function getTemplate(template) {
    return JST['html/mylist/' + template + '.html'];
  }
  
  function getMyListLib() {
    return LV.MyListLib;
  }
  
  var MyListTitles = Backbone.Model.extend({
    idAttribute: 'id',
    defaults: {
      listOneName: 'My List One',
      listTwoName: 'My List Two',
      listThreeName: 'My List Three',
      listFourName: 'My List Four',
      staffId: '',
      facilityCode: ''
    },
    url: function () {
      var line = (
        Resources.get('my-list-titles') ||
      {href: '../' + myListBasePath + '/rest/my-list-search/site/%23/id/%23/my-list-titles?_=1419000353539'}
        ).href;
  
      line = getMyListLib().insertURLPath(line, 'id', Resources.user.id);
      line = getMyListLib().insertURLPath(line, 'site', Resources.user.vistaLocation);
  
      return line;
    },
    getTitleArr: function () {
      return [this.get('listOneName'), this.get('listTwoName'), this.get('listThreeName'), this.get('listFourName')];
    },
    parse: function (response) {
      var that = this;
  
      response = response || {};
      _.each(
        _.filter(
          _.keys(this.defaults), function (prop) {
            return /^list/.test(prop);
          }
        ), function (item) {
          response[item] = getMyListLib().unescape(response[item]) || that.defaults[item];
        }
      );
      return response;
    },
    updateTitles: function (models) {
      var changed = false;
      _.each(models, function (model) {
        if (!changed) {
          changed = _.has(model.changed, 'name');
        }
        this.set(model.get('code'), model.get('name'));
      }, this);
      if (changed) {
        this.save();
      }
    }
  });
  
  var AMyList = Backbone.Model.extend({
    defaults: {
      name: 'My List One',
      code: 'listOneName',
      index: 1,
      active: true,
      edit: false
    }
  });
  
  var MyLists = Backbone.Collection.extend({
    model: AMyList,
    initialize: function (options) {
      var collection = this,
        curModel,
        clone,
        keys;
  
      var LIMIT = MY_LIST_LIMIT();
      if (this.length === LIMIT) {
        return;
      }
      while (LIMIT--) {
        var listIndex = this.length,
          wordIndex;
        switch (++listIndex) {
          case 1:
            wordIndex = 'One';
            break;
          case 2:
            wordIndex = 'Two';
            break;
          case 3:
            wordIndex = 'Three';
            break;
          case 4:
            wordIndex = 'Four';
            break;
          default:
            wordIndex = '(Unrecognized List)';
            break;
        }
        this.push(new this.model({name: 'My List ' + wordIndex, index: listIndex, code: 'list' + wordIndex + 'Name'}));
      }
  
      clone = new Backbone.Collection(this.models);
  
      options.myListTitles.deferred.done(function () {
        _.each(_.keys(options.myListTitles.toJSON()), function (key) {
          if (/^list\w+Name$/.test(key)) {
            curModel = clone.findWhere({code: key});
            curModel.set({name: options.myListTitles.get(key)});
          }
        });
        collection.reset(clone.models);
      });
    },
    setEdit: function (isEdit) {
      _.each(this.models, function (model) {
        model.set('edit', (isEdit || false));
      });
    }
  });
  
  var PatientInfo = Backbone.Model.extend({
    defaults: {
      dateOfBirth: '',
      firstName: '',
      gender: '',
      lastName: '',
      patientIdentifier: {
        assigningAuthority: '',
        uniqueId: ''
      },
      ssn: '',
      wardLocation: ''
    },
    initialize: function (opts) {
      var options = opts || {};
  
      this.url = '../PatientViewerServices/rest/patient/dfn-' + Resources.user.vistaLocation + '/' + options.patientId;
    },
    parse: function (response) {
      response.displayName = response.lastName + ', ' + response.firstName;
  
      return this.flattenLinks(response);
    },
    flattenLinks: function (object) {
      var links = object.link;
      if (_.isArray(links)) {
        var size = links.length;
  
        for (var i = 0; i < size; i++) {
          if (links[i].title) {
            object[links[i].title] = links[i];
          }
          else {
            object.self = links[i];
          }
        }
      }
      delete object.link;
      return object;
    }
  });
  
  var Patient = Backbone.Model.extend({
    url: function () {
      return '../' + myListBasePath + '/rest/my-list-search/site/' + Resources.user.vistaLocation + '/id/' + Resources.user.id + '/my-list-patients';
    },
    defaults: {
      'id': null,
      'displayName': '',
      'firstName': '',
      'lastName': '',
      'middleName': '',
      'gender': '',
      'location': '',
      'ssn': '',
      'dateOfBirth': -2209057200000,
      'facilityCode': '',
      'staffId': '',
      'patientId': '',
      'notesListOne': '',
      'notesListTwo': '',
      'notesListThree': '',
      'notesListFour': '',
      'myLists': 0
  
    },
    initialize: function () {
  
    },
    fetch: function (patientId) {
      this.clear();
      if (typeof patientId === 'undefined') {
        patientId = this.get('patientId');
      }
      var line = '../' + myListBasePath + '/rest/my-list-search/site/%23%23%23/id/%23%23/my-list-patient/';
      line = getMyListLib().insertURLPath(line, 'id', Resources.user.id);
      line = getMyListLib().insertURLPath(line, 'site', Resources.user.vistaLocation);
      var restURL = line + '?patientId=' + patientId;
  
      this.deferred = Backbone.Model.prototype.fetch.call(this, {
        url: restURL,
        dataType: 'json',
        reset: true
      }).fail(function (jqXHR) {
        if (jqXHR.status === 400) {
          //App.vent.trigger('patient-details:error', $.parseJSON(jqXHR.responseText).message);
        }
      });
    },
    parse: function (response) {
      response.patientIdentifier = {
        uniqueId: response.patientId
      };
  
      if (typeof response.id !== 'undefined' && response.id.length > 0) {
        response.notesListFour = getMyListLib().unescape(response.notesListFour);
        response.notesListThree = getMyListLib().unescape(response.notesListThree);
        response.notesListTwo = getMyListLib().unescape(response.notesListTwo);
        response.notesListOne = getMyListLib().unescape(response.notesListOne);
      }
      return response;
    },
    getNotesArr: function () {
      if (this.isExistingOnMongo()) {
        return [this.get('notesListOne'), this.get('notesListTwo'), this.get('notesListThree'), this.get('notesListFour')];
      } else {
        return ['', '', '', ''];
      }
    },
    isOnListsArr: function () {
      return getMyListLib().convertMyListsNumberToBooleanArray(this.get('myLists')).reverse();
    },
    isExistingOnMongo: function () {
      return this.get('id'); //the name made sense
    },
    isOnAnyMyLists: function () {
      return this.get('myLists') > 0; //the name made sense
    }
  });
  
  var MyListPatients = Backbone.PageableCollection.extend({
    model: Patient,
    url: function () {
      return '../' + myListBasePath + '/rest/my-list-search/site/' + Resources.user.vistaLocation + '/id/' + Resources.user.id + '/my-list-patients';
      //Resources.get('my-list-search').href + '/site/' + Resources.user.vistaLocation + '/id/' + Resources.user.id + '/my-list-patients';
    },
    state: {
      pageSize: 20,
      firstPage: 0,
      totalRecords: null
    },
    queryParams: {
      currentPage: 'currentPage',
      pageSize: null,
      totalRecords: null
    },
    parseState: function (resp, queryParams, state, options) {
      var count = parseInt(resp.myListCount);
      return {totalRecords: count};
    },
    parseRecords: function (resp, options) {
      return resp.patient;
    }
  });
  
  var FilterItemView = Backbone.Marionette.ItemView.extend({
    className: 'col-xs-6 filter-item-container',
    getTemplate: function () {
      if (this.model.get('edit')) {
        return getTemplate('filter-item-edit-view')(this.model.toJSON());
      }
      return getTemplate('filter-item-view')(this.model.toJSON());
    },
    modelEvents: {
      'change:active': 'render',
      'change:edit': 'render'
    },
    events: {
      'click button': 'handleLink',
      'change input': 'update'
    },
    ui: {
      checkbox: 'button',
      input: 'input'
    },
    initialize: function (opts) {
      this.model.set(_.extend(this.templateHelpers(), this.model.toJSON()));
    },
    handleLink: function (e) {
      e.preventDefault();
      this.model.set('active', !this.model.get('active'));
    },
    update: function () {
      this.model.set('name', this.ui.input.val().trim());
    },
    templateHelpers: function () {
      return {
        isActive: function () {
          return this.active;
        },
        activeClass: function () {
          return this.active ? 'checked' : '';
        }
      };
    },
    onShow: function () {
      this.bindUIElements();
      this.delegateEvents();
      if (this.options.focus) {
        this.ui.checkbox.focus();
      }
    }
  });
  
  var FilterView = Backbone.Marionette.CompositeView.extend({
    id: 'my-list-filter-item-view',
    itemView: FilterItemView,
    itemViewContainer: '#my-list-filters',
    getTemplate: function () {
      return getTemplate('filter-view')();
    },
    itemViewOptions: function (model, index) {
      return {focus: index === 0};
    },
    initialize: function (options) {
      _.extend(this, options);
    },
    ui: {
      updateResultsBtn: 'button'
    },
    events: {
      'click button': 'mylistFilter'
    },
    mylistFilter: function () {
      vent.trigger('mylist:filter');
    },
    onShow: function () {
      this.bindUIElements();
      this.delegateEvents();
    }
  });
  
  var MyListPatientItem = Backbone.Marionette.ItemView.extend({
    tagName: 'button',
    className: 'btn mylist-patient text-left',
    initialize: function (options) {
      this.listTitles = (options || {} ).listTitles;
      this.model.set(_.extend(this.templateHelpers(), this.model.toJSON()));
    },
    getTemplate: function () {
      return getTemplate('mylist-patient-item')(this.model.toJSON());
    },
    events: {
      'click': 'patientDetails',
      'keypress': 'patientDetails'
    },
    patientDetails: function () {
      var patientInfo = new PatientInfo({patientId: this.model.get('patientId')});
  
      patientInfo.fetch().done(function () {
        vent.trigger('mylist:patient:select', patientInfo);
      });
    },
    templateHelpers: function () {
      var that = this;
      var model = this.model.toJSON();
      return {
        getMyListsNumber: function () {
          return 3;
        },
        getMyListAriaLabel: function () {
          return getMyListLib().createAriaLabel(model.myLists, that.listTitles.getTitleArr());//update signature
        }
      };
    },
    onShow: function () {
      this.bindUIElements();
      this.delegateEvents();
    }
  });
  
  var MyListResults = Backbone.Marionette.CompositeView.extend({
    id: 'my-list-results-view',
    getTemplate: function () {
      return getTemplate('mylist-view')();
    },
    itemView: MyListPatientItem,
    itemViewContainer: '#mylist-results',
    itemViewOptions: function (model, index) {
      return {
        patients: this.patients,
        listTitles: this.myListTitles
      };
    },
    initialize: function (options) {
      _.extend(this, options);
    },
    ui: {
      resultCt: 'h5',
      previousBtn: '#previous-btn-ml',
      nextBtn: '#next-btn-ml'
    },
    collectionEvents: {
      add: 'updateResultCount',
      remove: 'updateResultCount',
      sync: 'updateResultCount'
    },
    events: {
      'click #previous-btn-ml': 'previousSearchResults',
      'click #next-btn-ml': 'nextSearchResults',
      'keypress #previous-btn-ml': 'previousSearchResults',
      'keypress #next-btn-ml': 'nextSearchResults'
    },
    updateResultCount: function () {
      var length = this.collection.state.totalRecords;
      var result = length === 1 ? 'Result' : 'Results';
      this.ui.resultCt.html(length + ' ' + result);
  
      this.updatePaging(this.ui.previousBtn, this.collection.hasPreviousPage());
      this.updatePaging(this.ui.nextBtn, this.collection.hasNextPage());
    },
    updatePaging: function (ele, enabled) {
      ele.attr('disabled', !enabled);
    },
    previousSearchResults: function () {
      this.collection.getPreviousPage(this.getParams());
    },
    nextSearchResults: function () {
      this.collection.getNextPage(this.getParams());
    },
    filter: function () {
      this.collection.getFirstPage(_.extend({reset: true}, this.getParams()));
    },
    getParams: function () {
      var boolArr = [],
        mylistNumber;
  
      if(this.myLists.length === MY_LIST_LIMIT()){
        _.each(this.myLists.models, function (model) {
          boolArr.unshift(model.get('active'));
        });
      }
      else{
        for(var i=0; i<MY_LIST_LIMIT(); i++){
          boolArr.push(true);
        }
      }
  
      mylistNumber = getMyListLib().convertBooleanArrayToMyListsNumber(boolArr);
  
      return {
        data: {
          list: mylistNumber
        }
      };
    },
    onShow: function () {
      vent.on('mylist:filter', $.proxy(this.filter, this));
      this.bindUIElements();
      this.delegateEvents();
      this.collection.getFirstPage(this.getParams());
    }
  });
  
  var MyListPatientMyListView = Backbone.Marionette.ItemView.extend({
    getTemplate: function () {
      var data = this.model.toJSON();
      return getTemplate('patient-mylist-item')(data);
    }
  });
  
  var MyListPatientMyListsView = Backbone.Marionette.CompositeView.extend({
    itemView: MyListPatientMyListView,
    itemViewContainer: 'ul',
    getTemplate: function () {
      var data = this.model.toJSON();
      return getTemplate('patient-mylists')(data);
    }
  });
  
  var MyListPatientSelectNote = Backbone.Marionette.ItemView.extend({
    className: 'note',
    getTemplate: function () {
      var data = _.extend(this.model.toJSON(), this.templateHelpers());
      return getTemplate('patient-select-note')(data);
    },
    ui: {
      checkBox: '.square-parent',
      editNote: '.patient-details-pencil-btn',
      noteBox: '.note-box',
      toggle: '.note-area'
    },
    events: {
      'click .square-child': 'toggleChecked',
      'click .patient-details-pencil-btn': 'editNote',
      'change .note-box': 'updateNote'
    },
    initialize: function () {
      vent.on('disable:note-boxes', this.disableNote, this);
    },
    toggleChecked: function () {
      var $checkBox = this.ui.checkBox;
      var checked;
  
      $checkBox.toggleClass('checked');
  
      checked = $checkBox.hasClass('checked');
  
      $checkBox.attr('aria-checked', checked);
      this.model.set('selected', checked);
  
      this.ui.toggle.toggleClass('out').toggleClass('in');
  
      this.editNote();
    },
    editNote: function () {
      var $noteBox = this.ui.noteBox;
  
      this.ui.editNote.addClass('hidden');
      $noteBox[0].disabled = false;
      $noteBox.focus().removeClass('border-none');
    },
    updateNote: function () {
      this.model.set('note', this.ui.noteBox.val());
    },
    disableNote: function () {
      this.bindUIElements();
      if (this.ui.checkBox.hasClass('checked')) {
        var $noteBox = this.ui.noteBox;
        $noteBox.addClass('border-none');
        $noteBox[0].disabled = true;
        this.ui.editNote.removeClass('hidden');
      }
    },
    templateHelpers: function () {
      var that = this;
  
      return {
        getNoteLabel: function () {
          var list = that.model.toJSON();
  
          if (list.selected) {
            return 'Note (Character Limit: ' + list.charLimit + ')';
          } else {
            return 'Note';
          }
        }
      };
    }
  });
  
  var MyListPatientSelectNotes = Backbone.Marionette.CollectionView.extend({
    itemView: MyListPatientSelectNote
  });
  
  var MyListPatientSelectView = Backbone.Marionette.LayoutView.extend({
    id: 'mylist-patient-select',
    ui: {
      'checkBoxes': '.square-parent',
      'expandMyList': '#expand-mylist',
      'noteBoxes': '.note-box',
      'updateMyList': '#update-patient-mylist',
      'selectPatient': '#select-patient'
    },
    modelEvents: {
      'change': 'render'
    },
    events: {
      'click .popup-close': 'closePopup',
      'click #select-patient': 'selectPatient',
      'click .square-parent button, .patient-details-pencil-btn': 'enableUpdateButton',
      'click #update-patient-mylist': 'updateToMyList'
    },
    regions: {
      'noteSection': '.note-section',
      'patientLists': '.patient-lists'
    },
    initialize: function () {
      this.dateman = new LV.Dateman();
  
      this.patientMyList = new Patient();
      this.listTitles = new MyListTitles();
  
      this.patientSelectNotesView = new MyListPatientSelectNotes();
      this.patientMyListsView = new MyListPatientMyListsView();
    },
    getTemplate: function () {
      var data = this.model.toJSON();
      return getTemplate('my-list-patient-select')(this.mixinTemplateHelpers(data));
    },
    mixinTemplateHelpers: function (target) {
      target = target || {};
      var templateHelpers = this.templateHelpers;
      if (_.isFunction(templateHelpers)) {
        templateHelpers = templateHelpers.call(this);
      }
      return _.extend(target, templateHelpers);
    },
    openPopup: function (model) {
      this.initModel(model);
  
      this.fetchData();
  
      this.draw();
  
      this.delegateEvents();
    },
    draw: function () {
      var $body = $('body');
      var $el = $('#' + this.id);
  
      if (!$el.length) {
        this.render();
        $body.append(this.$el);
      } else {
        this.$el = $el;
        this.render();
      }
  
      this.ui.selectPatient.focus();
    },
    closePopup: function () {
      this.clearModel();
      this.undelegateEvents();
      this.$el.addClass('hidden');
    },
    fetchData: function () {
      var that = this;
      var patientId = this.model.get('patientIdentifier').uniqueId;
  
      this.patientMyList.fetch(patientId);
      this.listTitles.deferred = this.listTitles.fetch();
  
      this.patientMyList.deferred.done(function () {
        that.listTitles.deferred.done(function () {
          that.afterFetchData();
        });
      });
    },
    afterFetchData: function () {
      var onAnyMyLists = this.patientMyList.isOnAnyMyLists();
  
      this.model.set({
        myListBtnText: onAnyMyLists ? 'Edit My List' : 'Add to My List',
        previouslySaved: this.patientMyList.isExistingOnMongo()
      }, {silent: true});
  
      this.render();
  
      this.listsCollection = new Backbone.Collection(this.populateLists());
  
      this.patientSelectNotesView.collection = this.listsCollection;
  
      this.noteSection.show(this.patientSelectNotesView);
  
      if (onAnyMyLists) {
        this.showPatientMyLists();
      }
  
      this.$el.removeClass('hidden');
    },
    showPatientMyLists: function () {
      var patientMyLists = new Backbone.Collection(this.listsCollection.where({selected: true}));
  
      this.patientMyListsView.model = new Backbone.Model({myLists: this.patientMyList.get('myLists')});
      this.patientMyListsView.collection = patientMyLists;
  
      this.patientLists.show(this.patientMyListsView);
    },
    initModel: function (model) {
      this.model = model;
  
      this.model.set({
        charLimit: 150,
        lists: [],
        myLists: '',
        myListBtnText: '',
        previouslySaved: false
      }, {silent: true});
    },
    clearModel: function () {
      this.model = new Backbone.Model();
  
      this.model.set({
        dateOfBirth: '',
        firstName: '',
        gender: '',
        lastName: '',
        ssn: '',
        wardLocation: ''
      }, {silent: true});
  
      this.initModel(this.model);
    },
    enableUpdateButton: function (config) {
      this.ui.updateMyList[0].disabled = config.disabled;
    },
    selectPatient: function () {
      vent.trigger('select:patient', this.model);
      this.closePopup();
    },
    populateLists: function () {
      var lists = [];
      var listNames = this.listTitles.getTitleArr();
      var notes = this.patientMyList.getNotesArr();
      var isOnLists = this.patientMyList.isOnListsArr();
      var charLimit = this.model.get('charLimit');
  
      _.each(_.values(listNames), function (name, index) {
        lists[index] = {
          charLimit: charLimit,
          id: index + 1,
          name: name,
          note: notes[index],
          selected: isOnLists[index]
        };
      }, this);
  
      return lists;
    },
    updateSuccess: function () {
      this.ui.expandMyList.text('Edit My List');
      this.enableUpdateButton({disabled: true});
      vent.trigger('disable:note-boxes');
      this.showPatientMyLists();
    },
    getSelectedArray: function () {
      var selectedArray = [];
  
      _.each(this.listsCollection.toJSON(), function (list) {
        selectedArray.unshift(list.selected);
      });
  
      return selectedArray;
    },
    updateModelForSave: function (sumOfLists) {
      var lists = this.listsCollection.toJSON();
      var attributes = {
        myLists: sumOfLists,
        notesListOne: lists[0].note,
        notesListTwo: lists[1].note,
        notesListThree: lists[2].note,
        notesListFour: lists[3].note
      };
  
      if (!this.model.get('previouslySaved')) {
        _.extend(attributes, {
          'facilityCode': Resources.user.vistaLocation,
          'staffId': Resources.user.id,
          'patientId': this.model.get('patientIdentifier').uniqueId,
          'patientIdentifier': this.model.get('patientIdentifier'),
          'displayName': this.model.get('displayName'),
          'firstName': this.model.get('firstName'),
          'lastName': this.model.get('lastName'),
          'gender': this.model.get('gender'),
          'middleName': ''
        });
      }
      this.patientMyList.set(attributes);
    },
    updateToMyList: function (e) {
      e.preventDefault();
      var that = this;
      var selectedArray = this.getSelectedArray();
  
      var sumOfLists = getMyListLib().convertBooleanArrayToMyListsNumber(selectedArray);
  
      this.updateModelForSave(sumOfLists);
  
      this.patientMyList.save(null, {
        success: function () {
          that.updateSuccess();
        }
      });
    },
    templateHelpers: function () {
      var that = this;
  
      return {
        getAge: function () {
          var date = that.model.get('dateOfBirth');
  
          return that.dateman.calculateAge(that.dateman.validateDate(new Date(date)));
        },
        getDate: function () {
          var date = that.model.get('dateOfBirth');
  
          return that.dateman.convertDate(new Date(date));
        }
      };
    }
  });
  
  var MyListLayoutView = Backbone.Marionette.LayoutView.extend({
    className: 'mylist-container',
    MY_LIST_LIMIT: function () {
      return MY_LIST_LIMIT();
    },
    getTemplate: function () {
      var data = _.extend(this.model.toJSON());
      return getTemplate('layout')(data);
    },
    configOptions: {},
    initialize: function (options) {
      this.model = new Backbone.Model(_.extend({closeBtn: false}, options));
      _.extend(this.configOptions, options);
      this.verifyOptions(this.configOptions);
      Resources = this.configOptions.Resources;
      vent = this.configOptions.Vent;
      basePath = this.configOptions.basePath || basePath;
      this.vents();
    },
    stage: function () {
      this.myListTitles = new MyListTitles();
      this.myListTitles.deferred = this.myListTitles.fetch();
      this.myLists = new MyLists({myListTitles: this.myListTitles});
      this.patients = new MyListPatients();
      this.filterView = new FilterView({collection: this.myLists});
      this.myListPatientSelect = this.myListPatientSelect || new MyListPatientSelectView();
      this.mylistResults = new MyListResults({collection: this.patients, myLists: this.myLists, myListTitles: this.myListTitles});
      this.vents();
    },
    verifyOptions: function (options) {
      var warnings = [],
        resourcesMissing = 'A <Resource> property must be specified',
        ventMissing = 'A <Vent> property must be specified that can handle triggering and listening to events.';
      if (!options) {
        warnings.push(resourcesMissing);
        warnings.push(ventMissing);
      }
      else {
        if (!options.Resources) {
          warnings.push(resourcesMissing);
        }
        if (!options.Vent) {
          warnings.push(ventMissing);
        }
      }
      _.each(warnings, function (warning) {
        console.warn(warning);
      });
      return warnings.length > 0;
    },
    regions: {
      filterRegion: '#filter-region',
      myListFilterRegion: '#my-list-filter-region',
      myListRegion: '#my-list-region',
      myListIcons: '.mylist-icons'
    },
    ui: {
      editBtn: '#edit',
      saveBtn: '#save',
      closeBtn: '#close'
    },
    events: {
      'click @ui.editBtn': 'editFilters',
      'click @ui.saveBtn': 'saveFilters',
      'click @ui.closeBtn': 'closeMyList'
    },
    editFilters: function () {
      this.toggleActiveFilterBtn(this.ui.editBtn, this.ui.saveBtn);
      this.myLists.setEdit(true);
    },
    saveFilters: function () {
      this.toggleActiveFilterBtn(this.ui.saveBtn, this.ui.editBtn);
      this.myListTitles.updateTitles(this.myLists.models);
      this.myLists.setEdit(false);
    },
    toggleActiveFilterBtn: function (active, nonActive) {
      active.addClass('active').attr('disabled', true);
      nonActive.removeClass('active').removeAttr('disabled');
    },
    vents: function () {
      var view = this;
      vent.on('mylist:patient:select', function (model, CustomView) {
        model = model || new Backbone.Model();
  
        if (CustomView instanceof Backbone.Marionette.View) {
          var CustomSelectView = Backbone.Marionette.LayoutView.extend({});
          $.extend(true, CustomSelectView.prototype, MyListPatientSelectView.prototype, CustomView, {initialize: MyListPatientSelectView.prototype.initialize});
          view.customSelectView = new CustomSelectView();
          view.customSelectView.openPopup(model);
        } else {
          view.myListPatientSelect = view.myListPatientSelect || new MyListPatientSelectView();
          view.myListPatientSelect.openPopup(model);
        }
      });
      vent.on('mylist:patient:select:close', function () {
        if (view.myListPatientSelect && _.isFunction(view.myListPatientSelect.closePopup)) {
          view.myListPatientSelect.closePopup();
        }
        if (view.customSelectView && _.isFunction(view.customSelectView.closePopup)) {
          view.customSelectView.closePopup();
        }
      });
    },
    onShow: function () {
      this.stage();
      this.bindUIElements();
      this.delegateEvents();
      this.myListFilterRegion.show(this.filterView);
      this.myListRegion.show(this.mylistResults);
    },
    closeMyList: function () {
      vent.trigger('mylist:close');
    }
  });
  
  LV.MyList = MyListLayoutView;
  

  var authorityId;
  var restURL;
  var cachedCoversheetData = new Backbone.Model();
  
  var isCached = function (item) {
    return cachedCoversheetData.get(item);
  };
  
  LV.Models.PatientFromCoverSheet = Backbone.Model.extend({
  
    defaults: {
      'id': null,
      'displayName': '',
      'firstName': '',
      'lastName': '',
      'gender': 'No Data Found',
      'patientIdentifier': 'No Data Found',
      'patientId': 'No Data Found',
      'dateOfBirth': 'No Data Found',
  
      address: {
        streetAddressLine1: 'No Data Found',
        streetAddressLine2: 'No Data Found',
        streetAddressLine3: 'No Data Found',
        city: 'No Data Found',
        state: 'No Data Found',
        zip: 'No Data Found'
      },
      'emailAddress': 'No Data Found',
      'phoneNumberWork': 'No Data Found',
      'phoneNumberHome': 'No Data Found',
      'phoneNumberMobile': 'No Data Found',
      'phoneNumberPager': 'No Data Found',
      wardLocation: 'No Data Found',
      nextOfKin: {
        relationship: 'No Data Found',
        name: 'No Data Found',
        phoneNumber: 'No Data Found'
      },
  
      //AllergiesInfoTabFields
      allergy: {
        'reaction': 'No Data Found',
        'sourceSystem': 'No Data Found',
        'substance': 'No Data Found'
      },
  
      //FutureAppointments
      appointment: {
        'facilityName': 'No Data Found',
        'clinicName': 'No Data Found',
        'provider': 'No Data Found',
        'appointmentStartDate': 'No Data Found'
      },
  
      //medical diagnosis fields
      'encounterDiagnosisICDCode': {
        'size': ''
      },
  
      //Surgeries
      surgery: [],
  
      //Hospitalizations
      admission: []
  
    },
    initialize: function () {
  
    },
    fetchFromMDWS: function (patientId, viewSource, ResourceDirectory, async, params) { //please provide a resource directory
      //Add the last part of the url for viewSource. Ex: 'demographics/scope/operational'
      if (typeof patientId === 'undefined') {
        patientId = this.get('patientId');
      }
      authorityId = ResourceDirectory.user.userIdentifier.assigningAuthority;
  
      restURL = ResourceDirectory.get('patient').href + '/' + authorityId + '/' + patientId;
      if (viewSource && viewSource.length > 0) {
        restURL += '/' + viewSource;
      }
      if (viewSource === '/problem-list-diagnoses') {
        var problemListURL = ResourceDirectory.get('problem-list-diagnoses').href;
  
        if (problemListURL) {
          restURL = problemListURL;
        } else {
          var userId = ResourceDirectory.user.id;
          var vistaLocation = ResourceDirectory.user.vistaLocation;
          restURL = ResourceDirectory.get('medical').href.split('%', 1) + vistaLocation + '/id/' + userId + '/patient/' + authorityId + '/' + patientId + '/problem-list-diagnoses';
        }
      }
      restURL += params || '';
      var that = this;
      this.fetch({
        url: restURL,
        dataType: 'json',
        reset: true,
        async: async || false,
        success: function () {
          that.trigger('saveLocalModel');
          that.trigger('fetch-done');
        }
      }).fail(function (jqXHR) {
        if (jqXHR.status === 400) {
          that.trigger('cover-sheet:error', $.parseJSON(jqXHR.responseText).message);
        }
      });
    },
    parse: function (response) {
      var patient = response;
  
      if (typeof response.patientIdentifier !== 'undefined') {
        patient.patientId = response.patientIdentifier.uniqueId;
      }
  
      var location = [];
      if (patient.wardLocation) {
        location.push(patient.wardLocation);
      }
      if (patient.roombed) {
        location.push(patient.roombed);
      }
      patient.fullLocation = location.join(' / ');
  
      return _.extend(this.toJSON(), patient);
    },
    setParse: function (data, clear) {
      if (clear) {
        this.clear();
      }
  
      this.set(this.parse(data));
    }
  });
  
  LV.Models.CoversheetOption = Backbone.Model.extend({
    defaults: {
      active: true,
      id: 'tab',
      label: 'Coversheet Option',
      order: 100
    },
    parse: function (object) {
      object.isCustom = _.isObject(object.view);
      return object;
    }
  });
  
  LV.Collections.CoversheetOptions = Backbone.Collection.extend({
    model: LV.Models.CoversheetOption,
    comparator: 'order',
    initialize: function (models, options) {
      options = options || {};
      var collection = this,
        defaultTabs = new Backbone.Collection([
          {label: 'Contact', id: 'contact', order: 0},
          {label: 'Medical', id: 'medical', order: 10},
          {label: 'Allergies', id: 'allergies', order: 20},
          {label: 'Inpatient Medications', id: 'inpatient', order: 30},
          {label: 'Outpatient Medications', id: 'outpatient', order: 40},
          {label: 'Surgeries', id: 'surgeries', order: 50},
          {label: 'Future Appointments', id: 'future', order: 60},
          {label: 'Hospitalizations', id: 'hospitalizations', order: 70}
        ]);
  
      if (_.isArray(options.TabsConfig)) {
        if (/^false$/i.test(options.mergeTabs)) {
          defaultTabs.reset(options.TabsConfig);
        }
        else {
          defaultTabs.add(options.TabsConfig, {merge: true});
        }
      }
  
      _.each(defaultTabs.models, function (model) {
        if (!/^false$/i.test(model.get('active'))) {
          collection.push(new collection.model(model.toJSON(), {parse: true}));
        }
      });
  
      collection.sort();
    }
  });
  
  LV.Views.ContactInfoTab = Backbone.Marionette.ItemView.extend({
    id: 'contact-info-view',
    tagName: 'div',
    className: '',
    template: function (data) {
      console.log('Contact Info Tab');
      console.log(data);
      data = data || {};
      var templateHelpers = {};
  
      data = _.extend(data, templateHelpers);
  
      var template = JST['html/coversheet/contactInfoTab.html'](data);
      return template;
    },
    events: {
    },
    initialize: function (options) {
      this.model = options.model;
    },
    onShow: function () {
    }
  });
  
  var medicationDetailFilter = '';
  
  LV.Views.MedicationsInfoTab = Backbone.Marionette.ItemView.extend({
    id: 'inpatient-medications-item-view',
    tagName: 'div',
    className: 'cover-sheet-table',
    template: function (data) {
      data = data || {};
      var templateHelpers = {
        getInstructions: function (objso) {
          if (objso.sig) { //nerf
            return '<br>' + objso.sig;
          } else {
            return ''; //or nuffin
          }
        },
        getActive: function (objso) {
          return objso.drugName;
        },
        getReason: function (objso) {
          if (!objso.nonVA) {
            return 'VA';
          } else {
            return 'NON-VA';
          }
        },
        getStatus: function (objso) {
          return objso.status;
        },
        dataRows: function () {
          var rows = [];
          var records = _.filter(this.medication, function (objso) {
            return objso.medicationSource === medicationDetailFilter;
          });
  
          _.each(records, function (objso, index, medications) {
            rows.push([this.getActive(objso) + this.getInstructions(objso), this.getStatus(objso), this.getReason(objso)]);
          }, this);
  
          return rows;
        },
        colHeaders: function () {
          return ['Active and Recently Expired', 'Status', 'Source'];
        }
      };
  
      data = _.extend(data, templateHelpers);
      var template = JST['html/coversheet/medicationsItemview.html'](data);
      return template;
    },
  
    initialize: function (opts) {
      this.model = opts.model;
  
      if (opts.isInpatient) {
        medicationDetailFilter = 'Inpatient';
      } else {
        medicationDetailFilter = 'Outpatient';
      }
    },
    onShow: function () {
  
    }
  
  });
  LV.Views.FutureTab = Backbone.Marionette.CompositeView.extend({
    id: 'appointment-info-view',
    template: function (data) {
      var template = JST['html/coversheet/appointmentInfoTab.html'](data);
      return template;
    },
    initialize: function (options) {
      this.model = options.model;
    },
    onShow: function () {
    }
  });
  
  
  LV.Views.MedicalTab = Backbone.Marionette.ItemView.extend({
    id: 'medical-view',
    tagName: 'div',
    className: '',
    template: function (data) {
      data = data || {};
      var templateHelpers = {
        isDiagnosis: function () {
          if (this.encounterDiagnosisICDCode.size === '') {
            return false;
          } else {
            return true;
          }
        }
      };
  
      data = _.extend(data, templateHelpers);
      var template = JST['html/coversheet/medicalTab.html'](data);
      return template;
    },
    events: {
    },
    initialize: function (options) {
      this.model = options.model;
    },
    onShow: function () {
    }
  });
  LV.Views.AllergiesInfoTab = Backbone.Marionette.CompositeView.extend({
    id: 'allergy-info-view',
    template: function (data) {
      var template = JST['html/coversheet/allergiesInfoTab.html'](data);
      return  template;
    },
    initialize: function (options) {
      this.model = options.model;
    },
    onShow: function () {
    }
  });
  LV.Views.SurgeriesInfoTab = Backbone.Marionette.CompositeView.extend({
    id: 'surgery-info-view',
    template: function (data) {
      var template = JST['html/coversheet/surgeriesInfoTab.html'](data);
      return  template;
    },
    initialize: function (options) {
      this.model = options.model;
    },
    onShow: function () {
    }
  });
  LV.Views.HospitalizationsInfoTab = Backbone.Marionette.CompositeView.extend({
    id: 'hospitalization-info-view',
    template: function (data) {
      var template = JST['html/coversheet/hospitalizationsInfoTab.html'](data);
      return  template;
    },
    initialize: function (options) {
      this.model = options.model;
    },
    onShow: function () {
    }
  });
  
  var Coversheet = Backbone.Marionette.LayoutView.extend({
    id: 'cover-sheet-view',
    tagName: 'div',
    className: '',
    template: function (data) {
      var template = JST['html/coversheet/lv-coversheet.html'](data);
      return _.template(template);
    },
    focusDelay: 500, //ms   focus into
    events: {
      'click .cover-tab': 'showTabbedSheetView',
      'click .panel-heading': 'showCollapseSheetView',
      'click .inpatient': 'showMedicationsView',
      'click .outpatient': 'showMedicationsView',
      'click .allergies': 'showAllergiesInfoTab',
      'click .medical': 'showMedicalDiagnosisTab',
      'click .surgeries': 'showSurgeriesInfoTab',
      'click .future': 'showFutureTab',
      'click .hospitalizations': 'showHospitalizationsInfoTab',
      'click .panel': 'focusInto',
      'click .cover-tab-container': 'focusInto'
    },
    regions: {
      'contactTab': '.contactTab',
      'medicalTab': '.medicalTab',
      'allergiesTab': '.allergiesTab',
      'inpatientTab': '.inpatientTab',
      'outpatientTab': '.outpatientTab',
      'surgeriesTab': '.surgeriesTab',
      'futureTab': '.futureTab',
      'hospitalizationsTab': '.hospitalizationsTab'
    },
    initialize: function (args) {
      //This constructor assumes both a json of model attributes and a reference to a marionette Application global object
      var view = this;
      args = args || {attributes: {}};
  
      cachedCoversheetData = new Backbone.Model();
  
      this.App = args.App;
      this.Patient = args.Patient || new Backbone.Model(args.attributes);
      this.App.coverLayout = {};
      this.coversheetOptions = new LV.Collections.CoversheetOptions(null, args);
      this.customTabView = null;
      this.headers = args.headers || false;
      this.async = args.async || false;
  
      this.cachedCoversheetData = cachedCoversheetData;
      this.isCached = isCached;
  
      if (args.ResourceDirectory instanceof Array) {
        this.ResourceDirectory = this.buildResourceDirectory(args.ResourceDirectory);
  
        this.addPatientLink();
      } 
      else {
        this.ResourceDirectory = args.ResourceDirectory;
      }
  
      this._createCustomViews();
      this._createCustomTabView(view, args);
  
      if ($.isEmptyObject(args.attributes)) {
  
        if (this.Patient.get('patientIdentifier') || this.Patient.get('patientId')) {
          this.patientId = this.Patient.get('patientIdentifier') ? this.Patient.get('patientIdentifier').uniqueId : this.Patient.get('patientId');
          this.setModelFromPatient();
        }
        else {
          this.trigger('error:coversheet');
        }
      } 
      else {
        this.setModelFromPatient();
      }
  
      //listen event triggers for each tab
      //this.listenTo(this.App.vent, 'goToCover', this.showCoverSheetTab); ?!?!?!
      this.model.set({'tabs': this.coversheetOptions.toArray()}, {});
      this.model.set('customTabs', !!(this.customTabView));
      this.model.set('desktopClass', (_.has(args, 'desktopClass') ? args.desktopClass : 'hidden-xs'));
      this.model.set('desktopTabClass', (_.has(args, 'desktopTabClass') ? args.desktopTabClass : 'col-sm-9'));
      this.listenTo(this.model, 'saveLocalModel', this.saveLocally);
      this.listenTo(this.model, 'fetch-done', function () {
        if (_.isFunction(this.fetchDone)) {
          this.fetchDone.call(this);
        }
      });
    },
    _createCustomTabView: function (view, args) {
      /*
       * A Custom View passed in as config to the new instance of the Coversheet
       * Takes all the existing handlers and rebinds them to the custom tabs.
       */
      if (args.CustomTabView) {
        _.extend(args.CustomTabView.prototype, {events: view.events});
        _.each(view.events, function (cb) {
          args.CustomTabView.prototype[cb] = function () {
            view[cb].apply(view, arguments);
          };
        });
        view.customTabView = new args.CustomTabView({collection: view.coversheetOptions, model: view.model});
      }
    },
    _createCustomViews: function () {
      var view = this;
      var regions = {};
      var customViews = _.filter(this.coversheetOptions.models, function (model) {
        return model.get('isCustom');
      });
  
      if (!customViews.length) {
        return;
      }
  
      /*
       * Allow for custom views for content
       * and callbacks to trigger the views
       * These are passed in as a config on the initialization
       */
      _.each(customViews, function (custom) {
        var id = custom.get('id');
        var properCaseId = id.substring(0, 1).toUpperCase() + id.substring(1);
        var tabIdClass = id + 'Tab';
        var callbackName = 'show' + properCaseId + 'Tab';
        var customCallback = custom.get('viewCallback');
        if (_.isFunction(customCallback)) {
          LV.Views[properCaseId + 'Tab'] = custom.get('view');
          view[callbackName] = function (e) {
            var callback = customCallback;
            callback.call(view, e, {
              viewName: properCaseId + 'Tab',
              id: id,
              tabName: tabIdClass
            });
          };
          view.events['click .' + id] = callbackName;
        }
        regions[tabIdClass] = '.' + tabIdClass;
      });
      view.addRegions(regions);
      view.delegateEvents();
    },
    setModelFromPatient: function () {
      this.model = new LV.Models.PatientFromCoverSheet(this.Patient.toJSON());
      this.saveLocally();
    },
    buildResourceDirectory: function (resources) {
      var resourceDirectory = resources[0];
  
      resources.splice(0, 1);
  
      _.each(resources, function (resource) {
        if (_.isFunction(resource.toJSON)) {
          resource = resource.toJSON();
        }
  
        resourceDirectory.directory = resourceDirectory.directory.concat(resource);
      });
  
      return resourceDirectory;
    },
    addPatientLink: function () {
      if (!this.ResourceDirectory.get('patient') && this.ResourceDirectory.get('patients')) {
        var patientLink = {
          title: 'patient',
          href: this.ResourceDirectory.get('patients').href.slice(0, -1)
        };
  
        this.ResourceDirectory.directory.push(patientLink);
      }
    },
    saveLocally: function () {
      cachedCoversheetData.set('patient', this.model.toJSON());
    },
    onRender: function () {
      this.showContactTab();
    },
    showMedicationsView: function (e) {
      var isInpatient = $(e.currentTarget).hasClass('inpatient');
      var tabUrl      = 'medications/scope/operational';
      var options;
      var params;
  
      var callBack = function () {
        if (isInpatient) {
          options = this.coversheetOptions.findWhere({id: 'inpatient'});
          this.model.set({
            'headers': this.headers,
            'label': options.get('label')
          });
          this.App.coverLayout.inpatientMedications = new LV.Views.MedicationsInfoTab({model: this.model, isInpatient: true});
          this.inpatientTab.show(this.App.coverLayout.inpatientMedications);
        } 
        else {
          options = this.coversheetOptions.findWhere({id: 'outpatient'});
          this.model.set({
            'headers': this.headers,
            'label': options.get('label')
          });
          this.App.coverLayout.outpatientMedications = new LV.Views.MedicationsInfoTab({model: this.model, isInpatient: false});
          this.outpatientTab.show(this.App.coverLayout.outpatientMedications);
        }
  
        params = options.get('params');
      };
  
      this.processView({tab: 'medicationTab', newModel: false, tabUrl: tabUrl, params: params, callback: callBack});
    },
    showContactTab: function (e) {
      var tabUrl = 'demographics/scope/operational';
      var options = this.coversheetOptions.findWhere({id: 'contact'});
      var params = options.get('params');
     
      var callBack = function () {
        this.model.set({
          'headers': this.headers,
          'label': options.get('label')
        });
        this.App.coverLayout.contact = new LV.Views.ContactInfoTab({model: this.model});
        this.contactTab.show(this.App.coverLayout.contact);
      };
  
      this.processView({tab: 'coverTab', newModel: false, tabUrl: tabUrl, params: params, callback: callBack});
    },
    showAllergiesInfoTab: function (e) {
      var tabUrl = 'allergies/scope/operational';
      var options = this.coversheetOptions.findWhere({id: 'allergies'});
      var params = options.get('params');
      
      var callBack = function () {
        this.model.set({
          'headers': this.headers,
          'label': options.get('label')
        });
        this.App.coverLayout.allergies = new LV.Views.AllergiesInfoTab({model: this.model});
        this.allergiesTab.show(this.App.coverLayout.allergies);
      };
  
      this.processView({tab: 'allergiesTab', newModel: false, tabUrl: tabUrl, params: params, callback: callBack});
    },
    showSurgeriesInfoTab: function (e) {
      var tabUrl = 'surgeries/scope/operational';
      var options = this.coversheetOptions.findWhere({id: 'surgeries'});
      var params = options.get('params');
      
      var callBack = function () {
        this.model.set({
          'headers': this.headers,
          'label': options.get('label')
        });
        this.App.coverLayout.surgeries = new LV.Views.SurgeriesInfoTab({model: this.model});
        this.surgeriesTab.show(this.App.coverLayout.surgeries);
      };
  
      this.processView({tab: 'surgeriesTab', newModel: false, tabUrl: tabUrl, params: params, callback: callBack});
    },
    showHospitalizationsInfoTab: function (e) {
      var tabUrl = 'admissions/scope/longitudinal';
      var options = this.coversheetOptions.findWhere({id: 'hospitalizations'});
      var params = options.get('params');
      
      var callBack = function () {
        this.model.set({
          'headers': this.headers,
          'label': options.get('label')
        });
        this.App.coverLayout.hospitalizations = new LV.Views.HospitalizationsInfoTab({model: this.model});
        this.hospitalizationsTab.show(this.App.coverLayout.hospitalizations);
      };
  
      this.processView({tab: 'hospitalizationsTab', newModel: false, tabUrl: tabUrl, params: params, callback: callBack});
    },
    showMedicalDiagnosisTab: function (e) {
      var tabUrl = '/problem-list-diagnoses';
      var options = this.coversheetOptions.findWhere({id: 'medical'});
      var params = options.get('params');
      
      var callBack = function () {
        this.model.set({
          'headers': this.headers,
          'label': options.get('label')
        });
        this.App.coverLayout.medical = new LV.Views.MedicalTab({model: this.model});
        this.medicalTab.show(this.App.coverLayout.medical);
      };
  
      this.processView({tab: 'diagnosisTab', newModel: false, tabUrl: tabUrl, params: params, callback: callBack});
    },
    showFutureTab: function (e) {
      var tabUrl = 'appointments/future';
      var options = this.coversheetOptions.findWhere({id: 'future'});
      var params = options.get('params');
      
      var callBack = function () {
        this.model.set({
          'headers': this.headers,
          'label': options.get('label')
        });
        this.App.coverLayout.future = new LV.Views.FutureTab({model: this.model});
        this.futureTab.show(this.App.coverLayout.future);
      };
  
      this.processView({tab: 'futureTab', newModel: true, tabUrl: tabUrl, params: params, callback: callBack});
    },
    showTabbedSheetView: function (e) {
      this.trigger('session:continue');
      var $tab = $(e.currentTarget);
      $tab.parent().siblings('.active').children().removeAttr('aria-label');
      $tab.attr('aria-label', $tab.text().trim() + ', Active');
    },
    showCollapseSheetView: function (e) {
      this.trigger('session:continue');
      var $collapse = $(e.currentTarget);
      if ($collapse.hasClass('collapsed')) {    //This panel is opening, we label it Open.
        $collapse.attr('aria-label', $collapse.text().trim() + ', Open');
      } else {
        $collapse.removeAttr('aria-label');     //This panel is collapsing, we remove the label.
      }
    },
    focusInto: function (e) {
      var $a = $(e.currentTarget).find('a');
      var dataToggle = $a.attr('data-toggle');
      var $tag = null;
      if (dataToggle === 'pill') {
        $tag = $($a.attr('href'));
      } else if (dataToggle === 'collapse') {
        $tag = $($a.attr('data-target'));
      } else {
        return;
      }
      if ($tag.length > 0) {
        setTimeout(function () {
          var $focusOnMe = $tag.find('.focusinto');
          $focusOnMe.focus();
        }, this.focusDelay);
      }
    },
    /**
     * Common function to handle the
     * retrieving of a model and processing
     * the view to show
     */
    processView: function (config) {
      if (isCached(config.tab)) {
        if (config.newModel) {
          this.model = new LV.Models.PatientFromCoverSheet(this.cachedCoversheetData.get('patient'));
        }
        else {
          this.model.setParse(this.cachedCoversheetData.get('patient'), true);
        }
        return config.callback.call(this);
      }
      this.fetchDone = config.callback;
      this.model.fetchFromMDWS(this.patientId, config.tabUrl, this.ResourceDirectory, this.async, config.params);
      this.cachedCoversheetData.set(config.tab, true);
    }
  });
  
  LV.Views.Coversheet = LV.Coversheet = Coversheet;
  

  var DefaultCarouselItem = Backbone.Marionette.CompositeView.extend({
      template: function () {
          var template = JST['html/carousel/carousel-item.html']();
          return _.template(template);
      },
      attributes: function () {
          return {
              class: 'item' + (this.options.active ? ' active' : ''),
              'data-index': this.options.index
          };
      }
  });
  
  var Carousel = Backbone.Marionette.CompositeView.extend({
      template: function () {
          var template = JST['html/carousel/lv-carousel.html']();
          return _.template(template);
      },
      itemView: DefaultCarouselItem,
      itemViewContainer: '.carousel-inner',
      attributes: function () {
          return {
              id: 'carousel-generic',
              class: 'carousel slide',
              'data-wrap': this.options.wrap || false,
              'data-interval': this.options.interval || false
          };
      },
      ui: {
          leftBtn: '.left',
          leftSection: '.carousel-left-section',
          rightBtn: '.right',
          rightSection: '.carousel-right-section'
      },
      events: {
          'click .left': 'showPrevSlide',
          'click .right': 'showNextSlide'
      },
      initialize: function () {
          _.bindAll(this, 'updateButtons');
  
          if (!this.options.wrap) {
              var that = this;
  
              this.$el.on('slid.bs.carousel', function (direction, relativeTarget) {
                  that.updateButtons(relativeTarget);
              });
          }
      },
      onShow: function () {
          if (!this.options.wrap) {
              this.updateButtons();
          }
  
          if (this.options.leftSection) {
              this.options.leftSection.render();
              this.ui.leftSection.html(this.options.leftSection.$el);
          }
  
          if (this.options.rightSection) {
              this.options.leftSection.render();
              this.ui.leftSection.html(this.options.rightSection.$el);
          }
      },
      itemViewOptions: function (model, index) {
          return {
              active: index === this.options.startAt || 0,
              index: index
          };
      },
      enableButtons: function () {
          this.ui.leftBtn[0].disabled = false;
          this.ui.rightBtn[0].disabled = false;
      },
      updateButtons: function (item) {
          var $item = item ? $(item) : this.$el.find('.active');
  
          this.enableButtons();
  
          if ($item.is(':first-child')) {
              this.ui.leftBtn[0].disabled = true;
              this.ui.rightBtn.focus();
          }
  
          if ($item.is(':last-child')) {
              this.ui.rightBtn[0].disabled = true;
              this.ui.leftBtn.focus();
          }
      },
      showPrevSlide: function () {
          this.$el.carousel('prev');
      },
      showNextSlide: function () {
          this.$el.carousel('next');
      }
  });
  
  LV.Views.Carousel = LV.Carousel = Carousel;
  

  root.LV = LV;

}));
