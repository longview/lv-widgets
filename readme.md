# lv-widgets

A collection of 508-compliant utilities for use in a Backbone.Marionette web application.

Adds the LV global namespace to the window.

Dependencies: Bower, jQuery, Underscore, Backbone, Marionette, 

## Setup

bower.json

```bash
{
"lv-widgets": "https://bitbucket.org/longview/lv-widgets.git#1.0.4"
}

```
```bash
bower install

```

## Usage - Utilities

# Ajax Loader
Adds a default ajax loader to all ajax requests via jQuery.
```javascript
//Instantiation
App.AjaxLoader = new LV.AjaxLoader();

```

# Dateman
```javascript
//Instantiation
App.Dateman = new LV.Dateman();

//Methods
App.Dateman.calculateAge(new Date());

```

# Resources
```javascript
//Instantiation
App.Resources = new LV.Resources('MobileHealthPlatformWeb');

//Methods
App.Resources.get('token').href;

App.Resources.isLoggedIn();

//READ THIS
When instansiating lv.resources, it expects a single parameter, $1. This parameter tells the object where to get the resource list from. Specifically, the parameter points to the 
backend resources you wish to utilize. This is assuming that the backend resources you point to both exists, and can perform the rest call [ /<$1>/rest/public/resource-directory ].
For most applications, there will always be both a front-end and a back-end application existing on the webserver. It is the job of the backend developer to coordinate with teh front end developer and make available this resource-directory URI providing a list of further restfull api's to be utilized within your front-end app. LV-Widgets expects this design pattern.
```

## Usage - Views

# EULA View
```javascript
//Instantiation
App.views.eulaView = new LV.Views.EulaView({app: App.name});

//Mixin
LV.Views.EulaView.extend({
  decline: function () {}
});

```

# Popup Layout
```javascript
//Instantiation
App.views.popupLayout = new LV.Views.PopupLayout(options) //the available options are listed below

App.popupRegion.show(popupLayout);

App.views.popupLayout.content.show(yourPopupView)

//READ THIS
This creates a basic configurable popup layout, which includes a header and a footer with one or two buttons.  
The view you would show in the layout would be the content between the header and footer.  

The available options are listed below:

*  title:   The title of the popup.  If left blank, the popup will render without a header
*  popupClass:   Allows you to set a class for the whole popup
*  contentClass:   Allows you to set a class for the content section of the popup
*  hideCloseBtn:   Set this flag to true to hide the 'X' close button at the top left of the popup
*  scope:   Allows you to set a scope for the button action callback functions.  Scope gets passed as the first parameter in the action callback.  It is used in place of "this".
*  buttons:   An array of objects, which allows you to configure the buttons shown on the popup.  
   You can specify up to two buttons.  Any extra will be ignored.  
   If this isn't set, it will default to a single "OK" button which simply closes the popup.
  
   The format is as follows: 
    -     title:   The title shown on the button
    -     action:   A callback function that is called when the button is clicked.  If you specified a scope for the function, it is passed as the first parameter
    -     icon:   Corresponds with the "icon" class.  Specify a class to show an icon with the button.
```

# Refresh View
```javascript
//Instantiation
App.views.refreshView = new LV.Views.RefreshView(options) //the available options are listed below

[someRegion].show(App.views.refreshView);

//READ THIS
This creates a basic configurable refresh view, which allows controls of formatting time, display label, and the refresh icon, etc...  

The available options are listed below:

* label: The label for the refresh icon, sets default to 'Last Refresh'
* labelClass: A class used to control the look of the label, defaults to 'bold' for a bold look
* format: Used to format the Refresh Date
*   -if a custom format is specified, moment library is required, defaults to 'MM/DD/YYYY HH:mm'
* widgetAttrs: A JSON Object of attributes to set on the top level element - refresh-widget
* refreshIcon: Set the classes for the refresh icon, defaults to 'glyphicon glyphicon-refresh'
* boldLabel: Used to toggle bold class on label, defaults to true to show bold
* disabled: Used to toggle the refresh button as enabled, disabled, defaults to 'false'
* fetchables: An object or array of objects that have a fetch method and are deferrable.
* finalizeCallback: A callback function that executes after all the callables are done
```

# Tab Layout
```javascript
//Mixin
LV.Views.TabLayout.extend({
  tabs: {
    'Grams': FilterGramView,
    'Search': FilterSearchView
  },
  defaultTab: 'Grams'
});

```

# Coversheet Instantiation
```javascript

new LV.Views.CoverSheet({
    App: <a marionette application object>,
    ResourceDirectory: <an lv-resources object>
});

```

Because CoverSheet was designed to be a layout view, there are several child views that make up the regions of coversheet.
These item views are available within the LV.Views namespace, but serve no purpose outside the context of coversheet.
Developers who encounter issues with coversheet may contact the authors of coversheet for future assistance.

Will: wpreston@synergybis.com
Almas: almasb@synergybis.com
Darien:dfoster@synergybis.com

```

# MyList Lib
```javascript

MyList Lib is just a reusable config object that is baked in with MyList.
MyList Lib provides useful help functions for processing MyList data.

MyList Lib can be used as a base for a custom MyList user interface.


```

# MyList Instantiation
```javascript

//Instantiation
App.<views.>MyList = new LV.MyList(options);

MyList is a Marionette Layout by default, therefore there are at least two ways of appending it to the DOM.

[someRegion].show(App.views.refreshView);
-- OR --
pass in an el: <existing DOM selector> as an option and call render.


//READ THIS
This is a resuable component that can essentially plug and play into any Backbone/Marionette site.  Site specific styling will most likely need to be implemented.

The available options are listed below:

* Resources: An LV-Resources object
* Vent: An Object capable of trigging and listening to events.
* closeBtn: boolean | default: true, to show a close button (typically for popups)
* any standard Marionette view options
*     i.e. el, className, attributes, etc...



```

# LoginUtils
```javascript

//Instatiation
new LV.Resources({
    loginUtilsOpts: {
        appName: 'my-app',
        resourcesLink: '../my-app-resources',
        authorizeUrl: '../AuthorizationServices/provider/authorize',
        lastAccessUrl: '../AuthorizationServices/rest/resourceLastAccessedTime'
    }
...

//READ THIS
This is a FE library to correctly handle retrieving a token according to OAUTH2.0 standards.  

This library is already incorporated into LV.Resources, but requires the following:
* appName: The name of the app as seen in the address bar
* resourcesLink: The location of the BE resources where the Oauth-lib was implemented
* authorizeUrl: The authorization url.  For a staff-facing app, generally: '../AuthorizationServices/provider/authorize'; For a vet-facing app, generally: '../ssoeproxy/veteran/authorize'
* lastAccessUrl: The url for the resourceLastAccessedTime resource, generally: '../AuthorizationServices/rest/resourceLastAccessedTime'


In addition, the following options are available:
* autoRedirect: Set this to true to automatically redirect to the login screen when a user is not logged in.  Set to false by default
* redirectUri: A custom redirect URI following authentication
* onReceiveToken: A callback specifying custom behavior after the token is received.  The token is passed into the callback as a parameter

```