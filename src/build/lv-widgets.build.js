(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['jquery', 'underscore', 'backbone', 'marionette'], function ($, _, Backbone) {
      root = factory(root, $, _, Backbone);
    });
  } else {
    root = factory(root, root.jQuery, root._, root.Backbone);
  }

}(this, function (root, $, _, Backbone) {

  var previousLV = root.LV;

  // @include ../../tmp/templates.js
  // @include ../../tmp/backbone.paginator.bare.js

  var JST = this['JST'];
  var LV = {};
  LV.VERSION = '<%= version %>';
  LV.Views = {};
  LV.Models = {};
  LV.Collections = {};
  LV.noConflict = function() {
    root.LV = previousLV;
    return this;
  };

  // @include ../js/lv-loginUtils.js
  // @include ../js/lv-ajax-loader.js
  // @include ../js/lv-dateman.js
  // @include ../js/lv-resources.js
  // @include ../js/lv-session.js
  // @include ../js/lv-popup-region.js

  // @include ../js/lv-view-eula.js
  // @include ../js/lv-view-tab-layout.js
  // @include ../js/lv-view-popup-layout.js
  // @include ../js/lv-view-refresh.js
  // @include ../js/lv-accessibility.js
  // @include ../js/lv-popover-widget.js

  // @include ../js/lv-mylist-lib.js
  // @include ../js/lv-mylist.js

  // @include ../js/lv-view-coversheet.js

  // @include ../js/lv-view-carousel.js

  root.LV = LV;

}));
