//Author: Almas Barzhaxynov
/*
 * This is a jquery powered wrapper object.
 * Given aria tags to elements with ariaTrue or ariaFalse classes. It was created so that developers would need to spend
 * less time testing for 508 and continue development on other parts of the project
 *
 * Aria tags will be added when add:aria event is triggered in Show methods.
 */

LV.Accessibility = function (opts) {
  var options = opts || {};
  $(this.selector).on('add:aria', this.addAria);
};
_.extend(LV.Accessibility.prototype, {
  addAria: function () {
    var $trueAria = $('.trueAria');
    var $falseAria = $('.falseAria');

    $trueAria.each(function(index) {
      $(this).attr('tabindex', 0);
      $(this).attr('aria-hidden', 'false');
      if(this.tagName === 'BUTTON') {
        $(this).attr('role', 'button');
      }
      if(this.tagName === 'A') {
        $(this).attr('role', 'link');
      }
    });

    $falseAria.each(function(index) {
      $(this).attr('tabindex', -1);
      $(this).attr('aria-hidden', true);
    });
  }
});
