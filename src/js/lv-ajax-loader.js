//Ajax Loader
// ---------

var appendHTML = function ($el) {
  $el.append('<div class="hide-me ajax-loading" aria-label="loading results"><span class="loader-icon loading"></span></div>');
};

LV.AjaxLoader = function (opts) {
  var options = opts || {};
  this.$el = options.el || $('body');
  appendHTML(this.$el);
  this.$loader = options.loader || $('.ajax-loading');
  this.initialize.apply(this, arguments);
};

_.extend(LV.AjaxLoader.prototype, Backbone.Events, {
  initialize: function () {
    var _self = this;
    $(document).ajaxStart(function () {
      _self.show();
    }).ajaxStop(function () {
      _self.hide();
    }).ajaxError(function (event, jqxhr, settings, thrownError) {
      if (thrownError === 'timeout') {
        _self.trigger('timeout');
      }
    });
  },
  show: function () {
    this.$loader.show();
  },
  hide: function () {
    this.$loader.hide();
  }
});
