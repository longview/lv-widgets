LV.LoginUtils = function (options) {
  options = options || {};

  var appName = options.appName || '';
  var resources = options.resources || { get: function () { return null; } };
  var resourcesLink = options.resourcesLink || '';
  var authorizeUrl = resources.get('oauth-authorize') ? resources.get('oauth-authorize').get('href') : options.authorizeUrl || '';
  var lastAccessUrl = resources.get('last-accesstime') ? resources.get('last-accesstime').get('href') : options.lastAccessUrl || '';
  var noToken = typeof options.noToken === 'function' ? options.noToken : function () { };
  var autoRedirect = options.autoRedirect || false;
  var redirectUri = options.redirectUri || '';
  var onReceiveToken = typeof options.onReceiveToken === 'function' ? 
    options.onReceiveToken : 
    function (token) {
      $.ajaxPrefilter(function (options, originalOptions, jqXHR) {
        jqXHR.setRequestHeader('Authorization', 'Bearer ' + token);
      });
    };
  var that = this;

  var clientId;
  var generateState = function () {
    var state = appName + '-' + new Date().getTime();

    window.sessionStorage.setItem(appName + '-state', state);

    return state;
  };

  var getParams = function (url) {
    url = url || window.location.href;

    var params = {},
      queryString = url.split('?')[1] || '',
      queryParams = queryString.split('&') || [];

    queryParams.forEach(function(param) {
      param = param.split('=');
      
      params[decodeURIComponent(param[0])] = decodeURIComponent(param[1]);
    });

    return params;
  };

  var authorizeCordova = function (url) {
    var ref = window.open(url, '_blank', 'location=no,toolbar=no');
    ref.addEventListener('loadstart', function (e) {
      var params = getParams(e.url);

      var code = params.code;
      var clientState = params.state;

      if (code && clientState) {
        window.sessionStorage.setItem('code', code);
        window.sessionStorage.setItem('clientState', clientState);
        window.location.reload();
        ref.close();
      }
    });
  };

  $.ajax({
    url: resourcesLink + '/oauth/info',
    async: false,
    success: function (response) {
      clientId = response.clientId || '';
      redirectUri = redirectUri || response.redirectUri || '';
    }
  });

  this.authorize = function () {
    var authorizeParams = [
      'response_type=code',
      'state=' + generateState(),
      'client_id=' + clientId,
      'redirect_uri=' + redirectUri,
      'scope=read'
    ];

    var url = authorizeUrl + '?' + authorizeParams.join('&');

    if (window.cordova) {
      authorizeCordova(url);
    } else {
      window.location = url;
    }
  };

  this.checkForAuthCode = function () {
    var params = getParams();

    var code = window.sessionStorage.getItem('code') || params.code;
    var clientState = window.sessionStorage.getItem('clientState') || params.state;
    var sessionState = window.sessionStorage.getItem(appName + '-state');
    var token;

    window.sessionStorage.removeItem('code');
    window.sessionStorage.removeItem('clientState');
    window.sessionStorage.removeItem(appName + '-state');
    
    if (!window.cordova) {
      window.history.replaceState({}, '', '/' + appName + '/' + window.location.hash);
    }

    if (!code || !clientState) {
      try {
        token = JSON.parse(window.sessionStorage.getItem('token'));
      } catch (e) {
        token = window.sessionStorage.getItem('token');
      }
    } else if (code && clientState && clientState === sessionState) {
      resourcesLink += '/oauth/token?code=' + code + '&redirect_uri=' + redirectUri;

      $.ajax({
        url: resourcesLink,
        async: false,
        headers: {
          'accept': 'application/json'
        },
        success: function (response) {
          token = response.access_token;

          window.sessionStorage.setItem('token', JSON.stringify(token));
        }
      });
    }

    if (token) {
      onReceiveToken(token);
    } else {
      noToken();
      if (autoRedirect) {
        this.authorize();
      }
    }
  };
};