//MyList Library
// ---------
/*
 * Below you will find a collection of methods useful for creating a front end application 
 * for the mylist back end.
 *
 */

LV.MyListLib = {
  insertURLPath: function (base, key, val) {
    //find the path that matches the key and insert the val in the following path
    // erg /opo/###/ko/
    //      ^^^
    //      key
    // erg /opo/###/ko/
    //          ^^^
    //          val
    base = base.split('%23').join('');
    var findMe = '/' + key + '/';
    var splitAt = base.indexOf(findMe) + findMe.length;
    var A = base.substring(0, splitAt);
    var B = base.substring(splitAt);
    return A + val + B;
  },
  convertBooleanArrayToMyListsNumber: function (bArr) {
    /* given an array of true or false values representing a binary string, convert it into a base ten integer. */
    bArr.reverse();
    var sumOfLists = 0;
    var factor = 1;
    for (var i = 0; i < bArr.length; i++) {
      if (bArr[i]) {
        sumOfLists += factor;
      }
      factor *= 2;
    }

    return sumOfLists;
  },
  convertMyListsNumberToBooleanArray: function (bnum) {
    /* given an integer number, transform it into an array of booleans representing the bits of a binary number 0 or 1 (base two) */

    var boolBits = [];
    var result = 0;
    while (bnum > 0) {
      result = bnum % 2;
      boolBits.push(!!result);
      bnum = Math.floor(bnum / 2);
    }

    while (boolBits.length < 4) {
      boolBits.push(false);
    }

    return boolBits.reverse();
  },
  createAriaLabel: function (mylistNumber, listNames) {
    //given a mylists number and an array of listNames, create a sentence explaining which of the user defined lists belong to the given integer

    var label = '';
    var line1 = 'Patient belongs to the following list: ';
    var line2 = 'Patient belongs to the following lists: ';
    var lineZero = 'Patient belongs to zero lists.';
    var isOnLists = this.convertMyListsNumberToBooleanArray(mylistNumber).reverse();

    var count = 0;
    _.each(isOnLists, function (isOnThisList, index, lists) {
      if (!isOnThisList) {
        return;
      } else {
        count++;
      }

      if (count > 1) {
        label += ' and ' + listNames[index];
      } else {
        label += ' ' + listNames[index];
      }

    });

    if(!label){
      label = '';
    }

    if (count < 1) {
      label = lineZero;
    } else if (count > 1) {
      label = line2 + label;
    } else {
      label = line1 + label;
    }
    return label;
  },
  createMyListTitleModel: function (url) {	//one must build the full url to be passed in
    var Model = Backbone.Model.extend({
      idAttribute: 'id',
      defaults: {
        listOneName: 'My List One',
        listTwoName: 'My List Two',
        listThreeName: 'My List Three',
        listFourName: 'My List Four',
        staffId: '',
        facilityCode: ''
      },
      initialize: function (args) {
        this.url = args.url;
      },
      fetchTitles: function () {
        if (typeof this.url === 'undefined' || this.url === null) {
          console.log('please provide the list model with a correctly built restful url.');
          return;
        }
        this.fetch({
          dataType: 'json',
          reset: true,
          async: false
        }).fail(function (jqXHR) {
          if (jqXHR.status === 400) {
            this.trigger('my-list-titles:error', $.parseJSON(jqXHR.responseText).message);
          }
        });
      },
      getTitlesArr: function () {
        return [this.get('listOneName'), this.get('listTwoName'), this.get('listThreeName'), this.get('listFourName')];
      },
      createAriaLabel: function (mylistNumber, listNames) {
        //given a mylists number and an array of listNames, create a sentence explaining which of the user defined lists belong to the given integer

        var label = '';
        var line1 = 'Patient belongs to the following list: ';
        var line2 = 'Patient belongs to the following lists: ';
        var lineZero = 'Patient belongs to zero lists.';
        var isOnLists = this.convertMyListsNumberToBooleanArray(mylistNumber).reverse();

        var count = 0;
        _.each(isOnLists, function (isOnThisList, index, lists) {
          if (!isOnThisList) {
            return;
          } else {
            count++;
          }

          if (count > 1) {
            label += ' and ' + listNames[index];
          } else {
            label += ' ' + listNames[index];
          }

        });

        if (count < 1) {
          label = lineZero;
        } else if (count > 1) {
          label = line2 + label;
        } else {
          label = line1 + label;
        }
        return label;
      },
      parse: function (response) {
        if (typeof response.listOneName !== 'undefined') {
          response.listOneName = LV.MyListLib.unescape(response.listOneName);
          response.listTwoName = LV.MyListLib.unescape(response.listTwoName);
          response.listThreeName = LV.MyListLib.unescape(response.listThreeName);
          response.listFourName = LV.MyListLib.unescape(response.listFourName);
        }

        return response;
      }
    });
    return  new Model({url: url});
  },
  unescape: function (line) {
    line = line || '';

    var funnyStuff = [
      {
        change: '&quot;',
        to: '\"'
      },
      {
        change: '&lt;',
        to: '<'
      },
      {
        change: '&gt;',
        to: '>'
      },
      {
        change: '&amp;',
        to: '&'
      }
    ];

    _.each(funnyStuff, function (mapping, index, arr) {
      var pieces = line.split(mapping.change);
      line = pieces.join(mapping.to);
    });
    return line;
  },
  createMyListPatientModel: function (url) {
    var Model = Backbone.Model.extend({
      numMyLists: 4,
      defaults: {
        'id': null,
        'displayName': '',
        'firstName': '',
        'lastName': '',
        'middleName': '',
        'gender': '',
        'location': '',
        'ssn': '',
        'dateOfBirth': -2209057200000,
        'facilityCode': '',
        'staffId': '',
        'patientId': '',
        'notesListOne': '',
        'notesListTwo': '',
        'notesListThree': '',
        'notesListFour': '',
        'myLists': 0

      },
      initialize: function (args) {
        this.url = args.url;
      },
      patientFetch: function () {
        if (typeof this.url === 'undefined' || this.url === null) {
          console.log('please provide the mylist patient model with a correctly built restful url.');
          return;
        }
        this.fetch({
          dataType: 'json',
          reset: true,
          async: false
        }).fail(function (jqXHR) {
          if (jqXHR.status === 400) {
            this.trigger('patient-details:error', $.parseJSON(jqXHR.responseText).message);
          }
        });
      },
      parse: function (response) {
        var patient = this.toJSON();
        if (typeof response.id !== 'undefined' && response.id.length > 0) {
          patient = response;
          patient.notesListFour = LV.MyListLib.unescape(response.notesListFour);
          patient.notesListThree = LV.MyListLib.unescape(response.notesListThree);
          patient.notesListTwo = LV.MyListLib.unescape(response.notesListTwo);
          patient.notesListOne = LV.MyListLib.unescape(response.notesListOne);
        }
        return patient;
      },
      getNotesArr: function () {
        if (this.isExistingOnMongo()) {
          return [this.get('notesListOne'), this.get('notesListTwo'), this.get('notesListThree'), this.get('notesListFour')];
        } else {
          return ['', '', '', ''];
        }
      },
      isOnListsArr: function () {
        return LV.MyListLib.convertMyListsNumberToBooleanArray(this.get('myLists')).reverse();
      },
      isAlreadySavedToMongo: function () {
        return this.get('id') !== null;
      },
      isOnAnyMyLists: function () {
        return this.get('myLists') > 0;
      },
      assignPatientToMyLists: function (mylistArray) {
        if (mylistArray.length !== 4) {
          console.log('array provided to assignPatientToMyLists must be of length exactly 4');
          return false;
        }
        //change which lists the patient is on by providing an array of booleans represting which lists the patienet is being added to
        this.set('myLists', LV.MyListLib.convertBooleanArrayToMyListsNumber(mylistArray));
        return true;
      },
      getIconClasses: function () {
        //inject these class names into the class property of an <i> tag to show the icon
        return 'icon binary-icon-' + this.get('myLists');
      }
    });
    return new Model({url: url});
  }
};
