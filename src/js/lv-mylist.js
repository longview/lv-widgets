var Resources,
  vent,
  basePath = 'PatientViewerServices/',
  myListBasePath = 'my-list-resource';

function MY_LIST_LIMIT() {
  return 4;
}

function getTemplate(template) {
  return JST['html/mylist/' + template + '.html'];
}

function getMyListLib() {
  return LV.MyListLib;
}

var MyListTitles = Backbone.Model.extend({
  idAttribute: 'id',
  defaults: {
    listOneName: 'My List One',
    listTwoName: 'My List Two',
    listThreeName: 'My List Three',
    listFourName: 'My List Four',
    staffId: '',
    facilityCode: ''
  },
  url: function () {
    var line = (
      Resources.get('my-list-titles') ||
    {href: '../' + myListBasePath + '/rest/my-list-search/site/%23/id/%23/my-list-titles?_=1419000353539'}
      ).href;

    line = getMyListLib().insertURLPath(line, 'id', Resources.user.id);
    line = getMyListLib().insertURLPath(line, 'site', Resources.user.vistaLocation);

    return line;
  },
  getTitleArr: function () {
    return [this.get('listOneName'), this.get('listTwoName'), this.get('listThreeName'), this.get('listFourName')];
  },
  parse: function (response) {
    var that = this;

    response = response || {};
    _.each(
      _.filter(
        _.keys(this.defaults), function (prop) {
          return /^list/.test(prop);
        }
      ), function (item) {
        response[item] = getMyListLib().unescape(response[item]) || that.defaults[item];
      }
    );
    return response;
  },
  updateTitles: function (models) {
    var changed = false;
    _.each(models, function (model) {
      if (!changed) {
        changed = _.has(model.changed, 'name');
      }
      this.set(model.get('code'), model.get('name'));
    }, this);
    if (changed) {
      this.save();
    }
  }
});

var AMyList = Backbone.Model.extend({
  defaults: {
    name: 'My List One',
    code: 'listOneName',
    index: 1,
    active: true,
    edit: false
  }
});

var MyLists = Backbone.Collection.extend({
  model: AMyList,
  initialize: function (options) {
    var collection = this,
      curModel,
      clone,
      keys;

    var LIMIT = MY_LIST_LIMIT();
    if (this.length === LIMIT) {
      return;
    }
    while (LIMIT--) {
      var listIndex = this.length,
        wordIndex;
      switch (++listIndex) {
        case 1:
          wordIndex = 'One';
          break;
        case 2:
          wordIndex = 'Two';
          break;
        case 3:
          wordIndex = 'Three';
          break;
        case 4:
          wordIndex = 'Four';
          break;
        default:
          wordIndex = '(Unrecognized List)';
          break;
      }
      this.push(new this.model({name: 'My List ' + wordIndex, index: listIndex, code: 'list' + wordIndex + 'Name'}));
    }

    clone = new Backbone.Collection(this.models);

    options.myListTitles.deferred.done(function () {
      _.each(_.keys(options.myListTitles.toJSON()), function (key) {
        if (/^list\w+Name$/.test(key)) {
          curModel = clone.findWhere({code: key});
          curModel.set({name: options.myListTitles.get(key)});
        }
      });
      collection.reset(clone.models);
    });
  },
  setEdit: function (isEdit) {
    _.each(this.models, function (model) {
      model.set('edit', (isEdit || false));
    });
  }
});

var PatientInfo = Backbone.Model.extend({
  defaults: {
    dateOfBirth: '',
    firstName: '',
    gender: '',
    lastName: '',
    patientIdentifier: {
      assigningAuthority: '',
      uniqueId: ''
    },
    ssn: '',
    wardLocation: ''
  },
  initialize: function (opts) {
    var options = opts || {};

    this.url = '../PatientViewerServices/rest/patient/dfn-' + Resources.user.vistaLocation + '/' + options.patientId;
  },
  parse: function (response) {
    response.displayName = response.lastName + ', ' + response.firstName;

    return this.flattenLinks(response);
  },
  flattenLinks: function (object) {
    var links = object.link;
    if (_.isArray(links)) {
      var size = links.length;

      for (var i = 0; i < size; i++) {
        if (links[i].title) {
          object[links[i].title] = links[i];
        }
        else {
          object.self = links[i];
        }
      }
    }
    delete object.link;
    return object;
  }
});

var Patient = Backbone.Model.extend({
  url: function () {
    return '../' + myListBasePath + '/rest/my-list-search/site/' + Resources.user.vistaLocation + '/id/' + Resources.user.id + '/my-list-patients';
  },
  defaults: {
    'id': null,
    'displayName': '',
    'firstName': '',
    'lastName': '',
    'middleName': '',
    'gender': '',
    'location': '',
    'ssn': '',
    'dateOfBirth': -2209057200000,
    'facilityCode': '',
    'staffId': '',
    'patientId': '',
    'notesListOne': '',
    'notesListTwo': '',
    'notesListThree': '',
    'notesListFour': '',
    'myLists': 0

  },
  initialize: function () {

  },
  fetch: function (patientId) {
    this.clear();
    if (typeof patientId === 'undefined') {
      patientId = this.get('patientId');
    }
    var line = '../' + myListBasePath + '/rest/my-list-search/site/%23%23%23/id/%23%23/my-list-patient/';
    line = getMyListLib().insertURLPath(line, 'id', Resources.user.id);
    line = getMyListLib().insertURLPath(line, 'site', Resources.user.vistaLocation);
    var restURL = line + '?patientId=' + patientId;

    this.deferred = Backbone.Model.prototype.fetch.call(this, {
      url: restURL,
      dataType: 'json',
      reset: true
    }).fail(function (jqXHR) {
      if (jqXHR.status === 400) {
        //App.vent.trigger('patient-details:error', $.parseJSON(jqXHR.responseText).message);
      }
    });
  },
  parse: function (response) {
    response.patientIdentifier = {
      uniqueId: response.patientId
    };

    if (typeof response.id !== 'undefined' && response.id.length > 0) {
      response.notesListFour = getMyListLib().unescape(response.notesListFour);
      response.notesListThree = getMyListLib().unescape(response.notesListThree);
      response.notesListTwo = getMyListLib().unescape(response.notesListTwo);
      response.notesListOne = getMyListLib().unescape(response.notesListOne);
    }
    return response;
  },
  getNotesArr: function () {
    if (this.isExistingOnMongo()) {
      return [this.get('notesListOne'), this.get('notesListTwo'), this.get('notesListThree'), this.get('notesListFour')];
    } else {
      return ['', '', '', ''];
    }
  },
  isOnListsArr: function () {
    return getMyListLib().convertMyListsNumberToBooleanArray(this.get('myLists')).reverse();
  },
  isExistingOnMongo: function () {
    return this.get('id'); //the name made sense
  },
  isOnAnyMyLists: function () {
    return this.get('myLists') > 0; //the name made sense
  }
});

var MyListPatients = Backbone.PageableCollection.extend({
  model: Patient,
  url: function () {
    return '../' + myListBasePath + '/rest/my-list-search/site/' + Resources.user.vistaLocation + '/id/' + Resources.user.id + '/my-list-patients';
    //Resources.get('my-list-search').href + '/site/' + Resources.user.vistaLocation + '/id/' + Resources.user.id + '/my-list-patients';
  },
  state: {
    pageSize: 20,
    firstPage: 0,
    totalRecords: null
  },
  queryParams: {
    currentPage: 'currentPage',
    pageSize: null,
    totalRecords: null
  },
  parseState: function (resp, queryParams, state, options) {
    var count = parseInt(resp.myListCount);
    return {totalRecords: count};
  },
  parseRecords: function (resp, options) {
    return resp.patient;
  }
});

var FilterItemView = Backbone.Marionette.ItemView.extend({
  className: 'col-xs-6 filter-item-container',
  getTemplate: function () {
    if (this.model.get('edit')) {
      return getTemplate('filter-item-edit-view')(this.model.toJSON());
    }
    return getTemplate('filter-item-view')(this.model.toJSON());
  },
  modelEvents: {
    'change:active': 'render',
    'change:edit': 'render'
  },
  events: {
    'click button': 'handleLink',
    'change input': 'update'
  },
  ui: {
    checkbox: 'button',
    input: 'input'
  },
  initialize: function (opts) {
    this.model.set(_.extend(this.templateHelpers(), this.model.toJSON()));
  },
  handleLink: function (e) {
    e.preventDefault();
    this.model.set('active', !this.model.get('active'));
  },
  update: function () {
    this.model.set('name', this.ui.input.val().trim());
  },
  templateHelpers: function () {
    return {
      isActive: function () {
        return this.active;
      },
      activeClass: function () {
        return this.active ? 'checked' : '';
      }
    };
  },
  onShow: function () {
    this.bindUIElements();
    this.delegateEvents();
    if (this.options.focus) {
      this.ui.checkbox.focus();
    }
  }
});

var FilterView = Backbone.Marionette.CompositeView.extend({
  id: 'my-list-filter-item-view',
  itemView: FilterItemView,
  itemViewContainer: '#my-list-filters',
  getTemplate: function () {
    return getTemplate('filter-view')();
  },
  itemViewOptions: function (model, index) {
    return {focus: index === 0};
  },
  initialize: function (options) {
    _.extend(this, options);
  },
  ui: {
    updateResultsBtn: 'button'
  },
  events: {
    'click button': 'mylistFilter'
  },
  mylistFilter: function () {
    vent.trigger('mylist:filter');
  },
  onShow: function () {
    this.bindUIElements();
    this.delegateEvents();
  }
});

var MyListPatientItem = Backbone.Marionette.ItemView.extend({
  tagName: 'button',
  className: 'btn mylist-patient text-left',
  initialize: function (options) {
    this.listTitles = (options || {} ).listTitles;
    this.model.set(_.extend(this.templateHelpers(), this.model.toJSON()));
  },
  getTemplate: function () {
    return getTemplate('mylist-patient-item')(this.model.toJSON());
  },
  events: {
    'click': 'patientDetails',
    'keypress': 'patientDetails'
  },
  patientDetails: function () {
    var patientInfo = new PatientInfo({patientId: this.model.get('patientId')});

    patientInfo.fetch().done(function () {
      vent.trigger('mylist:patient:select', patientInfo);
    });
  },
  templateHelpers: function () {
    var that = this;
    var model = this.model.toJSON();
    return {
      getMyListsNumber: function () {
        return 3;
      },
      getMyListAriaLabel: function () {
        return getMyListLib().createAriaLabel(model.myLists, that.listTitles.getTitleArr());//update signature
      }
    };
  },
  onShow: function () {
    this.bindUIElements();
    this.delegateEvents();
  }
});

var MyListResults = Backbone.Marionette.CompositeView.extend({
  id: 'my-list-results-view',
  getTemplate: function () {
    return getTemplate('mylist-view')();
  },
  itemView: MyListPatientItem,
  itemViewContainer: '#mylist-results',
  itemViewOptions: function (model, index) {
    return {
      patients: this.patients,
      listTitles: this.myListTitles
    };
  },
  initialize: function (options) {
    _.extend(this, options);
  },
  ui: {
    resultCt: 'h5',
    previousBtn: '#previous-btn-ml',
    nextBtn: '#next-btn-ml'
  },
  collectionEvents: {
    add: 'updateResultCount',
    remove: 'updateResultCount',
    sync: 'updateResultCount'
  },
  events: {
    'click #previous-btn-ml': 'previousSearchResults',
    'click #next-btn-ml': 'nextSearchResults',
    'keypress #previous-btn-ml': 'previousSearchResults',
    'keypress #next-btn-ml': 'nextSearchResults'
  },
  updateResultCount: function () {
    var length = this.collection.state.totalRecords;
    var result = length === 1 ? 'Result' : 'Results';
    this.ui.resultCt.html(length + ' ' + result);

    this.updatePaging(this.ui.previousBtn, this.collection.hasPreviousPage());
    this.updatePaging(this.ui.nextBtn, this.collection.hasNextPage());
  },
  updatePaging: function (ele, enabled) {
    ele.attr('disabled', !enabled);
  },
  previousSearchResults: function () {
    this.collection.getPreviousPage(this.getParams());
  },
  nextSearchResults: function () {
    this.collection.getNextPage(this.getParams());
  },
  filter: function () {
    this.collection.getFirstPage(_.extend({reset: true}, this.getParams()));
  },
  getParams: function () {
    var boolArr = [],
      mylistNumber;

    if(this.myLists.length === MY_LIST_LIMIT()){
      _.each(this.myLists.models, function (model) {
        boolArr.unshift(model.get('active'));
      });
    }
    else{
      for(var i=0; i<MY_LIST_LIMIT(); i++){
        boolArr.push(true);
      }
    }

    mylistNumber = getMyListLib().convertBooleanArrayToMyListsNumber(boolArr);

    return {
      data: {
        list: mylistNumber
      }
    };
  },
  onShow: function () {
    vent.on('mylist:filter', $.proxy(this.filter, this));
    this.bindUIElements();
    this.delegateEvents();
    this.collection.getFirstPage(this.getParams());
  }
});

var MyListPatientMyListView = Backbone.Marionette.ItemView.extend({
  getTemplate: function () {
    var data = this.model.toJSON();
    return getTemplate('patient-mylist-item')(data);
  }
});

var MyListPatientMyListsView = Backbone.Marionette.CompositeView.extend({
  itemView: MyListPatientMyListView,
  itemViewContainer: 'ul',
  getTemplate: function () {
    var data = this.model.toJSON();
    return getTemplate('patient-mylists')(data);
  }
});

var MyListPatientSelectNote = Backbone.Marionette.ItemView.extend({
  className: 'note',
  getTemplate: function () {
    var data = _.extend(this.model.toJSON(), this.templateHelpers());
    return getTemplate('patient-select-note')(data);
  },
  ui: {
    checkBox: '.square-parent',
    editNote: '.patient-details-pencil-btn',
    noteBox: '.note-box',
    toggle: '.note-area'
  },
  events: {
    'click .square-child': 'toggleChecked',
    'click .patient-details-pencil-btn': 'editNote',
    'change .note-box': 'updateNote'
  },
  initialize: function () {
    vent.on('disable:note-boxes', this.disableNote, this);
  },
  toggleChecked: function () {
    var $checkBox = this.ui.checkBox;
    var checked;

    $checkBox.toggleClass('checked');

    checked = $checkBox.hasClass('checked');

    $checkBox.attr('aria-checked', checked);
    this.model.set('selected', checked);

    this.ui.toggle.toggleClass('out').toggleClass('in');

    this.editNote();
  },
  editNote: function () {
    var $noteBox = this.ui.noteBox;

    this.ui.editNote.addClass('hidden');
    $noteBox[0].disabled = false;
    $noteBox.focus().removeClass('border-none');
  },
  updateNote: function () {
    this.model.set('note', this.ui.noteBox.val());
  },
  disableNote: function () {
    this.bindUIElements();
    if (this.ui.checkBox.hasClass('checked')) {
      var $noteBox = this.ui.noteBox;
      $noteBox.addClass('border-none');
      $noteBox[0].disabled = true;
      this.ui.editNote.removeClass('hidden');
    }
  },
  templateHelpers: function () {
    var that = this;

    return {
      getNoteLabel: function () {
        var list = that.model.toJSON();

        if (list.selected) {
          return 'Note (Character Limit: ' + list.charLimit + ')';
        } else {
          return 'Note';
        }
      }
    };
  }
});

var MyListPatientSelectNotes = Backbone.Marionette.CollectionView.extend({
  itemView: MyListPatientSelectNote
});

var MyListPatientSelectView = Backbone.Marionette.LayoutView.extend({
  id: 'mylist-patient-select',
  ui: {
    'checkBoxes': '.square-parent',
    'expandMyList': '#expand-mylist',
    'noteBoxes': '.note-box',
    'updateMyList': '#update-patient-mylist',
    'selectPatient': '#select-patient'
  },
  modelEvents: {
    'change': 'render'
  },
  events: {
    'click .popup-close': 'closePopup',
    'click #select-patient': 'selectPatient',
    'click .square-parent button, .patient-details-pencil-btn': 'enableUpdateButton',
    'click #update-patient-mylist': 'updateToMyList'
  },
  regions: {
    'noteSection': '.note-section',
    'patientLists': '.patient-lists'
  },
  initialize: function () {
    this.dateman = new LV.Dateman();

    this.patientMyList = new Patient();
    this.listTitles = new MyListTitles();

    this.patientSelectNotesView = new MyListPatientSelectNotes();
    this.patientMyListsView = new MyListPatientMyListsView();
  },
  getTemplate: function () {
    var data = this.model.toJSON();
    return getTemplate('my-list-patient-select')(this.mixinTemplateHelpers(data));
  },
  mixinTemplateHelpers: function (target) {
    target = target || {};
    var templateHelpers = this.templateHelpers;
    if (_.isFunction(templateHelpers)) {
      templateHelpers = templateHelpers.call(this);
    }
    return _.extend(target, templateHelpers);
  },
  openPopup: function (model) {
    this.initModel(model);

    this.fetchData();

    this.draw();

    this.delegateEvents();
  },
  draw: function () {
    var $body = $('body');
    var $el = $('#' + this.id);

    if (!$el.length) {
      this.render();
      $body.append(this.$el);
    } else {
      this.$el = $el;
      this.render();
    }

    this.ui.selectPatient.focus();
  },
  closePopup: function () {
    this.clearModel();
    this.undelegateEvents();
    this.$el.addClass('hidden');
  },
  fetchData: function () {
    var that = this;
    var patientId = this.model.get('patientIdentifier').uniqueId;

    this.patientMyList.fetch(patientId);
    this.listTitles.deferred = this.listTitles.fetch();

    this.patientMyList.deferred.done(function () {
      that.listTitles.deferred.done(function () {
        that.afterFetchData();
      });
    });
  },
  afterFetchData: function () {
    var onAnyMyLists = this.patientMyList.isOnAnyMyLists();

    this.model.set({
      myListBtnText: onAnyMyLists ? 'Edit My List' : 'Add to My List',
      previouslySaved: this.patientMyList.isExistingOnMongo()
    }, {silent: true});

    this.render();

    this.listsCollection = new Backbone.Collection(this.populateLists());

    this.patientSelectNotesView.collection = this.listsCollection;

    this.noteSection.show(this.patientSelectNotesView);

    if (onAnyMyLists) {
      this.showPatientMyLists();
    }

    this.$el.removeClass('hidden');
  },
  showPatientMyLists: function () {
    var patientMyLists = new Backbone.Collection(this.listsCollection.where({selected: true}));

    this.patientMyListsView.model = new Backbone.Model({myLists: this.patientMyList.get('myLists')});
    this.patientMyListsView.collection = patientMyLists;

    this.patientLists.show(this.patientMyListsView);
  },
  initModel: function (model) {
    this.model = model;

    this.model.set({
      charLimit: 150,
      lists: [],
      myLists: '',
      myListBtnText: '',
      previouslySaved: false
    }, {silent: true});
  },
  clearModel: function () {
    this.model = new Backbone.Model();

    this.model.set({
      dateOfBirth: '',
      firstName: '',
      gender: '',
      lastName: '',
      ssn: '',
      wardLocation: ''
    }, {silent: true});

    this.initModel(this.model);
  },
  enableUpdateButton: function (config) {
    this.ui.updateMyList[0].disabled = config.disabled;
  },
  selectPatient: function () {
    vent.trigger('select:patient', this.model);
    this.closePopup();
  },
  populateLists: function () {
    var lists = [];
    var listNames = this.listTitles.getTitleArr();
    var notes = this.patientMyList.getNotesArr();
    var isOnLists = this.patientMyList.isOnListsArr();
    var charLimit = this.model.get('charLimit');

    _.each(_.values(listNames), function (name, index) {
      lists[index] = {
        charLimit: charLimit,
        id: index + 1,
        name: name,
        note: notes[index],
        selected: isOnLists[index]
      };
    }, this);

    return lists;
  },
  updateSuccess: function () {
    this.ui.expandMyList.text('Edit My List');
    this.enableUpdateButton({disabled: true});
    vent.trigger('disable:note-boxes');
    this.showPatientMyLists();
  },
  getSelectedArray: function () {
    var selectedArray = [];

    _.each(this.listsCollection.toJSON(), function (list) {
      selectedArray.unshift(list.selected);
    });

    return selectedArray;
  },
  updateModelForSave: function (sumOfLists) {
    var lists = this.listsCollection.toJSON();
    var attributes = {
      myLists: sumOfLists,
      notesListOne: lists[0].note,
      notesListTwo: lists[1].note,
      notesListThree: lists[2].note,
      notesListFour: lists[3].note
    };

    if (!this.model.get('previouslySaved')) {
      _.extend(attributes, {
        'facilityCode': Resources.user.vistaLocation,
        'staffId': Resources.user.id,
        'patientId': this.model.get('patientIdentifier').uniqueId,
        'patientIdentifier': this.model.get('patientIdentifier'),
        'displayName': this.model.get('displayName'),
        'firstName': this.model.get('firstName'),
        'lastName': this.model.get('lastName'),
        'gender': this.model.get('gender'),
        'middleName': ''
      });
    }
    this.patientMyList.set(attributes);
  },
  updateToMyList: function (e) {
    e.preventDefault();
    var that = this;
    var selectedArray = this.getSelectedArray();

    var sumOfLists = getMyListLib().convertBooleanArrayToMyListsNumber(selectedArray);

    this.updateModelForSave(sumOfLists);

    this.patientMyList.save(null, {
      success: function () {
        that.updateSuccess();
      }
    });
  },
  templateHelpers: function () {
    var that = this;

    return {
      getAge: function () {
        var date = that.model.get('dateOfBirth');

        return that.dateman.calculateAge(that.dateman.validateDate(new Date(date)));
      },
      getDate: function () {
        var date = that.model.get('dateOfBirth');

        return that.dateman.convertDate(new Date(date));
      }
    };
  }
});

var MyListLayoutView = Backbone.Marionette.LayoutView.extend({
  className: 'mylist-container',
  MY_LIST_LIMIT: function () {
    return MY_LIST_LIMIT();
  },
  getTemplate: function () {
    var data = _.extend(this.model.toJSON());
    return getTemplate('layout')(data);
  },
  configOptions: {},
  initialize: function (options) {
    this.model = new Backbone.Model(_.extend({closeBtn: false}, options));
    _.extend(this.configOptions, options);
    this.verifyOptions(this.configOptions);
    Resources = this.configOptions.Resources;
    vent = this.configOptions.Vent;
    basePath = this.configOptions.basePath || basePath;
    this.vents();
  },
  stage: function () {
    this.myListTitles = new MyListTitles();
    this.myListTitles.deferred = this.myListTitles.fetch();
    this.myLists = new MyLists({myListTitles: this.myListTitles});
    this.patients = new MyListPatients();
    this.filterView = new FilterView({collection: this.myLists});
    this.myListPatientSelect = this.myListPatientSelect || new MyListPatientSelectView();
    this.mylistResults = new MyListResults({collection: this.patients, myLists: this.myLists, myListTitles: this.myListTitles});
    this.vents();
  },
  verifyOptions: function (options) {
    var warnings = [],
      resourcesMissing = 'A <Resource> property must be specified',
      ventMissing = 'A <Vent> property must be specified that can handle triggering and listening to events.';
    if (!options) {
      warnings.push(resourcesMissing);
      warnings.push(ventMissing);
    }
    else {
      if (!options.Resources) {
        warnings.push(resourcesMissing);
      }
      if (!options.Vent) {
        warnings.push(ventMissing);
      }
    }
    _.each(warnings, function (warning) {
      console.warn(warning);
    });
    return warnings.length > 0;
  },
  regions: {
    filterRegion: '#filter-region',
    myListFilterRegion: '#my-list-filter-region',
    myListRegion: '#my-list-region',
    myListIcons: '.mylist-icons'
  },
  ui: {
    editBtn: '#edit',
    saveBtn: '#save',
    closeBtn: '#close'
  },
  events: {
    'click @ui.editBtn': 'editFilters',
    'click @ui.saveBtn': 'saveFilters',
    'click @ui.closeBtn': 'closeMyList'
  },
  editFilters: function () {
    this.toggleActiveFilterBtn(this.ui.editBtn, this.ui.saveBtn);
    this.myLists.setEdit(true);
  },
  saveFilters: function () {
    this.toggleActiveFilterBtn(this.ui.saveBtn, this.ui.editBtn);
    this.myListTitles.updateTitles(this.myLists.models);
    this.myLists.setEdit(false);
  },
  toggleActiveFilterBtn: function (active, nonActive) {
    active.addClass('active').attr('disabled', true);
    nonActive.removeClass('active').removeAttr('disabled');
  },
  vents: function () {
    var view = this;
    vent.on('mylist:patient:select', function (model, CustomView) {
      model = model || new Backbone.Model();

      if (CustomView instanceof Backbone.Marionette.View) {
        var CustomSelectView = Backbone.Marionette.LayoutView.extend({});
        $.extend(true, CustomSelectView.prototype, MyListPatientSelectView.prototype, CustomView, {initialize: MyListPatientSelectView.prototype.initialize});
        view.customSelectView = new CustomSelectView();
        view.customSelectView.openPopup(model);
      } else {
        view.myListPatientSelect = view.myListPatientSelect || new MyListPatientSelectView();
        view.myListPatientSelect.openPopup(model);
      }
    });
    vent.on('mylist:patient:select:close', function () {
      if (view.myListPatientSelect && _.isFunction(view.myListPatientSelect.closePopup)) {
        view.myListPatientSelect.closePopup();
      }
      if (view.customSelectView && _.isFunction(view.customSelectView.closePopup)) {
        view.customSelectView.closePopup();
      }
    });
  },
  onShow: function () {
    this.stage();
    this.bindUIElements();
    this.delegateEvents();
    this.myListFilterRegion.show(this.filterView);
    this.myListRegion.show(this.mylistResults);
  },
  closeMyList: function () {
    vent.trigger('mylist:close');
  }
});

LV.MyList = MyListLayoutView;
