//Author: Almas Barzhaxynov
//Editor: Will Preston
/*
 * This is a bootstrap powered jquery wrapper object.
 * Given a valid selector and other parameters, will display a popover briefly once triggered.
 * It was painstakingly tested to be Section 508 accessible and compliant on iPad ios7
 *
 * USE: Instaniate the popover object with the require parameters.
 * @param: success        - if an error occured this should be set to false otherwise true
 * @param: content        - the description of the popover
 * @param: placement      - [ 'left', 'right', 'top', 'bottom']
 * @param: container      - valid jquery selector of html tag the popover should be associated with
 *
 * When the popover should appear, trigger an 'addPopover' event on the container element.
 */

LV.Popover = function (opts) {
    if (typeof opts === 'undefined' || opts.length !== 6) {
	console.log('Popover requires (success, content, placement, container, delayForReader, delayRemove) as parameters');
	return;
    } else {
	this.container      = opts.container;
	this.success        = opts.success;
	this.content        = opts.content;
	this.placement      = opts.placement;
	this.delayForReader = 1500; //ms
	this.delayRemove    = 4000; //ms
    }
    $(this.selector).on('addPopover', this.show);
};
_.extend(LV.Popover.prototype, {
	show: function () {
	var $popoverContainer = $(this.container);
	if (this.success) {
	    $popoverContainer.popover({
		    content: this.content + ' <i class="icon icon-check-1" aria-hidden="true"></i>',
			trigger: 'manual',
			placement: this.placement,
			html: 'true'
			});
	} else {
	    $popoverContainer.popover({
		    content: this.content + ' <i class="icon icon-close-1" aria-hidden="true"></i>',
			trigger: 'manual',
			placement: this.placement,
			html: 'true'
			});
	}
	$popoverContainer.popover('show');
	var that = this;
	setTimeout(function () {
		var $popover = $('.popover-content');
		$popover.attr('tabindex', 0);
		$popover.attr('aria-label', that.content);
		$popover.focus();
	    }, this.delayForReader);
	setTimeout(function () {
		$popoverContainer.popover('destroy');
		$popoverContainer.focus();
	    }, this.delayRemove);
	}
});
