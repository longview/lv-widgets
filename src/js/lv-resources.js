//Resources
// ---------
  
var getJSON = function (path) {
  var response = $.ajax({ url: path, dataType: 'json', async: false });
  return JSON.parse(response.responseText);
};

var cleanUpSession = function () {
  window.sessionStorage.token = null;
  $.ajaxPrefilter(function (options, originalOptions, jqXHR) {
    jqXHR.setRequestHeader('Authorization', '');
  });
  document.cookie = encodeURIComponent('JSESSIONID') + '=deleted; expires=' + new Date(0).toUTCString();
};

LV.Resources = function (opts) {
  var options = opts || {};
  this.loginUtilsOpts = opts.loginUtilsOpts || {};
  this.path = options.path || 'MobileHealthPlatformWeb';
  this._protocol = options._protocol || '';
  this._host = options._host || '';
  this.user = {};
  this.directory = {};
  this.store = {
    pool: {},
    put: function (tag, objectFunc) {
      this.pool[tag] = objectFunc;
    },
    get: function (tag) {
      if (typeof this.pool[tag] === 'function') {
        this.pool[tag] = new this.pool[tag]();
      }
      return this.pool[tag];
    }
  };
  this.initialize.apply(this, arguments);
};

_.extend(LV.Resources.prototype, {
  initialize: function () {
    var path = this.getResourcePath();
    var protocol = this._protocol ? this._protocol + '//' : '';
    
    this.loginUtils = new LV.LoginUtils({
      appName: this.loginUtilsOpts.appName,
      resourcesLink: protocol + this._host + this.loginUtilsOpts.resourcesLink,
      redirectUri: this.loginUtilsOpts.redirectUri ? protocol + this._host + this.loginUtilsOpts.redirectUri : window.location.protocol + '//' + window.location.host + window.location.pathname,
      noToken: cleanUpSession,
      authorizeUrl: protocol + this._host + this.loginUtilsOpts.authorizeUrl,
      lastAccessUrl: protocol + this._host + this.loginUtilsOpts.lastAccessUrl,
      autoRedirect: this.loginUtilsOpts.autoRedirect,
      onReceiveToken: this.loginUtilsOpts.onReceiveToken
    });
    this.directory = getJSON(path).link;
    this.loginUtils.checkForAuthCode();
    this.fetch('public-user-session');

    this.user = (this.isLoggedIn()) ? this.store.get('public-user-session').mhpuser : {};
  },
  get: function (tag) {
    return _.first(this.directory.filter(function (obj) { return obj.title === tag; }));
  },
  getResourcePath: function () {
    return (window.cordova ? this._protocol : window.location.protocol) + '//' + (window.cordova ? this._host : window.location.host) + '/' + this.path + '/rest/public/resource-directory';
  },
  getRedirectURI: function () {
    return '?redirect_uri=' + window.location.protocol + '//' + window.location.host + window.location.pathname;
  },
  fetch: function (tag) {
    this.store.put(tag, getJSON(this.get(tag).href));
  },
  instance: function (link) {
    return {
      directory: link,
      get: this.get
    };
  },
  encrypt: function (name) {
    return name.replace(/[a-z0-9]/gi, function (a) { return a.charCodeAt(0) - 11 + '|'; }).slice(0, -1);
  },
  getEncryptedUserId: function () {
    return this.encrypt(this.user.id || '');
  },
  checkToken: function () {
    var storedToken = window.sessionStorage.token;

    if (typeof storedToken !== 'undefined' && storedToken !== 'undefined' && storedToken !== null && storedToken !== 'null') {
      return true;
    } else {
      return false;
    }
  },
  isLoggedIn: function () {
    return !!(this.store.get('public-user-session').mhpuser) && this.checkToken();
  },
  getUserName: function () {
    return this.user.displayName || '';
  },
  login: function () {
    this.loginUtils.authorize();
  },
  cleanSession: cleanUpSession,
  logoutAsProvider: function () {
    var tokenUrl = this.get('token').href;
    var logoutUrl = window.location.protocol + '//' + window.location.host + '/AuthorizationServices/logout'; //this.get('logout').href;

    $.ajax({
      url: tokenUrl,
      type: 'DELETE',
      success: function () {
        cleanUpSession();
        window.location = logoutUrl;
      },
      error: function () {
        console.log('failed to delete authorization token');
      }
    });

  },
  logout: function () {
    var tokenUrl = this.get('token').href,
      logoutUrl = this.get('logout').href,
      ref;
    if (window.cordova) { ref = window.open(logoutUrl, '_blank', 'location=no,toolbar=no'); }
    $.ajax({
      url: tokenUrl,
      type: 'DELETE',
      success: function () {
        if (window.cordova) {
          ref.close();
          window.location.reload();
        } else {
          window.location = logoutUrl;
        }
      },
      error: function () {
        console.in('failed to delete authorization token');
      }
    });
  },
  checkROA: function () {
    if (this.isLoggedIn()) {
      if (!this.user.rightOfAccessAccepted) {
        window.location = this.get('roa').href + this.getRedirectURI();
      }
    }
  }
});