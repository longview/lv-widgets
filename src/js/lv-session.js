//Session
// ---------
/*jshint -W117 */

LV.Session = function (opts) {
  var options = opts || {};
  this.resources = options.resources || new LV.Resources();
  this.showWarningInterval = options.showWarningInterval || 180000;
  this.sleepCheckInterval = options.sleepCheckInterval || 20000;
  this.sleepCheckThreshold = options.sleepCheckThreshold || 60000;
  this.initialize.apply(this, arguments);
};

_.extend(LV.Session.prototype, Backbone.Events, {
  initialize: function () {
    if (this.resources.isLoggedIn()) {
      this.startSessionTimer();
      var that = this;
      $(document).on('ajaxSuccess', function () {
	      var reset = _.bind(that.resetSessionTimer, that);
	      reset(that.startSessionTimer());
      });
      this.startSessionSleepTimer();
    }
  },
  startSessionTimer: function() {
    var timeLeft = this.getTimeToExpireInMilliSeconds();
    if (this.warningTimer) {
      clearTimeout(this.warningTimer);
    }
    if (this.endedTimer) {
      clearTimeout(this.endedTimer);
    }
    var _self = this;
    if (timeLeft > 0) {
      this.warningTimer = window.setTimeout(function () {
	      _self.trigger('session:warning', _self.showWarningInterval);
      }, timeLeft - _self.showWarningInterval);
      this.endedTimer = window.setTimeout(function () {
	      _self.trigger('session:ended');
      }, timeLeft);
    } else {
        _self.trigger('session:ended'); //session has ended. let em know!
    }
  },
  startSessionSleepTimer: function () {
    this.lastActiveTime = new Date().getTime();
    this.sessionSleepThread();
  },
  sessionSleepThread: function () {
    var _self = this;
    window.setTimeout(function () {
      var currentTime = new Date().getTime();
      var diff = currentTime - _self.lastActiveTime - _self.sleepCheckInterval;
      if (diff > _self.sleepCheckThreshold) {
        _self.trigger('session:wokeUp');
      }
      _self.lastActiveTime = currentTime;
      window.setTimeout(_self.sessionSleepThread, _self.sleepCheckInterval);
    }, this.sleepCheckInterval);
  },
  resetSessionTimer: function(callback) {
    var cb = callback || function () {};
    var path = this.resources.get('user-session').href;
    var that = this;
    $.ajax({
      url: path,
      dataType: 'json',
      global: false,
      success: function () {
		if (typeof cb === 'function') {
		    var bound = _.bind(cb, that);
		    bound();
		}
      },
      error: function () {
		cleanUpSession();
		that.trigger('session:ended');
      }
    });
  },
  cleanUpSession: function () {
    if(this.warningTimer) {
      clearTimeout(this.warningTimer);
    }
    if (this.endedTimer) {
      clearTimeout(this.endedTimer);
    }
    if (this.sessionSleepThread) {
      clearTimeout(this.sessionSleepThread);
    }
    cleanUpSession();
  },
  getTimeToExpireInMilliSeconds: function () {
	    var path = this.resources.get('last-accesstime').href;
	    var that = this;
	    var response = $.ajax({url: path, dataType: 'json', async: false, global: false, error: function () {
			that.vent.trigger('session:ended');
		    }});

	    var result;
	    if (response.status === 200) {
		var json = JSON.parse(response.responseText);
		result = json.timeToExpireInSeconds;
	    } else {
		result = '0';
	    }

	    return parseInt(result) * 1000;
  }
});
