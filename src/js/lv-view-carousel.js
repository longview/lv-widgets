var DefaultCarouselItem = Backbone.Marionette.CompositeView.extend({
    template: function () {
        var template = JST['html/carousel/carousel-item.html']();
        return _.template(template);
    },
    attributes: function () {
        return {
            class: 'item' + (this.options.active ? ' active' : ''),
            'data-index': this.options.index
        };
    }
});

var Carousel = Backbone.Marionette.CompositeView.extend({
    template: function () {
        var template = JST['html/carousel/lv-carousel.html']();
        return _.template(template);
    },
    itemView: DefaultCarouselItem,
    itemViewContainer: '.carousel-inner',
    attributes: function () {
        return {
            id: 'carousel-generic',
            class: 'carousel slide',
            'data-wrap': this.options.wrap || false,
            'data-interval': this.options.interval || false
        };
    },
    ui: {
        leftBtn: '.left',
        leftSection: '.carousel-left-section',
        rightBtn: '.right',
        rightSection: '.carousel-right-section'
    },
    events: {
        'click .left': 'showPrevSlide',
        'click .right': 'showNextSlide'
    },
    initialize: function () {
        _.bindAll(this, 'updateButtons');

        if (!this.options.wrap) {
            var that = this;

            this.$el.on('slid.bs.carousel', function (direction, relativeTarget) {
                that.updateButtons(relativeTarget);
            });
        }
    },
    onShow: function () {
        if (!this.options.wrap) {
            this.updateButtons();
        }

        if (this.options.leftSection) {
            this.options.leftSection.render();
            this.ui.leftSection.html(this.options.leftSection.$el);
        }

        if (this.options.rightSection) {
            this.options.leftSection.render();
            this.ui.leftSection.html(this.options.rightSection.$el);
        }
    },
    itemViewOptions: function (model, index) {
        return {
            active: index === this.options.startAt || 0,
            index: index
        };
    },
    enableButtons: function () {
        this.ui.leftBtn[0].disabled = false;
        this.ui.rightBtn[0].disabled = false;
    },
    updateButtons: function (item) {
        var $item = item ? $(item) : this.$el.find('.active');

        this.enableButtons();

        if ($item.is(':first-child')) {
            this.ui.leftBtn[0].disabled = true;
            this.ui.rightBtn.focus();
        }

        if ($item.is(':last-child')) {
            this.ui.rightBtn[0].disabled = true;
            this.ui.leftBtn.focus();
        }
    },
    showPrevSlide: function () {
        this.$el.carousel('prev');
    },
    showNextSlide: function () {
        this.$el.carousel('next');
    }
});

LV.Views.Carousel = LV.Carousel = Carousel;
