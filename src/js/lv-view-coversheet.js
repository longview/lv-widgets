var authorityId;
var restURL;
var cachedCoversheetData = new Backbone.Model();

var isCached = function (item) {
  return cachedCoversheetData.get(item);
};

LV.Models.PatientFromCoverSheet = Backbone.Model.extend({

  defaults: {
    'id': null,
    'displayName': '',
    'firstName': '',
    'lastName': '',
    'gender': 'No Data Found',
    'patientIdentifier': 'No Data Found',
    'patientId': 'No Data Found',
    'dateOfBirth': 'No Data Found',

    address: {
      streetAddressLine1: 'No Data Found',
      streetAddressLine2: 'No Data Found',
      streetAddressLine3: 'No Data Found',
      city: 'No Data Found',
      state: 'No Data Found',
      zip: 'No Data Found'
    },
    'emailAddress': 'No Data Found',
    'phoneNumberWork': 'No Data Found',
    'phoneNumberHome': 'No Data Found',
    'phoneNumberMobile': 'No Data Found',
    'phoneNumberPager': 'No Data Found',
    wardLocation: 'No Data Found',
    nextOfKin: {
      relationship: 'No Data Found',
      name: 'No Data Found',
      phoneNumber: 'No Data Found'
    },

    //AllergiesInfoTabFields
    allergy: {
      'reaction': 'No Data Found',
      'sourceSystem': 'No Data Found',
      'substance': 'No Data Found'
    },

    //FutureAppointments
    appointment: {
      'facilityName': 'No Data Found',
      'clinicName': 'No Data Found',
      'provider': 'No Data Found',
      'appointmentStartDate': 'No Data Found'
    },

    //medical diagnosis fields
    'encounterDiagnosisICDCode': {
      'size': ''
    },

    //Surgeries
    surgery: [],

    //Hospitalizations
    admission: []

  },
  initialize: function () {

  },
  fetchFromMDWS: function (patientId, viewSource, ResourceDirectory, async, params) { //please provide a resource directory
    //Add the last part of the url for viewSource. Ex: 'demographics/scope/operational'
    if (typeof patientId === 'undefined') {
      patientId = this.get('patientId');
    }
    authorityId = ResourceDirectory.user.userIdentifier.assigningAuthority;

    restURL = ResourceDirectory.get('patient').href + '/' + authorityId + '/' + patientId;
    if (viewSource && viewSource.length > 0) {
      restURL += '/' + viewSource;
    }
    if (viewSource === '/problem-list-diagnoses') {
      var problemListURL = ResourceDirectory.get('problem-list-diagnoses').href;

      if (problemListURL) {
        restURL = problemListURL;
      } else {
        var userId = ResourceDirectory.user.id;
        var vistaLocation = ResourceDirectory.user.vistaLocation;
        restURL = ResourceDirectory.get('medical').href.split('%', 1) + vistaLocation + '/id/' + userId + '/patient/' + authorityId + '/' + patientId + '/problem-list-diagnoses';
      }
    }
    restURL += params || '';
    var that = this;
    this.fetch({
      url: restURL,
      dataType: 'json',
      reset: true,
      async: async || false,
      success: function () {
        that.trigger('saveLocalModel');
        that.trigger('fetch-done');
      }
    }).fail(function (jqXHR) {
      if (jqXHR.status === 400) {
        that.trigger('cover-sheet:error', $.parseJSON(jqXHR.responseText).message);
      }
    });
  },
  parse: function (response) {
    var patient = response;

    if (typeof response.patientIdentifier !== 'undefined') {
      patient.patientId = response.patientIdentifier.uniqueId;
    }

    var location = [];
    if (patient.wardLocation) {
      location.push(patient.wardLocation);
    }
    if (patient.roombed) {
      location.push(patient.roombed);
    }
    patient.fullLocation = location.join(' / ');

    return _.extend(this.toJSON(), patient);
  },
  setParse: function (data, clear) {
    if (clear) {
      this.clear();
    }

    this.set(this.parse(data));
  }
});

LV.Models.CoversheetOption = Backbone.Model.extend({
  defaults: {
    active: true,
    id: 'tab',
    label: 'Coversheet Option',
    order: 100
  },
  parse: function (object) {
    object.isCustom = _.isObject(object.view);
    return object;
  }
});

LV.Collections.CoversheetOptions = Backbone.Collection.extend({
  model: LV.Models.CoversheetOption,
  comparator: 'order',
  initialize: function (models, options) {
    options = options || {};
    var collection = this,
      defaultTabs = new Backbone.Collection([
        {label: 'Contact', id: 'contact', order: 0},
        {label: 'Medical', id: 'medical', order: 10},
        {label: 'Allergies', id: 'allergies', order: 20},
        {label: 'Inpatient Medications', id: 'inpatient', order: 30},
        {label: 'Outpatient Medications', id: 'outpatient', order: 40},
        {label: 'Surgeries', id: 'surgeries', order: 50},
        {label: 'Future Appointments', id: 'future', order: 60},
        {label: 'Hospitalizations', id: 'hospitalizations', order: 70}
      ]);

    if (_.isArray(options.TabsConfig)) {
      if (/^false$/i.test(options.mergeTabs)) {
        defaultTabs.reset(options.TabsConfig);
      }
      else {
        defaultTabs.add(options.TabsConfig, {merge: true});
      }
    }

    _.each(defaultTabs.models, function (model) {
      if (!/^false$/i.test(model.get('active'))) {
        collection.push(new collection.model(model.toJSON(), {parse: true}));
      }
    });

    collection.sort();
  }
});

LV.Views.ContactInfoTab = Backbone.Marionette.ItemView.extend({
  id: 'contact-info-view',
  tagName: 'div',
  className: '',
  template: function (data) {
    console.log('Contact Info Tab');
    console.log(data);
    data = data || {};
    var templateHelpers = {};

    data = _.extend(data, templateHelpers);

    var template = JST['html/coversheet/contactInfoTab.html'](data);
    return template;
  },
  events: {
  },
  initialize: function (options) {
    this.model = options.model;
  },
  onShow: function () {
  }
});

var medicationDetailFilter = '';

LV.Views.MedicationsInfoTab = Backbone.Marionette.ItemView.extend({
  id: 'inpatient-medications-item-view',
  tagName: 'div',
  className: 'cover-sheet-table',
  template: function (data) {
    data = data || {};
    var templateHelpers = {
      getInstructions: function (objso) {
        if (objso.sig) { //nerf
          return '<br>' + objso.sig;
        } else {
          return ''; //or nuffin
        }
      },
      getActive: function (objso) {
        return objso.drugName;
      },
      getReason: function (objso) {
        if (!objso.nonVA) {
          return 'VA';
        } else {
          return 'NON-VA';
        }
      },
      getStatus: function (objso) {
        return objso.status;
      },
      dataRows: function () {
        var rows = [];
        var records = _.filter(this.medication, function (objso) {
          return objso.medicationSource === medicationDetailFilter;
        });

        _.each(records, function (objso, index, medications) {
          rows.push([this.getActive(objso) + this.getInstructions(objso), this.getStatus(objso), this.getReason(objso)]);
        }, this);

        return rows;
      },
      colHeaders: function () {
        return ['Active and Recently Expired', 'Status', 'Source'];
      }
    };

    data = _.extend(data, templateHelpers);
    var template = JST['html/coversheet/medicationsItemview.html'](data);
    return template;
  },

  initialize: function (opts) {
    this.model = opts.model;

    if (opts.isInpatient) {
      medicationDetailFilter = 'Inpatient';
    } else {
      medicationDetailFilter = 'Outpatient';
    }
  },
  onShow: function () {

  }

});
LV.Views.FutureTab = Backbone.Marionette.CompositeView.extend({
  id: 'appointment-info-view',
  template: function (data) {
    var template = JST['html/coversheet/appointmentInfoTab.html'](data);
    return template;
  },
  initialize: function (options) {
    this.model = options.model;
  },
  onShow: function () {
  }
});


LV.Views.MedicalTab = Backbone.Marionette.ItemView.extend({
  id: 'medical-view',
  tagName: 'div',
  className: '',
  template: function (data) {
    data = data || {};
    var templateHelpers = {
      isDiagnosis: function () {
        if (this.encounterDiagnosisICDCode.size === '') {
          return false;
        } else {
          return true;
        }
      }
    };

    data = _.extend(data, templateHelpers);
    var template = JST['html/coversheet/medicalTab.html'](data);
    return template;
  },
  events: {
  },
  initialize: function (options) {
    this.model = options.model;
  },
  onShow: function () {
  }
});
LV.Views.AllergiesInfoTab = Backbone.Marionette.CompositeView.extend({
  id: 'allergy-info-view',
  template: function (data) {
    var template = JST['html/coversheet/allergiesInfoTab.html'](data);
    return  template;
  },
  initialize: function (options) {
    this.model = options.model;
  },
  onShow: function () {
  }
});
LV.Views.SurgeriesInfoTab = Backbone.Marionette.CompositeView.extend({
  id: 'surgery-info-view',
  template: function (data) {
    var template = JST['html/coversheet/surgeriesInfoTab.html'](data);
    return  template;
  },
  initialize: function (options) {
    this.model = options.model;
  },
  onShow: function () {
  }
});
LV.Views.HospitalizationsInfoTab = Backbone.Marionette.CompositeView.extend({
  id: 'hospitalization-info-view',
  template: function (data) {
    var template = JST['html/coversheet/hospitalizationsInfoTab.html'](data);
    return  template;
  },
  initialize: function (options) {
    this.model = options.model;
  },
  onShow: function () {
  }
});

var Coversheet = Backbone.Marionette.LayoutView.extend({
  id: 'cover-sheet-view',
  tagName: 'div',
  className: '',
  template: function (data) {
    var template = JST['html/coversheet/lv-coversheet.html'](data);
    return _.template(template);
  },
  focusDelay: 500, //ms   focus into
  events: {
    'click .cover-tab': 'showTabbedSheetView',
    'click .panel-heading': 'showCollapseSheetView',
    'click .inpatient': 'showMedicationsView',
    'click .outpatient': 'showMedicationsView',
    'click .allergies': 'showAllergiesInfoTab',
    'click .medical': 'showMedicalDiagnosisTab',
    'click .surgeries': 'showSurgeriesInfoTab',
    'click .future': 'showFutureTab',
    'click .hospitalizations': 'showHospitalizationsInfoTab',
    'click .panel': 'focusInto',
    'click .cover-tab-container': 'focusInto'
  },
  regions: {
    'contactTab': '.contactTab',
    'medicalTab': '.medicalTab',
    'allergiesTab': '.allergiesTab',
    'inpatientTab': '.inpatientTab',
    'outpatientTab': '.outpatientTab',
    'surgeriesTab': '.surgeriesTab',
    'futureTab': '.futureTab',
    'hospitalizationsTab': '.hospitalizationsTab'
  },
  initialize: function (args) {
    //This constructor assumes both a json of model attributes and a reference to a marionette Application global object
    var view = this;
    args = args || {attributes: {}};

    cachedCoversheetData = new Backbone.Model();

    this.App = args.App;
    this.Patient = args.Patient || new Backbone.Model(args.attributes);
    this.App.coverLayout = {};
    this.coversheetOptions = new LV.Collections.CoversheetOptions(null, args);
    this.customTabView = null;
    this.headers = args.headers || false;
    this.async = args.async || false;

    this.cachedCoversheetData = cachedCoversheetData;
    this.isCached = isCached;

    if (args.ResourceDirectory instanceof Array) {
      this.ResourceDirectory = this.buildResourceDirectory(args.ResourceDirectory);

      this.addPatientLink();
    } 
    else {
      this.ResourceDirectory = args.ResourceDirectory;
    }

    this._createCustomViews();
    this._createCustomTabView(view, args);

    if ($.isEmptyObject(args.attributes)) {

      if (this.Patient.get('patientIdentifier') || this.Patient.get('patientId')) {
        this.patientId = this.Patient.get('patientIdentifier') ? this.Patient.get('patientIdentifier').uniqueId : this.Patient.get('patientId');
        this.setModelFromPatient();
      }
      else {
        this.trigger('error:coversheet');
      }
    } 
    else {
      this.setModelFromPatient();
    }

    //listen event triggers for each tab
    //this.listenTo(this.App.vent, 'goToCover', this.showCoverSheetTab); ?!?!?!
    this.model.set({'tabs': this.coversheetOptions.toArray()}, {});
    this.model.set('customTabs', !!(this.customTabView));
    this.model.set('desktopClass', (_.has(args, 'desktopClass') ? args.desktopClass : 'hidden-xs'));
    this.model.set('desktopTabClass', (_.has(args, 'desktopTabClass') ? args.desktopTabClass : 'col-sm-9'));
    this.listenTo(this.model, 'saveLocalModel', this.saveLocally);
    this.listenTo(this.model, 'fetch-done', function () {
      if (_.isFunction(this.fetchDone)) {
        this.fetchDone.call(this);
      }
    });
  },
  _createCustomTabView: function (view, args) {
    /*
     * A Custom View passed in as config to the new instance of the Coversheet
     * Takes all the existing handlers and rebinds them to the custom tabs.
     */
    if (args.CustomTabView) {
      _.extend(args.CustomTabView.prototype, {events: view.events});
      _.each(view.events, function (cb) {
        args.CustomTabView.prototype[cb] = function () {
          view[cb].apply(view, arguments);
        };
      });
      view.customTabView = new args.CustomTabView({collection: view.coversheetOptions, model: view.model});
    }
  },
  _createCustomViews: function () {
    var view = this;
    var regions = {};
    var customViews = _.filter(this.coversheetOptions.models, function (model) {
      return model.get('isCustom');
    });

    if (!customViews.length) {
      return;
    }

    /*
     * Allow for custom views for content
     * and callbacks to trigger the views
     * These are passed in as a config on the initialization
     */
    _.each(customViews, function (custom) {
      var id = custom.get('id');
      var properCaseId = id.substring(0, 1).toUpperCase() + id.substring(1);
      var tabIdClass = id + 'Tab';
      var callbackName = 'show' + properCaseId + 'Tab';
      var customCallback = custom.get('viewCallback');
      if (_.isFunction(customCallback)) {
        LV.Views[properCaseId + 'Tab'] = custom.get('view');
        view[callbackName] = function (e) {
          var callback = customCallback;
          callback.call(view, e, {
            viewName: properCaseId + 'Tab',
            id: id,
            tabName: tabIdClass
          });
        };
        view.events['click .' + id] = callbackName;
      }
      regions[tabIdClass] = '.' + tabIdClass;
    });
    view.addRegions(regions);
    view.delegateEvents();
  },
  setModelFromPatient: function () {
    this.model = new LV.Models.PatientFromCoverSheet(this.Patient.toJSON());
    this.saveLocally();
  },
  buildResourceDirectory: function (resources) {
    var resourceDirectory = resources[0];

    resources.splice(0, 1);

    _.each(resources, function (resource) {
      if (_.isFunction(resource.toJSON)) {
        resource = resource.toJSON();
      }

      resourceDirectory.directory = resourceDirectory.directory.concat(resource);
    });

    return resourceDirectory;
  },
  addPatientLink: function () {
    if (!this.ResourceDirectory.get('patient') && this.ResourceDirectory.get('patients')) {
      var patientLink = {
        title: 'patient',
        href: this.ResourceDirectory.get('patients').href.slice(0, -1)
      };

      this.ResourceDirectory.directory.push(patientLink);
    }
  },
  saveLocally: function () {
    cachedCoversheetData.set('patient', this.model.toJSON());
  },
  onRender: function () {
    this.showContactTab();
  },
  showMedicationsView: function (e) {
    var isInpatient = $(e.currentTarget).hasClass('inpatient');
    var tabUrl      = 'medications/scope/operational';
    var options;
    var params;

    var callBack = function () {
      if (isInpatient) {
        options = this.coversheetOptions.findWhere({id: 'inpatient'});
        this.model.set({
          'headers': this.headers,
          'label': options.get('label')
        });
        this.App.coverLayout.inpatientMedications = new LV.Views.MedicationsInfoTab({model: this.model, isInpatient: true});
        this.inpatientTab.show(this.App.coverLayout.inpatientMedications);
      } 
      else {
        options = this.coversheetOptions.findWhere({id: 'outpatient'});
        this.model.set({
          'headers': this.headers,
          'label': options.get('label')
        });
        this.App.coverLayout.outpatientMedications = new LV.Views.MedicationsInfoTab({model: this.model, isInpatient: false});
        this.outpatientTab.show(this.App.coverLayout.outpatientMedications);
      }

      params = options.get('params');
    };

    this.processView({tab: 'medicationTab', newModel: false, tabUrl: tabUrl, params: params, callback: callBack});
  },
  showContactTab: function (e) {
    var tabUrl = 'demographics/scope/operational';
    var options = this.coversheetOptions.findWhere({id: 'contact'});
    var params = options.get('params');
   
    var callBack = function () {
      this.model.set({
        'headers': this.headers,
        'label': options.get('label')
      });
      this.App.coverLayout.contact = new LV.Views.ContactInfoTab({model: this.model});
      this.contactTab.show(this.App.coverLayout.contact);
    };

    this.processView({tab: 'coverTab', newModel: false, tabUrl: tabUrl, params: params, callback: callBack});
  },
  showAllergiesInfoTab: function (e) {
    var tabUrl = 'allergies/scope/operational';
    var options = this.coversheetOptions.findWhere({id: 'allergies'});
    var params = options.get('params');
    
    var callBack = function () {
      this.model.set({
        'headers': this.headers,
        'label': options.get('label')
      });
      this.App.coverLayout.allergies = new LV.Views.AllergiesInfoTab({model: this.model});
      this.allergiesTab.show(this.App.coverLayout.allergies);
    };

    this.processView({tab: 'allergiesTab', newModel: false, tabUrl: tabUrl, params: params, callback: callBack});
  },
  showSurgeriesInfoTab: function (e) {
    var tabUrl = 'surgeries/scope/operational';
    var options = this.coversheetOptions.findWhere({id: 'surgeries'});
    var params = options.get('params');
    
    var callBack = function () {
      this.model.set({
        'headers': this.headers,
        'label': options.get('label')
      });
      this.App.coverLayout.surgeries = new LV.Views.SurgeriesInfoTab({model: this.model});
      this.surgeriesTab.show(this.App.coverLayout.surgeries);
    };

    this.processView({tab: 'surgeriesTab', newModel: false, tabUrl: tabUrl, params: params, callback: callBack});
  },
  showHospitalizationsInfoTab: function (e) {
    var tabUrl = 'admissions/scope/longitudinal';
    var options = this.coversheetOptions.findWhere({id: 'hospitalizations'});
    var params = options.get('params');
    
    var callBack = function () {
      this.model.set({
        'headers': this.headers,
        'label': options.get('label')
      });
      this.App.coverLayout.hospitalizations = new LV.Views.HospitalizationsInfoTab({model: this.model});
      this.hospitalizationsTab.show(this.App.coverLayout.hospitalizations);
    };

    this.processView({tab: 'hospitalizationsTab', newModel: false, tabUrl: tabUrl, params: params, callback: callBack});
  },
  showMedicalDiagnosisTab: function (e) {
    var tabUrl = '/problem-list-diagnoses';
    var options = this.coversheetOptions.findWhere({id: 'medical'});
    var params = options.get('params');
    
    var callBack = function () {
      this.model.set({
        'headers': this.headers,
        'label': options.get('label')
      });
      this.App.coverLayout.medical = new LV.Views.MedicalTab({model: this.model});
      this.medicalTab.show(this.App.coverLayout.medical);
    };

    this.processView({tab: 'diagnosisTab', newModel: false, tabUrl: tabUrl, params: params, callback: callBack});
  },
  showFutureTab: function (e) {
    var tabUrl = 'appointments/future';
    var options = this.coversheetOptions.findWhere({id: 'future'});
    var params = options.get('params');
    
    var callBack = function () {
      this.model.set({
        'headers': this.headers,
        'label': options.get('label')
      });
      this.App.coverLayout.future = new LV.Views.FutureTab({model: this.model});
      this.futureTab.show(this.App.coverLayout.future);
    };

    this.processView({tab: 'futureTab', newModel: true, tabUrl: tabUrl, params: params, callback: callBack});
  },
  showTabbedSheetView: function (e) {
    this.trigger('session:continue');
    var $tab = $(e.currentTarget);
    $tab.parent().siblings('.active').children().removeAttr('aria-label');
    $tab.attr('aria-label', $tab.text().trim() + ', Active');
  },
  showCollapseSheetView: function (e) {
    this.trigger('session:continue');
    var $collapse = $(e.currentTarget);
    if ($collapse.hasClass('collapsed')) {    //This panel is opening, we label it Open.
      $collapse.attr('aria-label', $collapse.text().trim() + ', Open');
    } else {
      $collapse.removeAttr('aria-label');     //This panel is collapsing, we remove the label.
    }
  },
  focusInto: function (e) {
    var $a = $(e.currentTarget).find('a');
    var dataToggle = $a.attr('data-toggle');
    var $tag = null;
    if (dataToggle === 'pill') {
      $tag = $($a.attr('href'));
    } else if (dataToggle === 'collapse') {
      $tag = $($a.attr('data-target'));
    } else {
      return;
    }
    if ($tag.length > 0) {
      setTimeout(function () {
        var $focusOnMe = $tag.find('.focusinto');
        $focusOnMe.focus();
      }, this.focusDelay);
    }
  },
  /**
   * Common function to handle the
   * retrieving of a model and processing
   * the view to show
   */
  processView: function (config) {
    if (isCached(config.tab)) {
      if (config.newModel) {
        this.model = new LV.Models.PatientFromCoverSheet(this.cachedCoversheetData.get('patient'));
      }
      else {
        this.model.setParse(this.cachedCoversheetData.get('patient'), true);
      }
      return config.callback.call(this);
    }
    this.fetchDone = config.callback;
    this.model.fetchFromMDWS(this.patientId, config.tabUrl, this.ResourceDirectory, this.async, config.params);
    this.cachedCoversheetData.set(config.tab, true);
  }
});

LV.Views.Coversheet = LV.Coversheet = Coversheet;
