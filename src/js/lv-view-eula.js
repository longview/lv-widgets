//Eula View
// ---------

LV.Views.EulaView = Backbone.Marionette.ItemView.extend({
  id: 'eula',
  tagName: 'div',
  template: function () {
    return JST['html/lv-view-eula.html']();
  },
  events: {
    'click #accept-button': 'accept',
    'click #decline-button': 'decline'
  },
  eulaDate: function () {
    return $(this.template()).find('#modified-date').text();
  },
  initialize: function (opts) {
    var options = opts || {};
    this.app = options.app || '';
    this.isUserSpecific = options.isUserSpecific || false;
	this.resources = options.resources || new LV.Resources();
    this.value = {};
  },
  accept: function (e) {
	var encryptedId = this.isUserSpecific ? '/' + this.resources.getEncryptedUserId() : '';

    e.preventDefault();
    this.value.DateAccepted = Date.now();
    localStorage.setItem(this.app + '/eula' + encryptedId, JSON.stringify(this.value));
    window.location.reload();
  },
  decline: function (e) {
    if (typeof device !== 'undefined' && 'Windows' || 'Android') {
      navigator.app.exitApp();
    }
    else {
      e.preventDefault();
    }
  }
});
