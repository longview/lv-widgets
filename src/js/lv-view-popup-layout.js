//Popup Layout
// ---------

var PopupModel = Backbone.Model.extend({
  initialize: function () {
    var that = this;

    this.on('change', function () {
      that.updateModel();
    });
  },
  updateModel: function () {
    var data = this.toJSON(),
      buttons = data.buttons || [],
      buttonL = buttons[0] || {},
      buttonR = buttons[1] || {};

    data.title = data.title || '';
    data.closeBtnClass = data.title ? 'title-close' : '';
    data.closeBtnIcon = data.closeBtnIcon || '';
    data.numButtons = buttons.length > 1 ? 2 : 1;
    data.btnTitleL = buttonL.title || 'OK';
    data.btnActionL = buttonL.action || function () {
    };
    data.btnIconL = buttonL.icon || '';
    data.btnTitleR = buttonR.title || '';
    data.btnActionR = buttonR.action || function () {
    };
    data.btnIconR = buttonR.icon || '';
    data.popupClass = data.popupClass || '';
    data.contentClass = data.contentClass || '';
    data.hideCloseBtn = data.hideCloseBtn || false;

    data.contentClass += data.title ? ' header-size' : '';

    data.scope = data.scope || {};

    this.set(data, {silent: true});
  }
});

LV.Views.PopupLayout = Backbone.Marionette.LayoutView.extend({
  className: 'popup',
  template: function (data) {
    return JST['html/lv-view-popup-layout.html'](data);
  },
  regions: {
    'content': '.popup-content'
  },
  ui: {
    body: '.popup-body',
    content: '.popup-content',
    footer: '.popup-footer'
  },
  events: {
    'click #popup-close-btn': 'closePopup',
    'click #popup-button-l': 'doLeftBtnAction',
    'click #popup-button-r': 'doRightBtnAction'
  },
  initialize: function (options) {
    options = options || {};

    this.model = new PopupModel();

    this.model.set(options).trigger('change');
  },
  onRender: function () {
    this.appendButtons();

    this.delegateEvents();
  },
  onShow: function () {
    this.stackButtons();
  },
  appendButtons: function () {
    var templateFileName = 'html/lv-view-popup-' + (this.model.get('numButtons') > 1 ? 'two' : 'one') + '-button.html';
    var buttonTemplate = JST[templateFileName](this.model.toJSON());

    this.ui.footer.empty().append(buttonTemplate);
  },
  stackButtons: function () {
    var width = this.ui.body.outerWidth(),
      numButtons = this.model.get('numButtons');

    if (width <= 400 && numButtons > 1) {
      this.ui.footer.find('button').removeClass('popup-two-button');
      this.ui.content.addClass('popup-two-button');
    }
  },
  doLeftBtnAction: function () {
    this.doButtonAction(this.model.get('btnActionL'));
  },
  doButtonAction: function (action) {
    if ($.isFunction(action)) {
      action(this.model.get('scope'));
    }

    this.closePopup();
  },
  doRightBtnAction: function () {
    this.doButtonAction(this.model.get('btnActionR'));
  },
  closePopup: function () {
    this.close();
  }
});