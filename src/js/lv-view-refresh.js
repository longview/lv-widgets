  //Refresh View
  // ---------
  /*jshint -W117 */

LV.Views.RefreshView = Backbone.Marionette.ItemView.extend({
	getTemplate: function () {
	    var data = this.serializeData();
	  	return JST['html/lv-view-refresh.html'](data);
	},
	attributes: function(){
		var attrs = {
			class: 'refresh-widget' + (/^true$/i.test(this.options.invert) ? ' invert' : '')
		};
  		return _.extend(attrs, this.options.widgetAttrs);
	},
	events: {
		'click button': 'doRefresh'
	},
	ui: {
		button: 'button'
	},
	initialize: function (options) {
		var defaults = {
			label: 'Last Refresh',
			labelClass: 'bold',
			format: 'MM/DD/YYYY HH:mm',
			refreshIcon: 'glyphicon glyphicon-refresh',
			boldLabel: true,
    disabled: false,
    fetchables: []
		};
		_.extend(this.options, defaults, options);
		var fetchables = this.options.fetchables;
		this.options.fetchables = _.isArray(fetchables) ? fetchables : [fetchables];
	},
	serializeData: function(){
		return _.extend(
					this.options, 
					{time: this.formatTime()},
					{labelClass: this.getLabelClass()}
				);
	},
	formatTime: function () {
		if(moment){
			return moment().format(this.options.format);
		}
		function fix(val){
			return val < 10 ? '0' + val : val;
		}
		console.warn('Moment Date Library not found, standard date format is shown for Refresh View.');
		var d = new Date();
		var month = fix(month = d.getMonth() + 1);
		var day = fix(day = d.getDate());
		var year = d.getFullYear();
		var hour = fix(hour = d.getHours());
		var minute = fix(minute = d.getMinutes());
		return month + '/' + day + '/' + year + ' ' + hour + ':' + minute;
	},
	getLabelClass: function () {
		if(!/^true$/i.test(this.options.boldLabel)){
			this.options.labelClass.replace(/\bbold\b/gi, '');
		}
		return this.options.labelClass;
	},
	toggleRefreshButton: function () {
	  this.ui.button[this.ui.button.is(':disabled') ? 'removeAttr' : 'attr']('disabled', true);
	},
	doRefresh: function () {
		var that = this;
		var fetchables = this.options.fetchables;
		var deferreds = [];
		
  		function finalize(){
  			var finalizeCallback = that.options.finalize;
  			if(_.isFunction(finalizeCallback)){
  				finalizeCallback.call(finalizeCallback);
  			}
  			that.toggleRefreshButton.call(that);
        	that.render();
  		}

		this.toggleRefreshButton();
		_.each(fetchables, function (fetchable) {
			deferreds.push(fetchable.fetch());
		});
		$.when(deferreds).then(function(){
			finalize.call(that);
		});
	},
	onRender: function () {
	  if(/^true$/i.test(this.options.disabled)){
	    this.toggleRefreshButton();
	  }
	}
});