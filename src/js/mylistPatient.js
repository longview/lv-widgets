define(function (require) {
  var App = require('app');

  return Backbone.Model.extend({
    url: function () {
      return App.Resources.get('my-list-search').href + '/site/' + App.Resources.user.vistaLocation + '/id/' + App.Resources.user.id + '/my-list-patients';
    },
    numMyLists: 4,
    defaults: {
      'id': null,
      'displayName': '',
      'firstName': '',
      'lastName': '',
      'middleName': '',
      'gender': '',
      'location': '',
      'ssn': '',
      'dateOfBirth': -2209057200000,
      'facilityCode': '',
      'staffId': '',
      'patientId': '',
      'notesListOne' : '',
      'notesListTwo' : '',
      'notesListThree' : '',
      'notesListFour' : '',
      'myLists': 0

    },
    initialize: function () {

    },
    patientFetch: function (patientId) {
      if (typeof patientId === 'undefined') {
        patientId = this.get('patientId');
      }
      var line = App.Resources.get('my-list-patient').href;
      line =  App.insertURLPath(line, 'id', App.Resources.user.id);
      line =  App.insertURLPath(line, 'site', App.Resources.user.vistaLocation);
      var restURL = line + '?patientId=' + patientId;

      this.fetch({
        url: restURL,
        dataType: 'json',
        reset: true,
        async: false
      }).fail(function (jqXHR) {
        if (jqXHR.status === 400) {
          App.vent.trigger('patient-details:error', $.parseJSON(jqXHR.responseText).message);
        }
      });
    },
    parse: function (response) {
      var patient = this.toJSON();
      if (typeof response.id !== 'undefined' && response.id.length > 0) {
        patient = response;
        patient.notesListFour = App.unescape(response.notesListFour);
        patient.notesListThree = App.unescape(response.notesListThree);
        patient.notesListTwo = App.unescape(response.notesListTwo);
        patient.notesListOne = App.unescape(response.notesListOne);
      }
      return patient;
    },
    getNotesArr: function () {
      if (this.isExistingOnMongo()) {
        return [this.get('notesListOne'), this.get('notesListTwo'), this.get('notesListThree'), this.get('notesListFour')];
      } else {
        return ['', '', '', ''];
      }
    },
    isOnListsArr: function () {
      return App.convertBinaryNumberToBooleanArray(this.get('myLists')).reverse();
    },
    isExistingOnMongo: function () {
      return this.get('id') !== null; //the name made sense
    },
    isOnAnyMyLists: function () {
      return this.get('myLists') > 0; //the name made sense
    }
  });
});
