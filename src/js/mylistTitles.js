define(function (require) {
  var App = require('app');

  return Backbone.Model.extend({
    idAttribute: 'id',
    url: function () {
      var line = App.Resources.get('my-list-titles').href;
      line =  App.insertURLPath(line, 'id', App.Resources.user.id);
      line =  App.insertURLPath(line, 'site', App.Resources.user.vistaLocation);

      return line;
    },
    defaults: {
      listOneName: 'My List One',
      listTwoName: 'My List Two',
      listThreeName: 'My List Three',
      listFourName: 'My List Four',
      staffId: '',
      facilityCode: ''
    },
    initialize: function () {

    },
    fetchTitles: function () {
      this.fetch({
        url: this.url(),
        dataType: 'json',
        reset: true,
        async: false
      }).fail(function (jqXHR) {
        if (jqXHR.status === 400) {
          App.vent.trigger('my-list-titles:error', $.parseJSON(jqXHR.responseText).message);
        }
      });
    },
    getTitlesArr: function () {
      return [this.get('listOneName'), this.get('listTwoName'), this.get('listThreeName'), this.get('listFourName')];
    },
    createAriaLabel: function (bNum) {
      return App.createAriaLabel(bNum, this.getTitlesArr());
    },
    parse: function (response) {
      if (typeof response.listOneName !== 'undefined') {
        response.listOneName = App.unescape(response.listOneName);
        response.listTwoName = App.unescape(response.listTwoName);
        response.listThreeName = App.unescape(response.listThreeName);
        response.listFourName = App.unescape(response.listFourName);
      }

      return response;
    }

  });
});
